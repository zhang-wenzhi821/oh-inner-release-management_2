

# 标准系统接口变更

| 模块名称  | 接口名称  | 变更类型  | 变更说明  |
|  --------  |  --------  |  --------  |  --------  |
  |  时间日期数字模块-Locale  |  constructor(locale: string, options?:options)  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  toString(): string  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  maximize(): Locale  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  minimize(): Locale  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  calendar  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  caseFirst  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  collation  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  hourCycle  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  numberingSystem  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  numeric  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  language  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  script  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  region  |  新增  |  -  |
  |  时间日期数字模块-Locale  |  baseName  |  新增  |  -  |
  |  时间日期数字模块-DateTimeFormat  |  constructor(locale: string, options?:options)  |  新增  |  -  |
  |  时间日期数字模块-DateTimeFormat  |  constructor(locale: string[], options?:options)  |  新增  |  -  |
  |  时间日期数字模块-DateTimeFormat  |  resolvedOptions(): DateTimeOptions  |  新增  |  -  |
  |  时间日期数字模块-DateTimeFormat  |  format(date: Date): string;  |  新增  |  -  |
  |  时间日期数字模块-DateTimeFormat  |  formatRange(fromDate: Date, toDate: Date): string;  |  新增  |  -  |
  |  时间日期数字模块-NumberFormat  |  constructor(locale: string, options?:options)  |  新增  |  -  |
  |  时间日期数字模块-NumberFormat  |  constructor(locale: string[], options?:options)  |  新增  |  -  |
  |  时间日期数字模块-NumberFormat  |  resolvedOptions(): NumberOptions  |  新增  |  -  |
  |  时间日期数字模块-NumberFormat  |  format(number: number): string;  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  locale  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  dateStyle  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  timeStyle  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  calendar  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  dayPeriod  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  numberingSystem  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  localeMatcher  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  timeZone  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  hour12  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  hourCycle  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  formatMatcher  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  weekday  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  era  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  year  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  month  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  day  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  hour  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  minute  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  second  |  新增  |  -  |
  |  时间日期数字模块-DateTimeOptions  |  timeZoneName  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  locale  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  compactDisplay  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  currency  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  currencyDisplay  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  currencySign  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  localeMatcher  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  notation  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  numberingSystem  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  signDisplay  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  style  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  unit  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  unitDisplay  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  useGrouping  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  minimumIntegerDigits  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  minimumFractionDigits  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  maximumFractionDigits  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  minimumSignificantDigits  |  新增  |  -  |
  |  时间日期数字模块-NumberOptions  |  maximumSignificantDigits  |  新增  |  -  |
|文件存储- system.file|mkdir|新增|-|
|文件存储- system.file|rmdir|新增|-|
|文件存储- system.file|get|新增|-|
|文件存储- system.file|list|新增|-|
|文件存储- system.file|copy|新增|-|
|文件存储- system.file|move|新增|-|
|文件存储- system.file|delete|新增|-|
|文件存储- system.file|access|新增|-|
|文件存储- system.file|writeText|新增|-|
|文件存储- system.file|writeArrayBuffer|新增|-|
|文件存储- system.file|readText|新增|-|
|文件存储- system.file|readArrayBuffer|新增|-|
|文件存储- fileio|Dir.readSync|新增|-|
|文件存储- fileio|Dir.closeSync|新增|-|
|文件存储- fileio|dirent.name|新增|-|
|文件存储- fileio|dirent.isBlockDevice()|新增|-|
|文件存储- fileio|dirent.isCharacterDevice()|新增|-|
|文件存储- fileio|dirent.isDirectory()|新增|-|
|文件存储- fileio|dirent.isFIFO()|新增|-|
|文件存储- fileio|dirent.isFile()|新增|-|
|文件存储- fileio|dirent.isSocket()|新增|-|
|文件存储- fileio|dirent.isSymbolicLink()|新增|-|
|文件存储- fileio|stat.dev|新增|-|
|文件存储- fileio|stat.ino|新增|-|
|文件存储- fileio|stat.mode|新增|-|
|文件存储- fileio|stat.nlink|新增|-|
|文件存储- fileio|stat.uid|新增|-|
|文件存储- fileio|stat.gid|新增|-|
|文件存储- fileio|stat.rdev|新增|-|
|文件存储- fileio|stat.size|新增|-|
|文件存储- fileio|stat.blocks|新增|-|
|文件存储- fileio|stat.atime|新增|-|
|文件存储- fileio|stat.mtime|新增|-|
|文件存储- fileio|stat.ctime|新增|-|
|文件存储- fileio|stat.isBlockDevice()|新增|-|
|文件存储- fileio|stat.isCharacterDevice()|新增|-|
|文件存储- fileio|stat.isDirectory()|新增|-|
|文件存储- fileio|stat.isFIFO()|新增|-|
|文件存储- fileio|stat.isFile()|新增|-|
|文件存储- fileio|stat.isSocket()|新增|-|
|文件存储- fileio|stat.isSymbolicLink()|新增|-|
|文件存储- fileio|Stream.flushSync()|新增|-|
|文件存储- fileio|Stream.writeSync()|新增|-|
|文件存储- fileio|Stream.readSync()|新增|-|
|文件存储- fileio|Stream.closeSync()|新增|-|
|文件存储- fileio|fileio.accessSync()|新增|-|
|文件存储- fileio|fileio.chmodSync()|新增|-|
|文件存储- fileio|fileio.chownSync()|新增|-|
|文件存储- fileio|fileio.closeSync()|新增|-|
|文件存储- fileio|fileio.copyFileSync()|新增|-|
|文件存储- fileio|fileio.createStreamSync()|新增|-|
|文件存储- fileio|fileio.fchmodSync()|新增|-|
|文件存储- fileio|fileio.fchownSync()|新增|-|
|文件存储- fileio|fileio.fdopenStreamSync()|新增|-|
|文件存储- fileio|fileio.fstatSync()|新增|-|
|文件存储- fileio|fileio.fsyncSync()|新增|-|
|文件存储- fileio|fileio.ftruncateSync()|新增|-|
|文件存储- fileio|fileio.mkdirSync()|新增|-|
|文件存储- fileio|fileio.openSync()|新增|-|
|文件存储- fileio|fileio.opendirSync()|新增|-|
|文件存储- fileio|fileio.readSync()|新增|-|
|文件存储- fileio|fileio.renameSync()|新增|-|
|文件存储- fileio|fileio.rmdirSync()|新增|-|
|文件存储- fileio|fileio.statSync()|新增|-|
|文件存储- fileio|fileio.truncateSync()|新增|-|
|文件存储- fileio|fileio.unlinkSync()|新增|-|
|文件存储- fileio|fileio.writeSync()|新增|-|
|设备管理-DeviceManager|deviceType: DeviceType;|新增| -|
|设备管理-DeviceManager|deviceId: string;|新增| -|
|设备管理-DeviceManager|deviceName: string;|新增| -|
|设备管理-DeviceManager|DeviceType|新增| -|
|设备管理-DeviceManager|DeviceStateChangeAction|新增| -|
|设备管理-DeviceManager|createDeviceManager(bundleName: string, callback: AsyncCallback<DeviceManager>): void|新增| -|
|设备管理-DeviceManager|release(): void|新增| -|
|设备管理-DeviceManager|getTrustedDeviceListSync(): Array<DeviceInfo>|新增| -|
|设备管理-DeviceManager|on(type: 'deviceStateChange', callback: Callback<{ action: DeviceStateChangeAction, device: DeviceInfo }>): void|新增| -|
|设备管理-DeviceManager|off(type: 'deviceStateChange', callback?: Callback<{ action: DeviceStateChangeAction, device: DeviceInfo }>): void|新增| -|
|设备管理-DeviceManager|on(type: 'serviceDie', callback: () => void): void|新增| -|
|设备管理-DeviceManager|off(type: 'serviceDie', callback?: () => void): void|新增| -|
|ohos.multimedia.audio|audio getAudioManager(): AudioManager|新增| -|
|ohos.multimedia.audio|ActiveDeviceType SPEAKERPHONE|新增| -|
|ohos.multimedia.audio|ActiveDeviceType BLUETOOTH_SCO|新增| -|
|ohos.multimedia.audio|interface AudioDeviceDescriptor|新增| -|
|ohos.multimedia.audio|AudioDeviceDescriptor readonly deviceRole: DeviceRole|新增| -|
|ohos.multimedia.audio|AudioDeviceDescriptor readonly deviceType: DeviceType|新增| -|
|ohos.multimedia.audio|type AudioDeviceDescriptors = Array<Readonly<AudioDeviceDescriptor>>|新增| -|
|ohos.multimedia.audio|DeviceType BLUETOOTH_SCO|新增| -|
|ohos.multimedia.audio|DeviceType BLUETOOTH_A2DP|新增| -|
|ohos.multimedia.audio|DeviceType MIC|新增| -|
|ohos.multimedia.audio|AudioVolumeType RINGTONE|新增| -|
|ohos.multimedia.audio|AudioVolumeType MEDIA|新增| -|
|ohos.multimedia.audio|DeviceFlag OUTPUT_DEVICES_FLAG|新增| -|
|ohos.multimedia.audio|DeviceFlag INPUT_DEVICES_FLAG|新增| -|
|ohos.multimedia.audio|DeviceFlag ALL_DEVICES_FLAG|新增| -|
|ohos.multimedia.audio|DeviceRole INPUT_DEVICE|新增| -|
|ohos.multimedia.audio|DeviceRole OUTPUT_DEVICE|新增| -|
|ohos.multimedia.audio|DeviceType INVALID|新增| -|
|ohos.multimedia.audio|DeviceType EARPIECE|新增| -|
|ohos.multimedia.audio|DeviceType SPEAKER|新增| -|
|ohos.multimedia.audio|DeviceType WIRED_HEADSET|新增| -|
|ohos.multimedia.audio|AudioRingMode RINGER_MODE_SILENT|新增| -|
|ohos.multimedia.audio|AudioRingMode RINGER_MODE_VIBRATE|新增| -|
|ohos.multimedia.audio|AudioRingMode RINGER_MODE_NORMAL|新增| -|
|ohos.multimedia.audio|"AudioManager getAudioParameter(key: string, callback: AsyncCallback<string>): void getAudioParameter(key: string): Promise<string>"|新增| -|
|ohos.multimedia.audio|"AudioManager setAudioParameter(key: string, value: string, callback: AsyncCallback<void>): void setAudioParameter(key: string, value: string): Promise<void>"|新增| -|
|ohos.multimedia.audio|"AudioManager setRingerMode(mode: AudioRingMode, callback: AsyncCallback<void>): void setRingerMode(mode: AudioRingMode): Promise<void>"|新增| -|
|ohos.multimedia.audio|"AudioManager getRingerMode(callback: AsyncCallback<AudioRingMode>): void getRingerMode(): Promise<AudioRingMode>"|新增| -|
|ohos.multimedia.audio|"AudioManager setVolume(volumeType: AudioVolumeType, volume: number, callback: AsyncCallback<void>): void setVolume(volumeType: AudioVolumeType, volume: number): romise<void>"|新增| -|
|ohos.multimedia.audio|"AudioManager getVolume(volumeType: AudioVolumeType, callback: AsyncCallback<number>): void getVolume(volumeType: AudioVolumeType): Promise<number>"|新增| -|
|ohos.multimedia.audio|"AudioManager getMinVolume(volumeType: AudioVolumeType, callback: AsyncCallback<number>): void getMinVolume(volumeType: AudioVolumeType): Promise<number>"|新增| -|
|ohos.multimedia.audio|"AudioManager getMaxVolume(volumeType: AudioVolumeType, callback: AsyncCallback<number>): void getMaxVolume(volumeType: AudioVolumeType): Promise<number>"|新增| -|
|ohos.multimedia.audio|"AudioManager mute(volumeType: AudioVolumeType, mute: boolean, callback: AsyncCallback<void>): void mute(volumeType: AudioVolumeType, mute: boolean): Promise<void>"|新增| -|
|ohos.multimedia.audio|"AudioManager isMute(volumeType: AudioVolumeType, callback: AsyncCallback<boolean>): void isMute(volumeType: AudioVolumeType): Promise<boolean>"|新增| -|
|ohos.multimedia.audio|"AudioManager isActive(volumeType: AudioVolumeType, callback: AsyncCallback<boolean>): void isActive(volumeType: AudioVolumeType): Promise<boolean>"|新增| -|
|ohos.multimedia.audio|"AudioManager getDevices(deviceFlag: DeviceFlag, callback: AsyncCallback<AudioDeviceDescriptors>): void getDevices(deviceFlag: DeviceFlag): Promise<AudioDeviceDescriptors>"|新增| -|
|ohos.multimedia.audio|"AudioManager isMicrophoneMute(callback: AsyncCallback<boolean>): void isMicrophoneMute(): Promise<boolean>"|新增| -|
|ohos.multimedia.audio|"AudioManager setMicrophoneMute(mute: boolean, callback: AsyncCallback<void>): void setMicrophoneMute(mute: boolean): Promise<void>"|新增| -|
|ohos.multimedia.audio|"AudioManager isDeviceActive(deviceType: ActiveDeviceType, callback: AsyncCallback<boolean>): void isDeviceActive(deviceType: DeviceType): Promise<boolean>"|新增| -|
|ohos.multimedia.audio|"AudioManager setDeviceActive(deviceType: ActiveDeviceType, active: boolean, callback: AsyncCallback<void>): void setDeviceActive(deviceType: DeviceType, active: boolean): Promise<void>"|新增| -|
|ohos.multimedia.media|media createAudioPlayer(): AudioPlayer|新增| -|
|ohos.multimedia.media|type AudioState = 'idle' / 'playing' / 'paused' / 'stopped'|新增| -|
|ohos.multimedia.media|interface AudioPlayer|新增| -|
|ohos.multimedia.media|AudioPlayer play(): void|新增| -|
|ohos.multimedia.media|AudioPlayer pause(): void|新增| -|
|ohos.multimedia.media|AudioPlayer stop(): void|新增| -|
|ohos.multimedia.media|AudioPlayer seek(timeMs: number): void|新增| -|
|ohos.multimedia.media|AudioPlayer setVolume(vol: number): void|新增| -|
|ohos.multimedia.media|AudioPlayer reset(): void|新增| -|
|ohos.multimedia.media|AudioPlayer release(): void|新增| -|
|ohos.multimedia.media|AudioPlayer src: string|新增| -|
|ohos.multimedia.media|AudioPlayer loop: boolean|新增| -|
|ohos.multimedia.media|AudioPlayer readonly currentTime: number|新增| -|
|ohos.multimedia.media|AudioPlayer readonly duration: number|新增| -|
|ohos.multimedia.media|AudioPlayer readonly state: AudioState|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'play', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'pause', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'stop', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'reset', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'dataLoad', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'finish', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'volumeChange', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'timeUpdate', callback: Callback<number>): void|新增| -|
|ohos.multimedia.media|AudioPlayer on(type: 'error', callback: ErrorCallback): void|新增| -|
|ohos.multimedia.media|media createAudioRecorder(): AudioRecorder|新增| -|
|ohos.multimedia.media|AudioOutputFormat MPEG_4|新增| -|
|ohos.multimedia.media|AudioOutputFormat AAC_ADTS|新增| -|
|ohos.multimedia.media|AudioEncoder AAC_LC|新增| -|
|ohos.multimedia.media|"interface Location {  latitude: number   longitude: number}"|新增| -|
|ohos.multimedia.media|"interface AudioRecorderConfig {  audioEncoder?: AudioEncoder  audioEncodeBitRate?: number  audioSampleRate?: number  numberOfChannels?: number  format?: AudioOutputFormat  uri: string  location?: Location}"|新增| -|
|ohos.multimedia.media|AudioRecorder prepare(config: AudioRecorderConfig): void|新增| -|
|ohos.multimedia.media|AudioRecorder start(): void|新增| -|
|ohos.multimedia.media|AudioRecorder pause(): void|新增| -|
|ohos.multimedia.media|AudioRecorder resume(): void|新增| -|
|ohos.multimedia.media|AudioRecorder stop(): void|新增| -|
|ohos.multimedia.media|AudioRecorder release(): void|新增| -|
|ohos.multimedia.media|AudioRecorder reset(): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'prepare', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'start', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'pause', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'resume', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'stop', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'release', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'reset', callback: () => void): void|新增| -|
|ohos.multimedia.media|AudioRecorder on(type: 'error', callback: ErrorCallback): void|新增| -|
|语言编译器运行时-worker|postMessage(obj):void|新增|宿主线程与worker通信，传递数据|
|语言编译器运行时-worker|postMessage(message: Object, options?: PostMessageOptions):void|新增|宿主线程与worker通信，转移arrayBuffer的数据控制权|
|语言编译器运行时-worker|terminate():void|新增|宿主线程主动停止worker|
|语言编译器运行时-worker|on(type: string, listener: EventListener): void|新增|向worker添加回调接口|
|语言编译器运行时-worker|once(type: string, listener: EventListener): void|新增|向worker添加回调接口，并且在回调一次会释放回调|
|语言编译器运行时-worker|off(type: string, listener?: EventListener): void|新增|删除worker已添加的回调接口|
|语言编译器运行时-worker|addEventListener(type: string, listener: EventListener): void|新增|向worker添加回调接口|
|语言编译器运行时-worker|removeEventListener(type: string, listener?: EventListener): void|新增|删除worker已添加的回调接口|
|语言编译器运行时-worker|removeAllListener(): void|新增|删除worker所有的回调接口|
|语言编译器运行时-worker|dispatchEvent(event: Event): boolean|新增|向worker发送指定事件，触发回调接口|
|语言编译器运行时-parentPort|postMessage(obj):void|新增|worker与宿主线程通信，传递数据|
|语言编译器运行时-parentPort|postMessage(message: Object, options?: PostMessageOptions):void|新增|worker与宿主线程通信，转移arrayBuffer的数据控制权|
|语言编译器运行时-parentPort|close(): void|新增|worker主动终止|
|语言编译器运行时-Util|printf(format: string, ...args: Object[]): string|新增|-|
|语言编译器运行时-Util|getErrorString(errno: number): string|新增|-|
|语言编译器运行时-Util|callbackWrapper(original: Function): (err: Object, value: Object) => void|新增|-|
|语言编译器运行时-Util|promiseWrapper(original: (err: Object, value: Object) => void): Object|新增|-|
|语言编译器运行时-Util|new TextDecoder([encoding[, options]])|新增|-|
|语言编译器运行时-Util|decode([input[, options]]):string|新增|-|
|语言编译器运行时-Util|new TextEncoder()|新增|-|
|语言编译器运行时-Util|encode(input?: string): Uint8Array;|新增|-|
|语言编译器运行时-Util|"encodeInto(input: string,dest: Uint8Array,): { read: number; written: number };"|新增|-|
|语言编译器运行时-Util|readonly encoding: string;|新增|-|
|语言编译器运行时-Util|readonly fatal: boolean;|新增|-|
|语言编译器运行时-Util|readonly ignoreBOM = false;|新增|-|
|语言编译器运行时-Util|readonly encoding = "utf-8";|新增|-|
|语言编译器运行时-URL|new URL(url: string, base?: string/URL)|新增|-|
|语言编译器运行时-URL|toString(): string;|新增|-|
|语言编译器运行时-URL|toJSON(): string;|新增|-|
|语言编译器运行时-URL|new URSearchParams()|新增|-|
|语言编译器运行时-URL|new URSearchParams(string)|新增|-|
|语言编译器运行时-URL|new URSearchParams(obj)|新增|-|
|语言编译器运行时-URL|new URSearchParams(iterable)|新增|-|
|语言编译器运行时-URL|append(name: string, value: string): void;|新增|-|
|语言编译器运行时-URL|delete(name: string): void;|新增|-|
|语言编译器运行时-URL|entries(): IterableIterator<[string, string]>;|新增|-|
|语言编译器运行时-URL|forEach(callbackfn: (value: string, key: string, parent: this) => void, thisArg?: any,): void;|新增|-|
|语言编译器运行时-URL|get(name: string): string / null;|新增|-|
|语言编译器运行时-URL|getAll(name: string): string[];|新增|-|
|语言编译器运行时-URL|has(name: string): boolean;|新增|-|
|语言编译器运行时-URL|keys(): IterableIterator<string>;|新增|-|
|语言编译器运行时-URL|set(name: string, value: string): void;|新增|-|
|语言编译器运行时-URL|sort():void;|新增|-|
|语言编译器运行时-URL|toString():string|新增|-|
|语言编译器运行时-URL|values(): IterableIterator<string>;|新增|-|
|语言编译器运行时-URL|URSearchParams[Symbol.iterator]()|新增|-|
|语言编译器运行时-URL|hash: string;|新增|-|
|语言编译器运行时-URL|host: string;|新增|-|
|语言编译器运行时-URL|hostname: string;|新增|-|
|语言编译器运行时-URL|href: string;|新增|-|
|语言编译器运行时-URL|readonly origin: string;|新增|-|
|语言编译器运行时-URL|password: string;|新增|-|
|语言编译器运行时-URL|pathname: string;|新增|-|
|语言编译器运行时-URL|port: string;|新增|-|
|语言编译器运行时-URL|protocol: string;|新增|-|
|语言编译器运行时-URL|search: string;|新增|-|
|语言编译器运行时-URL|readonly searchParams: URLSearchParams;|新增|-|
|语言编译器运行时-URL|username: string;|新增|-|
|语言编译器运行时-ChildProcess|readonly pid: number;|新增|-|
|语言编译器运行时-ChildProcess|readonly ppid: number;|新增|-|
|语言编译器运行时-ChildProcess|readonly exitCode: number;|新增|-|
|语言编译器运行时-ChildProcess|readonly killed: boolean;|新增|-|
|语言编译器运行时-ChildProcess|wait(): Promise<number>;|新增|-|
|语言编译器运行时-ChildProcess|getOutput(): Promise<Uint8Array>;|新增|-|
|语言编译器运行时-ChildProcess|getErrorOutput(): Promise<Uint8Array>;|新增|-|
|语言编译器运行时-ChildProcess|close(): void;|新增|-|
|语言编译器运行时-ChildProcess|kill(signal: number): void;|新增|-|
|语言编译器运行时-process|runCmd(command: string,options?: { timeout : number, killSignal : number / string, maxBuffer : number }): ChildProcess;|新增|-|
|语言编译器运行时-process|getPid(): number;|新增|-|
|语言编译器运行时-process|getPpid(): number;|新增|-|
|语言编译器运行时-process|abort(): void;|新增|-|
|语言编译器运行时-process|on(type: string, listener: EventListener): void;|新增|-|
|语言编译器运行时-process|exit(code?:number): void;|新增|-|
|语言编译器运行时-process|cwd(): string;|新增|-|
|语言编译器运行时-process|chdir(dir: string): void;|新增|-|
|语言编译器运行时-process|getEgid(): number;|新增|-|
|语言编译器运行时-process|getEuid(): number;|新增|-|
|语言编译器运行时-process|getGid(): number|新增|-|
|语言编译器运行时-process|getUid(): number;|新增|-|
|语言编译器运行时-process|uptime(): number;|新增|-|
|语言编译器运行时-process|getGroups(): number[];|新增|-|
|语言编译器运行时-process|kill(signal?: number, pid?: number): boolean;|新增|-|
|升级服务子系统-Updater|checkNewVersion(): Promise<NewVersionInfo>;|新增|    -|
|升级服务子系统-Updater|rebootAndCleanUserData(callback: AsyncCallback<number>): void;|新增|    -|
|升级服务子系统-Updater|rebootAndCleanCache(): Promise<number>;|新增|    -|
|升级服务子系统-Updater|function getUpdaterFromOther(device: string, updateType?: UpdateTypes): Updater;|新增|    -|
|升级服务子系统-Updater|cancel(): void;|新增|    -|
|升级服务子系统-Updater|upgrade(): void;|新增|    -|
|升级服务子系统-Updater|off(eventType: 'downloadProgress', callback?: UpdateProgressCallback): void;|新增|    -|
|升级服务子系统-Updater|getUpdatePolicy(callback: AsyncCallback<UpdatePolicy>): void;|新增|    -|
|升级服务子系统-Updater|function getUpdaterForOther(device: string, updateType?: UpdateTypes): Updater;|新增|    -|
|升级服务子系统-Updater|setUpdatePolicy(policy: UpdatePolicy, callback: AsyncCallback<number>): void;|新增|    -|
|升级服务子系统-Updater|getNewVersionInfo(): Promise<NewVersionInfo>;|新增|    -|
|升级服务子系统-Updater|function getUpdater(updateType?: UpdateTypes): Updater;|新增|    -|
|升级服务子系统-Updater|applyNewVersion(callback: AsyncCallback<number>): void;|新增|    -|
|升级服务子系统-Updater|rebootAndCleanUserData(): Promise<number>;|新增|    -|
|升级服务子系统-Updater|off(eventType: 'verifyProgress', callback?: UpdateProgressCallback): void;|新增|    -|
|升级服务子系统-Updater|on(eventType: 'upgradeProgress', callback: UpdateProgressCallback): void;|新增|    -|
|升级服务子系统-Updater|checkNewVersion(callback: AsyncCallback<NewVersionInfo>): void;|新增|    -|
|升级服务子系统-Updater|on(eventType: 'downloadProgress', callback: UpdateProgressCallback): void;|新增|    -|
|升级服务子系统-Updater|getUpdatePolicy(): Promise<UpdatePolicy>;|新增|    -|
|升级服务子系统-Updater|download(): void;|新增|    -|
|升级服务子系统-Updater|off(eventType: 'upgradeProgress', callback?: UpdateProgressCallback): void;|新增|    -|
|升级服务子系统-Updater|getNewVersionInfo(callback: AsyncCallback<NewVersionInfo>): void;|新增|    -|
|升级服务子系统-Updater|on(eventType: 'verifyProgress', callback: UpdateProgressCallback): void;|新增|    -|
|升级服务子系统-Updater|verifyUpdatePackage(upgradeFile: string, certsFile: string): void;|新增|    -|
|升级服务子系统-Updater|setUpdatePolicy(policy: UpdatePolicy): Promise<number>;|新增|    -|
|升级服务子系统-Updater|rebootAndCleanCache(callback: AsyncCallback<number>): void;|新增|    -|
|升级服务子系统-Updater|applyNewVersion(): Promise<number>;|新增|    -|
|全球化子系统-I18n|getSystemLanguages(): Array<string>;|新增|    -|
|全球化子系统-I18n|getSystemCountries(language: string): Array<string>;|新增|    -|
|全球化子系统-I18n|isSuggested(language: string, region?: string): boolean;|新增|    -|
|全球化子系统-I18n|getSystemLanguage(): string;|新增|    -|
|全球化子系统-I18n|setSystemLanguage(language: string);|新增|    -|
|全球化子系统-I18n|getSystemRegion(): string;|新增|    -|
|全球化子系统-I18n|setSystemRegion(region: string);|新增|    -|
|全球化子系统-I18n|"getDisplayCountry(locale: string, displayLocale: string,sentenceCase?: boolean): string;"|新增|    -|
|全球化子系统-I18n|getSystemLocale(): string;|新增|    -|
|全球化子系统-I18n|setSystemLocale(locale: string);|新增|    -|
|全球化子系统-I18n|"getDisplayLanguage(locale: string, displayLocale: string,sentenceCase?: boolean): string;"|新增|    -|
|电话子系统_radio|getNetworkState(callback: AsyncCallback<NetworkState>): void;getNetworkState(slotId: number, callback: AsyncCallback<NetworkState>): void;getNetworkState(slotId?: number): Promise<NetworkState>;|新增| -|
|电话子系统_sim|getSimAccountInfo(slotId: number, callback: AsyncCallback<IccAccountInfo>): void;getSimAccountInfo(slotId: number): Promise<IccAccountInfo>;|新增| -|
|电话子系统_sim|getDefaultVoiceSlotId(callback: AsyncCallback<number>): void;getDefaultVoiceSlotId(): Promise<number>;|新增| -|
|电话子系统_sim|getSimSpn(slotId: number, callback: AsyncCallback<string>): void;getSimSpn(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getISOCountryCodeForSim(slotId: number, callback: AsyncCallback<string>): void;getISOCountryCodeForSim(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getSimIccId(slotId: number, callback: AsyncCallback<string>): void;getSimIccId(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getSimGid1(slotId: number, callback: AsyncCallback<string>): void;getSimGid1(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getISOCountryCodeForSim(slotId: number, callback: AsyncCallback<string>): void;getISOCountryCodeForSim(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getSimOperatorNumeric(slotId: number, callback: AsyncCallback<string>): void;getSimOperatorNumeric(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getSimSpn(slotId: number, callback: AsyncCallback<string>): void;getSimSpn(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getSimIccId(slotId: number, callback: AsyncCallback<string>): void;getSimIccId(slotId: number): Promise<string>;|新增| -|
|电话子系统_sim|getIMSI(slotId: number, callback: AsyncCallback<string>): void;getIMSI(slotId: number): Promise<string>;|新增| -|
|电话子系统_call|combineConference(callId: number, callback: AsyncCallback<void>): void;combineConference(callId: number): Promise<void>;|新增| -|
|电话子系统_call|startDTMF(callId: number, character: string, callback: AsyncCallback<void>): void;startDTMF(callId: number, character: string): Promise<void>;|新增| -|
|电话子系统_call|stopDTMF(callId: number, callback: AsyncCallback<void>): void;stopDTMF(callId: number): Promise<void>;|新增| -|
|电话子系统_sim|setDefaultVoiceSlotId(slotId: number, callback: AsyncCallback<void>): void;setDefaultVoiceSlotId(slotId: number): Promise<void>;|新增| -|
|电话子系统_sim|unlockPin(slotId: number, pin: string, callback: AsyncCallback<LockStatusResponse>): void;unlockPin(slotId: number, pin: string): Promise<LockStatusResponse>;|新增| -|
|电话子系统_sim|alterPin(slotId: number, newPin: string, oldPin: string, callback: AsyncCallback<LockStatusResponse>): void;alterPin(slotId: number, newPin: string, oldPin: string): Promise<LockStatusResponse>;|新增| -|
|电话子系统_sim|setLockState(slotId: number, pin: string, enable: number, callback: AsyncCallback<LockStatusResponse>): void;setLockState(slotId: number, pin: string, enable: number): Promise<LockStatusResponse>;|新增| -|
|电话子系统_sim|getSimState(slotId: number, callback: AsyncCallback<SimState>): void;getSimState(slotId: number): Promise<SimState>;|新增| -|
|电话子系统_sim|getSimState(slotId: number, callback: AsyncCallback<SimState>): void;getSimState(slotId: number): Promise<SimState>;|新增| -|
|电话子系统_sim|getSimState(slotId: number, callback: AsyncCallback<SimState>): void;getSimState(slotId: number): Promise<SimState>;|新增| -|
|电话子系统_sim|getSimState(slotId: number, callback: AsyncCallback<SimState>): void;getSimState(slotId: number): Promise<SimState>;|新增| -|
|电话子系统_call|isEmergencyPhoneNumber(phoneNumber: string, callback: AsyncCallback<boolean>): void;isEmergencyPhoneNumber(phoneNumber: string, options: EmergencyNumberOptions, callback: AsyncCallback<boolean>): void;isEmergencyPhoneNumber(phoneNumber: string, options?: EmergencyNumberOptions): Promise<boolean>;|新增| -|
|电话子系统_sms|createMessage(pdu: Array<number>, specification: string, callback: AsyncCallback<ShortMessage>): void;createMessage(pdu: Array<number>, specification: string): Promise<ShortMessage>;|新增| -|
|电话子系统_call|hasCall(callback: AsyncCallback<boolean>): void;hasCall(): Promise<boolean>;|新增| -|
|电话子系统_sms|sendMessage(options: SendMessageOptions): void;|新增| -|
|电话子系统_call|dial(phoneNumber: string, callback: AsyncCallback<boolean>): void;dial(phoneNumber: string, options: DialOptions, callback: AsyncCallback<boolean>): void;dial(phoneNumber: string, options?: DialOptions): Promise<boolean>;|新增| -|
|电话子系统_call|interface DialOptions {extras?: boolean;  }|新增| -|
|电话子系统_sms|sendMessage(options: SendMessageOptions): void;|新增| -|
|电话子系统_sms|getDefaultSmsSlotId(callback: AsyncCallback<number>): void;getDefaultSmsSlotId(): Promise<number>;|新增| -|
|电话子系统_call|formatPhoneNumber(phoneNumber: string, callback: AsyncCallback<string>): void;formatPhoneNumber(phoneNumber: string, options: NumberFormatOptions, callback: AsyncCallback<string>): void;formatPhoneNumber(phoneNumber: string, options?: NumberFormatOptions): Promise<string>;|新增| -|
|电话子系统_call|formatPhoneNumber(phoneNumber: string, callback: AsyncCallback<string>): void;formatPhoneNumber(phoneNumber: string, options: NumberFormatOptions, callback: AsyncCallback<string>): void;formatPhoneNumber(phoneNumber: string, options?: NumberFormatOptions): Promise<string>;|新增| -|
|电话子系统_call|formatPhoneNumberToE164(phoneNumber: string, countryCode: string, callback: AsyncCallback<string>): void;formatPhoneNumberToE164(phoneNumber: string, countryCode: string): Promise<string>;|新增| -|
|电话子系统_sms|setDefaultSmsSlotId(slotId: number, callback: AsyncCallback<void>): void;setDefaultSmsSlotId(slotId: number): Promise<void>;|新增| -|
|电话子系统_call|getCallState(callback: AsyncCallback<CallState>): void;getCallState(): Promise<CallState>;|新增| -|
|电话子系统_sms|setSmscAddr(slotId: number, smscAddr: string, callback: AsyncCallback<void>): void;setSmscAddr(slotId: number, smscAddr: string): Promise<void>;|新增| -|
|电话子系统_sms|getSmscAddr(slotId: number, callback: AsyncCallback<string>): void;getSmscAddr(slotId: number): Promise<string>;|新增| -|
|电话子系统_sms|addSimMessage(options: SimMessageOptions, callback: AsyncCallback<void>): void;addSimMessage(options: SimMessageOptions): Promise<void>;|新增| -|
|电话子系统_sms|delSimMessage(slotId: number, msgIndex: number, callback: AsyncCallback<void>): void;delSimMessage(slotId: number, msgIndex: number): Promise<void>;|新增| -|
|电话子系统_radio|getISOCountryCodeForNetwork(slotId: number, callback: AsyncCallback<string>): void;getISOCountryCodeForNetwork(slotId: number): Promise<string>;|新增| -|
|电话子系统_sms|updateSimMessage(options: UpdateSimMessageOptions, callback: AsyncCallback<void>): void;updateSimMessage(options: UpdateSimMessageOptions): Promise<void>;|新增| -|
|电话子系统_radio|getISOCountryCodeForNetwork(slotId: number, callback: AsyncCallback<string>): void;getISOCountryCodeForNetwork(slotId: number): Promise<string>;|新增| -|
|电话子系统_sms|getAllSimMessages(slotId: number, callback: AsyncCallback<Array<SimShortMessage>>): void;getAllSimMessages(slotId: number): Promise<Array<SimShortMessage>>;|新增| -|
|电话子系统_call|isInEmergencyCall(callback: AsyncCallback<boolean>): void;isInEmergencyCall(): Promise<boolean>;|新增| -|
|电话子系统_sms|setCBConfig(options: CBConfigOptions, callback: AsyncCallback<void>): void;setCBConfig(options: CBConfigOptions): Promise<void>;|新增| -|
|电话子系统_call|answer(callId: number, callback: AsyncCallback<void>): void;answer(callId: number): Promise<void>;|新增| -|
|电话子系统_call|hangup(callId: number, callback: AsyncCallback<void>): void;hangup(callId: number): Promise<void>;|新增| -|
|电话子系统_call|reject(callId: number, callback: AsyncCallback<void>): void;reject(callId: number, options: RejectMessageOptions, callback: AsyncCallback<void>): void;reject(callId: number, options?: RejectMessageOptions): Promise<void>;|新增| -|
|电话子系统_call|holdCall(callId: number, callback: AsyncCallback<void>): void;holdCall(callId: number): Promise<void>;|新增| -|
|电话子系统_call|unHoldCall(callId: number, callback: AsyncCallback<void>): void;unHoldCall(callId: number): Promise<void>;|新增| -|
|电话子系统_call|switchCall(callId: number, callback: AsyncCallback<void>): void;switchCall(callId: number): Promise<void>;|新增| -|
|电话子系统_radio|setNetworkSelectionMode(options: NetworkSelectionModeOptions, callback: AsyncCallback<void>): void;setNetworkSelectionMode(options: NetworkSelectionModeOptions): Promise<void>;|新增| -|
|电话子系统_radio|getNetworkSearchInformation(slotId: number, callback: AsyncCallback<NetworkSearchResult>): void;getNetworkSearchInformation(slotId: number): Promise<NetworkSearchResult>;|新增| -|
|电话子系统_radio|getNetworkSelectionMode(slotId: number, callback: AsyncCallback<NetworkSelectionMode>): void;getNetworkSelectionMode(slotId: number): Promise<NetworkSelectionMode>;|新增| -|
|电话子系统_radio|isRadioOn(callback: AsyncCallback<boolean>): void;isRadioOn(): Promise<boolean>;|新增| -|
|电话子系统_radio|turnOnRadio(callback: AsyncCallback<void>): void;turnOnRadio(): Promise<void>;|新增| -|
|电话子系统_radio|turnOffRadio(callback: AsyncCallback<void>): void;turnOffRadio(): Promise<void>;|新增| -|
|电话子系统_radio|getSignalInformation(slotId: number, callback: AsyncCallback<Array<SignalInformation>>): void;getSignalInformation(slotId: number): Promise<Array<SignalInformation>>;|新增| -|
|电话子系统_radio|getRadioTech(slotId: number, callback: AsyncCallback<{psRadioTech: RadioTechnology, csRadioTech: RadioTechnology}>): void;getRadioTech(slotId: number): Promise<{psRadioTech: RadioTechnology, csRadioTech: RadioTechnology}>;|新增| -|
|电话子系统_radio|getRadioTech(slotId: number, callback: AsyncCallback<{psRadioTech: RadioTechnology, csRadioTech: RadioTechnology}>): void;getRadioTech(slotId: number): Promise<{psRadioTech: RadioTechnology, csRadioTech: RadioTechnology}>;|新增| -|
|电话子系统_radio|getRadioTech(slotId: number, callback: AsyncCallback<{psRadioTech: RadioTechnology, csRadioTech: RadioTechnology}>): void;getRadioTech(slotId: number): Promise<{psRadioTech: RadioTechnology, csRadioTech: RadioTechnology}>;|新增| -|
|ohos.data.distributeddata|put(key:string, value:Uint8Array / string / boolean / number, callback: AsyncCallback<void>):void put(key:string, value:Uint8Array / string / boolean / number):Promise<void>|新增| -|
|ohos.data.distributeddata|delete(key: string, callback: AsyncCallback<void>): void delete(key: string): Promise<void>|新增| -|
|ohos.data.distributeddata|on(event:'dataChange', subType: SubscribeType, observer: Callback<ChangeNotification>): void|新增| -|
|ohos.data.distributeddata|get(key:string, callback:AsyncCallback<Uint8Array / string / boolean / number>):void get(key:string):Promise<Uint8Array / string / boolean / number>|新增| -|
|ohos.data.distributeddata|sync(deviceIdList:string[], mode:SyncMode, allowedDelayMs?:number):void|新增| -|
|ohos.data.distributeddata|createKVManager(config: KVManagerConfig, callback: AsyncCallback<KVManager>): void;createKVManager(config: KVManagerConfig): Promise<KVManager>;|新增| -|
|ohos.data.distributeddata|getKVStore<T extends KVStore>(options: Options, storeId: string): Promise<T>;getKVStore<T extends KVStore>(options: Options, storeId: string, callback: AsyncCallback<T>): void;|新增| -|
|ohos.data.distributeddata|on(event:'syncComplete', syncCallback: Callback<Array<[string, number]>>):void|新增| -|
|ohos.data.rdb|type ValueType = number / string / boolean;|新增| -|
|ohos.data.rdb|type ValuesBucket = { [key: string]: ValueType / Uint8Array / null; }|新增| -|
|ohos.data.rdb|name: string;|新增| -|
|ohos.data.rdb|constructor(name: string)|新增| -|
|ohos.data.rdb|equalTo(field: string, value: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|notEqualTo(field: string, value: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|beginWrap(): RdbPredicates;|新增| -|
|ohos.data.rdb|endWrap(): RdbPredicates;|新增| -|
|ohos.data.rdb|function getRdbStore(config: StoreConfig, version: number, callback: AsyncCallback<RdbStore>): void;function getRdbStore(config: StoreConfig, version: number): Promise<RdbStore>;|新增| -|
|ohos.data.rdb|function deleteRdbStore(name: string, callback: AsyncCallback<void>): void;function deleteRdbStore(name: string): Promise<void>;|新增| -|
|ohos.data.rdb|insert(name: string, values: ValuesBucket, callback: AsyncCallback<number>): void;insert(name: string, values: ValuesBucket): Promise<number>;|新增| -|
|ohos.data.rdb|update(values: ValuesBucket, rdbPredicates: RdbPredicates, callback: AsyncCallback<number>): void;update(values: ValuesBucket, rdbPredicates: RdbPredicates): Promise<number>;|新增| -|
|ohos.data.rdb|delete(rdbPredicates: RdbPredicates, callback: AsyncCallback<number>): void;delete(rdbPredicates: RdbPredicates): Promise<number>;|新增| -|
|ohos.data.rdb|query(rdbPredicates: RdbPredicates, columns: Array<string>, callback: AsyncCallback<ResultSet>): void;query(rdbPredicates: RdbPredicates, columns: Array<string>): Promise<ResultSet>;|新增| -|
|ohos.data.rdb|executeSql(sql: string, bindArgs: Array<ValueType>, callback: AsyncCallback<void>): void;executeSql(sql: string, bindArgs: Array<ValueType>): Promise<void>;|新增| -|
|ohos.data.rdb|like(field: string, value: string): RdbPredicates;|新增| -|
|ohos.data.rdb|glob(field: string, value: string): RdbPredicates;|新增| -|
|ohos.data.rdb|between(field: string, low: ValueType, high: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|notBetween(field: string, low: ValueType, high: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|greaterThan(field: string, value: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|lessThan(field: string, value: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|greaterThanOrEqualTo(field: string, value: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|lessThanOrEqualTo(field: string, value: ValueType): RdbPredicates;|新增| -|
|ohos.data.rdb|or(): RdbPredicates;|新增| -|
|ohos.data.rdb|and(): RdbPredicates;|新增| -|
|ohos.data.rdb|contains(field: string, value: string): RdbPredicates;|新增| -|
|ohos.data.rdb|beginsWith(field: string, value: string): RdbPredicates;|新增| -|
|ohos.data.rdb|endsWith(field: string, value: string): RdbPredicates;|新增| -|
|ohos.data.rdb|isNull(field: string): RdbPredicates;|新增| -|
|ohos.data.rdb|isNotNull(field: string): RdbPredicates;|新增| -|
|ohos.data.rdb|isEnded: boolean;|新增| -|
|ohos.data.rdb|isStarted: boolean;|新增| -|
|ohos.data.rdb|isClosed: boolean;|新增| -|
|ohos.data.rdb|getColumnIndex(columnName: string): number;|新增| -|
|ohos.data.rdb|getColumnName(columnIndex: number): string;|新增| -|
|ohos.data.rdb|goTo(offset: number): boolean;|新增| -|
|ohos.data.rdb|goToRow(position: number): boolean;|新增| -|
|ohos.data.rdb|goToFirstRow(): boolean;|新增| -|
|ohos.data.rdb|goToLastRow(): boolean;|新增| -|
|ohos.data.rdb|goToNextRow(): boolean;|新增| -|
|ohos.data.rdb|goToPreviousRow(): boolean;|新增| -|
|ohos.data.rdb|getBlob(columnIndex: number): Uint8Array;|新增| -|
|ohos.data.rdb|getString(columnIndex: number): string;|新增| -|
|ohos.data.rdb|getLong(columnIndex: number): number;|新增| -|
|ohos.data.rdb|getDouble(columnIndex: number): number;|新增| -|
|ohos.data.dataAbility|orderByDesc(field: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|distinct(): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|limitAs(value: number): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|offsetAs(rowOffset: number): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|groupBy(fields: Array<string>): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|indexedBy(field: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|in(field: string, value: Array<ValueType>): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|notIn(field: string, value: Array<ValueType>): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|glob(field: string, value: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|between(field: string, low: ValueType, high: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|notBetween(field: string, low: ValueType, high: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|greaterThan(field: string, value: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|lessThan(field: string, value: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|greaterThanOrEqualTo(field: string, value: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|lessThanOrEqualTo(field: string, value: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|orderByAsc(field: string): DataAbilityPredicates;|新增| -|
|ohos.data.rdb|isColumnNull(columnIndex: number): boolean;|新增| -|
|ohos.data.rdb|close(): void;|新增| -|
|ohos.data.dataAbility|function createRdbPredicates(name: string, dataAbilityPredicates: DataAbilityPredicates): rdb.RdbPredicates;|新增| -|
|ohos.data.dataAbility|equalTo(field: string, value: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|notEqualTo(field: string, value: ValueType): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|beginWrap():DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|endWrap(): DataAbilityPredicates;|新增| -|
|ohos.data.rdb|orderByAsc(field: string): RdbPredicates;|新增| -|
|ohos.data.rdb|orderByDesc(field: string): RdbPredicates;|新增| -|
|ohos.data.rdb|distinct(): RdbPredicates;|新增| -|
|ohos.data.rdb|limitAs(value: number): RdbPredicates;|新增| -|
|ohos.data.rdb|offsetAs(rowOffset: number): RdbPredicates;|新增| -|
|ohos.data.rdb|groupBy(fields: Array<string>): RdbPredicates;|新增| -|
|ohos.data.rdb|indexedBy(field: string): RdbPredicates;|新增| -|
|ohos.data.dataAbility|or(): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|and(): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|contains(field: string, value: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|beginsWith(field: string, value: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|endsWith(field: string, value: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|isNull(field: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|isNotNull(field: string): DataAbilityPredicates;|新增| -|
|ohos.data.dataAbility|like(field: string, value: string): DataAbilityPredicates;|新增| -|
|ohos.data.rdb|in(field: string, value: Array<ValueType>): RdbPredicates;|新增| -|
|ohos.data.rdb|notIn(field: string, value: Array<ValueType>): RdbPredicates;|新增| -|
|ohos.data.rdb|columnNames: Array<string>;|新增| -|
|ohos.data.rdb|columnCount: number;|新增| -|
|ohos.data.rdb|rowCount: number;|新增| -|
|ohos.data.rdb|rowIndex: number;|新增| -|
|ohos.data.rdb|isAtFirstRow: boolean;|新增| -|
|ohos.data.rdb|isAtLastRow: boolean;|新增| -|
|ohos.notification|title: string;|新增| -|
|ohos.notification|sound?: string;|新增| -|
|ohos.notification|text: string;|新增| -|
|ohos.notification|vibrationValues?: Array<number>;|新增| -|
|ohos.wantAgent|want?: Want;|新增| -|
|ohos.notification|vibrationEnabled?: boolean;|新增| -|
|ohos.notification|badgeFlag?: boolean;|新增| -|
|ohos.notification|type: notification.SlotType;|新增| -|
|ohos.wantAgent|code: number;|新增| -|
|ohos.notification|contentType: ContentType;|新增| -|
|ohos.notification|picture: image.PixelMap;|新增| -|
|ohos.notification|briefText: string;|新增| -|
|ohos.notification|briefText: string;|新增| -|
|ohos.notification|briefText: string;|新增| -|
|ohos.notification|bypassDnd?: boolean;|新增| -|
|ohos.notification|additionalText?: string;|新增| -|
|ohos.wantagent|function cancel(info: WantAgentInfo, callback: AsyncCallback<void>): void;|新增| -|
|ohos.wantAgent|enum OperationType|新增| -|
|ohos.wantAgent|enum WantAgentFlags|新增| -|
|ohos.wantAgent|permission?: string;|新增| -|
|ohos.notification|picture?: NotificationPictureContent;|新增| -|
|ohos.notification|normal?: NotificationBasicContent;|新增| -|
|ohos.notification|expandedTitle: string;|新增| -|
|ohos.notification|expandedTitle: string;|新增| -|
|ohos.wantAgent|function trigger(info: WantAgentInfo, triggerInfo: TriggerInfo, callback: AsyncCallback<CompleteData>): void;|新增| -|
|ohos.wantAgent|extraInfo?: {[key: string]: any};|新增| -|
|ohos.notification|multiLine?: NotificationMultiLineContent;|新增| -|
|ohos.notification|level?: notification.SlotLevel;|新增| -|
|ohos.notification|lightColor?: number;|新增| -|
|ohos.notification|lightEnabled?: boolean;|新增| -|
|ohos.notification|lines: Array<String>;|新增| -|
|ohos.notification|lockscreenVisibility?: number;|新增| -|
|ohos.notification|longText: string;|新增| -|
|ohos.wantAgent|function getBundleName(info: WantAgentInfo, callback: AsyncCallback<string>): void;|新增| -|
|ohos.notification|longText?: NotificationLongTextContent;|新增| -|
|ohos.notification|longTitle: string;|新增| -|
|ohos.wantAgent|function judgeEquality(info: WantAgentInfo, info2: WantAgentInfo, callback: AsyncCallback<boolean>): void;|新增| -|
|ohos.wantAgent|function getUid(info: WantAgentInfo, callback: AsyncCallback<number>): void;|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_TEMPERATURE_ABNORMAL = common.event.IVI_TEMPERATURE_ABNORMAL,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_VOLTAGE_RECOVERY = common.event.IVI_VOLTAGE_RECOVERY,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_TEMPERATURE_RECOVERY = common.event.IVI_TEMPERATURE_RECOVERY,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_ACTIVE = common.event.IVI_ACTIVE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USB_DEVICE_ATTACHED = usual.event.hardware.usb.action.USB_DEVICE_ATTACHED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USB_DEVICE_DETACHED = usual.event.hardware.usb.action.USB_DEVICE_DETACHED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_PAUSE = common.event.IVI_PAUSE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_STANDBY = common.event.IVI_STANDBY,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_LASTMODE_SAVE = common.event.IVI_LASTMODE_SAVE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_VOLTAGE_ABNORMAL = common.event.IVI_VOLTAGE_ABNORMAL,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_HIGH_TEMPERATURE = common.event.IVI_HIGH_TEMPERATURE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_EXTREME_TEMPERATURE = common.event.IVI_EXTREME_TEMPERATURE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DISK_UNMOUNTABLE = usual.event.data.DISK_UNMOUNTABLE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DISK_EJECT = usual.event.data.DISK_EJECT,|新增| -|
|ohos.commonEvent|COMMON_EVENT_VISIBLE_ACCOUNTS_UPDATED = usual.event.data.VISIBLE_ACCOUNTS_UPDATED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_ACCOUNT_DELETED = usual.event.data.ACCOUNT_DELETED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_FOUNDATION_READY = common.event.FOUNDATION_READY,|新增| -|
|ohos.commonEvent|COMMON_EVENT_AIRPLANE_MODE_CHANGED = usual.event.AIRPLANE_MODE|新增| -|
|ohos.commonEvent|COMMON_EVENT_USB_ACCESSORY_ATTACHED = usual.event.hardware.usb.action.USB_ACCESSORY_ATTACHED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USB_ACCESSORY_DETACHED = usual.event.hardware.usb.action.USB_ACCESSORY_DETACHED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DISK_REMOVED = usual.event.data.DISK_REMOVED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DISK_UNMOUNTED = usual.event.data.DISK_UNMOUNTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DISK_MOUNTED = usual.event.data.DISK_MOUNTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DISK_BAD_REMOVAL = usual.event.data.DISK_BAD_REMOVAL,|新增| -|
|ohos.commonEvent|COMMON_EVENT_NFC_ACTION_RF_FIELD_OFF_DETECTED = usual.event.nfc.action.RF_FIELD_OFF_DETECTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DISCHARGING = usual.event.DISCHARGING,|新增| -|
|ohos.commonEvent|COMMON_EVENT_CHARGING = usual.event.CHARGING,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DEVICE_IDLE_MODE_CHANGED = usual.event.DEVICE_IDLE_MODE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_POWER_SAVE_MODE_CHANGED = usual.event.POWER_SAVE_MODE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_ADDED = usual.event.USER_ADDED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_REMOVED = usual.event.USER_REMOVED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_ABILITY_ADDED = common.event.ABILITY_ADDED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_ABILITY_REMOVED = common.event.ABILITY_REMOVED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_ABILITY_UPDATED = common.event.ABILITY_UPDATED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_LOCATION_MODE_STATE_CHANGED = usual.event.location.MODE_STATE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_IVI_SLEEP = common.event.IVI_SLEEP,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_NAME_UPDATE = usual.event.bluetooth.host.NAME_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSINK_CONNECT_STATE_UPDATE = usual.event.bluetooth.a2dpsink.CONNECT_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSINK_PLAYING_STATE_UPDATE = usual.event.bluetooth.a2dpsink.PLAYING_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSINK_AUDIO_STATE_UPDATE = usual.event.bluetooth.a2dpsink.AUDIO_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_NFC_ACTION_ADAPTER_STATE_CHANGED = usual.event.nfc.action.ADAPTER_STATE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_NFC_ACTION_RF_FIELD_ON_DETECTED = usual.event.nfc.action.RF_FIELD_ON_DETECTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_REQ_ENABLE = usual.event.bluetooth.host.REQ_ENABLE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_REQ_DISABLE = usual.event.bluetooth.host.REQ_DISABLE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_SCAN_MODE_UPDATE = usual.event.bluetooth.host.SCAN_MODE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_DISCOVERY_STARTED = usual.event.bluetooth.host.DISCOVERY_STARTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_DISCOVERY_FINISHED = usual.event.bluetooth.host.DISCOVERY_FINISHED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_P2P_CONN_STATE = usual.event.wifi.p2p.CONN_STATE_CHANGE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_P2P_STATE_CHANGED = usual.event.wifi.p2p.STATE_CHANGE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_P2P_PEERS_STATE_CHANGED = usual.event.wifi.p2p.DEVICES_CHANGE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_P2P_CURRENT_DEVICE_STATE_CHANGED = usual.event.wifi.p2p.CURRENT_DEVICE_CHANGE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_P2P_GROUP_STATE_CHANGED = usual.event.wifi.p2p.GROUP_STATE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HANDSFREE_AG_CONNECT_STATE_UPDATE = usual.event.bluetooth.handsfree.ag.CONNECT_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HANDSFREE_AG_CURRENT_DEVICE_UPDATE = usual.event.bluetooth.handsfree.ag.CURRENT_DEVICE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HANDSFREE_AG_AUDIO_STATE_UPDATE = usual.event.bluetooth.handsfree.ag.AUDIO_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSOURCE_CONNECT_STATE_UPDATE = usual.event.bluetooth.a2dpsource.CONNECT_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSOURCE_CURRENT_DEVICE_UPDATE = usual.event.bluetooth.a2dpsource.CURRENT_DEVICE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_RSSI_VALUE = usual.event.wifi.RSSI_VALUE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_CONN_STATE = usual.event.wifi.CONN_STATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_HOTSPOT_STATE = usual.event.wifi.HOTSPOT_STATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_AP_STA_JOIN = usual.event.wifi.WIFI_HS_STA_JOIN,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_AP_STA_LEAVE = usual.event.wifi.WIFI_HS_STA_LEAVE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_MPLINK_STATE_CHANGE = usual.event.wifi.mplink.STATE_CHANGE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_HWID_LOGOUT = common.event.HWID_LOGOUT,|新增| -|
|ohos.commonEvent|COMMON_EVENT_HWID_TOKEN_INVALID = common.event.HWID_TOKEN_INVALID,|新增| -|
|ohos.commonEvent|COMMON_EVENT_HWID_LOGOFF = common.event.HWID_LOGOFF,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_POWER_STATE = usual.event.wifi.POWER_STATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_SCAN_FINISHED = usual.event.wifi.SCAN_FINISHED,|新增| -|
|ohos.commonEvent|clearAbortCommonEvent(): Promise<void>;|新增| -|
|ohos.commonEvent|bundleName?: string;|新增| -|
|ohos.commonEvent|code?: number;|新增| -|
|ohos.commonEvent|data?: string;|新增| -|
|ohos.commonEvent|subscriberPermissions?: Array<string>;|新增| -|
|ohos.commonEvent|isOrdered?: boolean;|新增| -|
|ohos.commonEvent|isSticky?: boolean;|新增| -|
|ohos.commonEvent|abortCommonEvent(callback: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|abortCommonEvent(): Promise<void>;|新增| -|
|ohos.commonEvent|function createSubscriber(subscribeInfo: CommonEventSubscribeInfo): Promise<CommonEventSubscriber>;|新增| -|
|ohos.commonEvent|function createSubscriber(subscribeInfo: CommonEventSubscribeInfo, callback: AsyncCallback<CommonEventSubscriber>): void;|新增| -|
|ohos.commonEvent|function subscribe(subscriber: CommonEventSubscriber, callback: AsyncCallback<CommonEventData>): void;|新增| -|
|ohos.commonEvent|function publish(event: string, options: CommonEventPublishData, callback: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|isOrderedCommonEvent(callback: AsyncCallback<boolean>): void;|新增| -|
|ohos.commonEvent|isOrderedCommonEvent(): Promise<boolean>;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BOOT_COMPLETED = usual.event.BOOT_COMPLETED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_LOCKED_BOOT_COMPLETED = usual.event.LOCKED_BOOT_COMPLETED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_SHUTDOWN = usual.event.SHUTDOWN,|新增| -|
|ohos.commonEvent|isStickyCommonEvent(): Promise<boolean>;|新增| -|
|ohos.commonEvent|getData(callback: AsyncCallback<string>): void;|新增| -|
|ohos.commonEvent|getData(): Promise<string>;|新增| -|
|ohos.commonEvent|getSubscribeInfo(callback: AsyncCallback<CommonEventSubscribeInfo>): void;|新增| -|
|ohos.commonEvent|getSubscribeInfo(): Promise<CommonEventSubscribeInfo>;|新增| -|
|ohos.commonEvent|function publish(event: string, callback: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|event: string|新增| -|
|ohos.commonEvent|bundleName?: string;|新增| -|
|ohos.commonEvent|code?: number;|新增| -|
|ohos.commonEvent|data?: string;|新增| -|
|ohos.commonEvent|setCode(code: number, callback: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|COMMON_EVENT_DRIVE_MODE = common.event.DRIVE_MODE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_HOME_MODE = common.event.HOME_MODE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_OFFICE_MODE = common.event.OFFICE_MODE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_STARTED = usual.event.USER_STARTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_BACKGROUND = usual.event.USER_BACKGROUND,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_FOREGROUND = usual.event.USER_FOREGROUND,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_SWITCHED = usual.event.USER_SWITCHED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_STARTING = usual.event.USER_STARTING,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_UNLOCKED = usual.event.USER_UNLOCKED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_STOPPING = usual.event.USER_STOPPING,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_STOPPED = usual.event.USER_STOPPED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_HWID_LOGIN = common.event.HWID_LOGIN,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_VERIFIED = usual.event.PACKAGE_VERIFIED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_EXTERNAL_APPLICATIONS_AVAILABLE = usual.event.EXTERNAL_APPLICATIONS_AVAILABLE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_EXTERNAL_APPLICATIONS_UNAVAILABLE = usual.event.EXTERNAL_APPLICATIONS_UNAVAILABLE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_CONFIGURATION_CHANGED = usual.event.CONFIGURATION_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_LOCALE_CHANGED = usual.event.LOCALE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_MANAGE_PACKAGE_STORAGE = usual.event.MANAGE_PACKAGE_STORAGE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGES_UNSUSPENDED = usual.event.PACKAGES_UNSUSPENDED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_MY_PACKAGE_SUSPENDED = usual.event.MY_PACKAGE_SUSPENDED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_MY_PACKAGE_UNSUSPENDED = usual.event.MY_PACKAGE_UNSUSPENDED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_UID_REMOVED = usual.event.UID_REMOVED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_FIRST_LAUNCH = usual.event.PACKAGE_FIRST_LAUNCH,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_NEEDS_VERIFICATION = usual.event.PACKAGE_NEEDS_VERIFICATION,|新增| -|
|ohos.commonEvent|COMMON_EVENT_SCREEN_OFF = usual.event.SCREEN_OFF,|新增| -|
|ohos.commonEvent|COMMON_EVENT_SCREEN_ON = usual.event.SCREEN_ON,|新增| -|
|ohos.commonEvent|COMMON_EVENT_USER_PRESENT = usual.event.USER_PRESENT,|新增| -|
|ohos.commonEvent|COMMON_EVENT_TIME_TICK = usual.event.TIME_TICK,|新增| -|
|ohos.commonEvent|COMMON_EVENT_TIME_CHANGED = usual.event.TIME_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_DATE_CHANGED = usual.event.DATE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BATTERY_CHANGED = usual.event.BATTERY_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BATTERY_LOW = usual.event.BATTERY_LOW,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BATTERY_OKAY = usual.event.BATTERY_OKAY,|新增| -|
|ohos.commonEvent|COMMON_EVENT_POWER_CONNECTED = usual.event.POWER_CONNECTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_POWER_DISCONNECTED = usual.event.POWER_DISCONNECTED,|新增| -|
|ohos.commonEvent|function unsubscribe(subscriber: CommonEventSubscriber, callback?: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_ACL_CONNECTED = usual.event.bluetooth.remotedevice.ACL_CONNECTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_ACL_DISCONNECTED = usual.event.bluetooth.remotedevice.ACL_DISCONNECTED,|新增| -|
|ohos.commonEvent|getAbortCommonEvent(callback: AsyncCallback<boolean>): void;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_NAME_UPDATE = usual.event.bluetooth.remotedevice.NAME_UPDATE,|新增| -|
|ohos.commonEvent|getAbortCommonEvent(): Promise<boolean>;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HANDSFREEUNIT_CONNECT_STATE_UPDATE = usual.event.bluetooth.handsfreeunit.CONNECT_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_PAIR_STATE = usual.event.bluetooth.remotedevice.PAIR_STATE,|新增| -|
|ohos.commonEvent|getCode(callback: AsyncCallback<number>): void;|新增| -|
|ohos.commonEvent|setCode(code: number): Promise<void>;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HANDSFREEUNIT_AUDIO_STATE_UPDATE = usual.event.bluetooth.handsfreeunit.AUDIO_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_BATTERY_VALUE_UPDATE = usual.event.bluetooth.remotedevice.BATTERY_VALUE_UPDATE,|新增| -|
|ohos.commonEvent|getCode(): Promise<number>;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HANDSFREEUNIT_AG_COMMON_EVENT = usual.event.bluetooth.handsfreeunit.AG_COMMON_EVENT,|新增| -|
|ohos.commonEvent|setCodeAndData(code: number, data: string, callback: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_SDP_RESULT = usual.event.bluetooth.remotedevice.SDP_RESULT,|新增| -|
|ohos.commonEvent|isStickyCommonEvent(callback: AsyncCallback<boolean>): void;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HANDSFREEUNIT_AG_CALL_STATE_UPDATE = usual.event.bluetooth.handsfreeunit.AG_CALL_STATE_UPDATE,|新增| -|
|ohos.commonEvent|setCodeAndData(code: number, data: string): Promise<void>;|新增| -|
|ohos.commonEvent|events: Array<string>;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_STATE_UPDATE = usual.event.bluetooth.host.STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSOURCE_PLAYING_STATE_UPDATE = usual.event.bluetooth.a2dpsource.PLAYING_STATE_UPDATE,|新增| -|
|ohos.commonEvent|setData(data: string, callback: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSOURCE_AVRCP_CONNECT_STATE_UPDATE = usual.event.bluetooth.a2dpsource.AVRCP_CONNECT_STATE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_HOST_REQ_DISCOVERABLE = usual.event.bluetooth.host.REQ_DISCOVERABLE,|新增| -|
|ohos.commonEvent|publisherPermission?: string;|新增| -|
|ohos.commonEvent|setData(data: string): Promise<void>;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_UUID_VALUE = usual.event.bluetooth.remotedevice.UUID_VALUE,|新增| -|
|ohos.commonEvent|publisherDeviceId?: string;|新增| -|
|ohos.commonEvent|clearAbortCommonEvent(callback: AsyncCallback<void>): void;|新增| -|
|ohos.commonEvent|userId?: number;|新增| -|
|ohos.commonEvent|COMMON_EVENT_TIMEZONE_CHANGED = usual.event.TIMEZONE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_PAIRING_REQ = usual.event.bluetooth.remotedevice.PAIRING_REQ,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_DISCOVERED = usual.event.bluetooth.remotedevice.DISCOVERED,|新增| -|
|ohos.commonEvent|priority?: number;|新增| -|
|ohos.commonEvent|COMMON_EVENT_BUNDLE_REMOVED = usual.event.BUNDLE_REMOVED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_CLASS_VALUE_UPDATE = usual.event.bluetooth.remotedevice.CLASS_VALUE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_PAIRING_CANCEL = usual.event.bluetooth.remotedevice.PAIRING_CANCEL,|新增| -|
|ohos.commonEvent|COMMON_EVENT_CLOSE_SYSTEM_DIALOGS = usual.event.CLOSE_SYSTEM_DIALOGS,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_ADDED = usual.event.PACKAGE_ADDED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_CONNECT_REQ = usual.event.bluetooth.remotedevice.CONNECT_REQ,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_FULLY_REMOVED = usual.event.PACKAGE_FULLY_REMOVED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_REPLACED = usual.event.PACKAGE_REPLACED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_CONNECT_REPLY = usual.event.bluetooth.remotedevice.CONNECT_REPLY,|新增| -|
|ohos.commonEvent|COMMON_EVENT_MY_PACKAGE_REPLACED = usual.event.MY_PACKAGE_REPLACED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_CHANGED = usual.event.PACKAGE_CHANGED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_REMOTEDEVICE_CONNECT_CANCEL = usual.event.bluetooth.remotedevice.CONNECT_CANCEL,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_REMOVED = usual.event.PACKAGE_REMOVED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_RESTARTED = usual.event.PACKAGE_RESTARTED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGE_DATA_CLEARED = usual.event.PACKAGE_DATA_CLEARED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_PACKAGES_SUSPENDED = usual.event.PACKAGES_SUSPENDED,|新增| -|
|ohos.commonEvent|COMMON_EVENT_BLUETOOTH_A2DPSOURCE_CODEC_VALUE_UPDATE = usual.event.bluetooth.a2dpsource.CODEC_VALUE_UPDATE,|新增| -|
|ohos.commonEvent|COMMON_EVENT_WIFI_P2P_PEERS_DISCOVERY_STATE_CHANGED = usual.event.wifi.p2p.PEER_DISCOVERY_STATE_CHANGE,|新增| -|
|ohos.notification|LEVEL_NONE = 0,|新增| -|
|ohos.notification|LEVEL_MIN = 1,|新增| -|
|ohos.notification|LEVEL_LOW = 2,|新增| -|
|ohos.notification|LEVEL_DEFAULT = 3,|新增| -|
|ohos.notification|label?: string;|新增| -|
|ohos.notification|bundle: string;|新增| -|
|ohos.notification|uid?: number;|新增| -|
|ohos.notification|NOTIFICATION_CONTENT_MULTILINE,|新增| -|
|ohos.notification|UNKNOWN_TYPE = 0,|新增| -|
|ohos.notification|SOCIAL_COMMUNICATION = 1,|新增| -|
|ohos.notification|LEVEL_HIGH = 4,|新增| -|
|ohos.notification|NOTIFICATION_CONTENT_BASIC_TEXT,|新增| -|
|ohos.notification|NOTIFICATION_CONTENT_LONG_TEXT,|新增| -|
|ohos.notification|NOTIFICATION_CONTENT_PICTURE,|新增| -|
|ohos.notification|isFloatingIcon?: boolean;|新增| -|
|ohos.notification|label?: string;|新增| -|
|ohos.notification|badgeIconStyle?: number;|新增| -|
|ohos.notification|showDeliveryTime?: boolean;|新增| -|
|ohos.notification|isAlertOnce?: boolean;|新增| -|
|ohos.notification|function getActiveNotifications(callback: AsyncCallback<Array<NotificationRequest>>): void;|新增| -|
|ohos.notification|isStopwatch?: boolean;|新增| -|
|ohos.notification|isCountDown?: boolean;|新增| -|
|ohos.notification|function getActiveNotifications(): Promise<Array<NotificationRequest>>;|新增| -|
|ohos.notification|function getActiveNotificationCount(callback: AsyncCallback<number>): void;|新增| -|
|ohos.notification|readonly creatorUid?: number;|新增| -|
|ohos.notification|function getActiveNotificationCount(): Promise<number>;|新增| -|
|ohos.notification|readonly creatorPid?: number;|新增| -|
|ohos.notification|function cancel(id: number, label?: string): Promise<void>;|新增| -|
|ohos.notification|classification?: string;|新增| -|
|ohos.notification|readonly hashCode?: string;|新增| -|
|ohos.notification|function cancelAll(callback: AsyncCallback<void>): void;|新增| -|
|ohos.notification|actionButtons?: Array<NotificationActionButton>;|新增| -|
|ohos.notification|function cancelAll(): Promise<void>;|新增| -|
|ohos.notification|smallIcon?: image.PixelMap;|新增| -|
|ohos.notification|isUnremovable?: boolean;|新增| -|
|ohos.notification|largeIcon?: image.PixelMap;|新增| -|
|ohos.notification|deliveryTime?: number;|新增| -|
|ohos.notification|readonly creatorBundleName?: string;|新增| -|
|ohos.notification|tapDismissed?: boolean;|新增| -|
|ohos.notification|function publish(request: NotificationRequest): Promise<void>;|新增| -|
|ohos.notification|autoDeletedTime?: number;|新增| -|
|ohos.notification|function cancel(id: number, callback: AsyncCallback<void>): void;|新增| -|
|ohos.notification|content: NotificationContent;|新增| -|
|ohos.notification|wantAgent?: WantAgentInfo;|新增| -|
|ohos.notification|function cancel(id: number, label: string, callback: AsyncCallback<void>): void;|新增| -|
|ohos.notification|function getSlot(slotType: SlotType, callback: AsyncCallback<NotificationSlot>): void;|新增| -|
|ohos.notification|extraInfo?: {[key: string]: any};|新增| -|
|ohos.notification|function getSlot(slotType: SlotType): Promise<NotificationSlot>;|新增| -|
|ohos.notification|SERVICE_INFORMATION = 2,|新增| -|
|ohos.notification|color?: number;|新增| -|
|ohos.notification|id?: number;|新增| -|
|ohos.notification|function getSlots(callback: AsyncCallback<Array<NotificationSlot>>): void;|新增| -|
|ohos.notification|CONTENT_INFORMATION = 3,|新增| -|
|ohos.notification|slotType?: notification.SlotType;|新增| -|
|ohos.notification|colorEnabled?: boolean;|新增| -|
|ohos.notification|OTHER_TYPES = 0xFFFF,|新增| -|
|ohos.notification|isOngoing?: boolean;|新增| -|
|ohos.notification|function addSlot(type: SlotType, callback: AsyncCallback<void>): void;|新增| -|
|ohos.notification|id: number;|新增| -|
|ohos.notification|function addSlot(type: SlotType): Promise<void>;|新增| -|
|ohos.notification|desc?: string;|新增| -|
|ohos.notification|function publish(request: NotificationRequest, callback: AsyncCallback<void>): void;|新增| -|
|ohos.notification|function removeAllSlots(callback: AsyncCallback<void>): void;|新增| -|
|ohos.notification|function removeAllSlots(): Promise<void>;|新增| -|
|ohos.notification|function getSlots(): Promise<Array<NotificationSlot>>;|新增| -|
|ohos.notification|function removeSlot(slotType: SlotType, callback: AsyncCallback<void>): void;|新增| -|
|ohos.notification|function removeSlot(slotType: SlotType): Promise<void>;|新增| -|
|全球化-资源管理-resourceManager|getString(resId: number, callback: AsyncCallback<string>);getString(resId: number): Promise<string>;|新增| -|
|全球化-资源管理-resourceManager|getStringArray(resId: number, callback: AsyncCallback<Array<string>>);getStringArray(resId: number): Promise<Array<string>>;|新增| -|
|全球化-资源管理-resourceManager|getConfiguration(callback: AsyncCallback<Configuration>);getConfiguration(): Promise<Configuration>;|新增| -|
|全球化-资源管理-resourceManager|getDeviceCapability(callback: AsyncCallback<DeviceCapability>);getDeviceCapability(): Promise<DeviceCapability>;|新增| -|
|全球化-资源管理-resourceManager|getMedia(resId: number, callback: AsyncCallback<Uint8Array>);getMedia(resId: number): Promise<Uint8Array>;getMediaBase64(resId: number, callback: AsyncCallback<string>);getMediaBase64(resId: number): Promise<string>;|新增| -|
|全球化-资源管理-resourceManager|"getPluralString(resId: number, num: number, callback: AsyncCallback<string>);getPluralString(resId: number, num: number): Promise<string>;"|新增| -|
|全球化-资源管理-resourceManager|DeviceCapability|新增| -|
|全球化-资源管理-resourceManager|"getMediaBase64(resId: number, callback: AsyncCallback<Uint8Array>);getMediaBase64(resId: number): Promise<Uint9Array>;"|新增| -|
|全球化-资源管理-resourceManager|"getResourceManager(callback: AsyncCallback<ResourceManager>);getResourceManager(bundleName: string, callback: AsyncCallback<ResourceManager>);getResourceManager(): Promise<ResourceManager>;getResourceManager(bundleName: string): Promise<ResourceManager>;"|新增| -|
|全球化-资源管理-resourceManager|DeviceType|新增| -|
|全球化-资源管理-resourceManager|Direction|新增| -|
|全球化-资源管理-resourceManager|Configuration|新增| -|
|全球化-资源管理-resourceManager|ScreenDensity|新增| -|
|全球化-资源管理-resourceManager|deviceType|新增| -|
|全球化-资源管理-resourceManager|locale|新增| -|
|全球化-资源管理-resourceManager|direction|新增| -|
|全球化-资源管理-resourceManager|screenDensity|新增| -|
|ohos.batteryInfo|batteryInfo:const batterySOC: number;|新增| -|
|ohos.batteryInfo|batteryInfo:const technology: string;|新增| -|
|ohos.batteryInfo|batteryInfo:const isBatteryPresent: boolean;|新增| -|
|ohos.batteryInfo|batteryInfo:const batteryTemperature: number;|新增| -|
|ohos.batteryInfo|batteryInfo:const pluggedType: BatteryPluggedType;|新增| -|
|ohos.batteryInfo|batteryInfo:const chargingStatus: BatteryChargeState;|新增| -|
|ohos.batteryInfo|batteryInfo:const healthStatus: BatteryHealthState;|新增| -|
|ohos.batteryInfo|batteryInfo:const voltage: number;|新增| -|
|ohos.batteryInfo|BatteryChargeState:NONE|新增| -|
|ohos.batteryInfo|BatteryChargeState:DISABLE|新增| -|
|ohos.batteryInfo|BatteryChargeState:ENABLE,|新增| -|
|ohos.batteryInfo|BatteryChargeState:FULL|新增| -|
|ohos.batteryInfo|BatteryHealthState:COLD|新增| -|
|ohos.batteryInfo|BatteryHealthState:OVERHEAT|新增| -|
|ohos.batteryInfo|BatteryHealthState:OVERVOLTAGE|新增| -|
|ohos.batteryInfo|BatteryHealthState:DEAD|新增| -|
|ohos.batteryInfo|BatteryHealthState:UNKNOWN|新增| -|
|ohos.batteryInfo|BatteryHealthState:GOOD|新增| -|
|ohos.batteryInfo|BatteryPluggedType:WIRELESS|新增| -|
|ohos.batteryInfo|BatteryPluggedType:NONE|新增| -|
|ohos.batteryInfo|BatteryPluggedType:AC|新增| -|
|ohos.batteryInfo|BatteryPluggedType:USB|新增| -|
|ohos.runningLock|RunningLock:unlock()|新增| -|
|ohos.runningLock|runningLock:isRunningLockTypeSupported(type: RunningLockType, callback: AsyncCallback<boolean>): void;|新增| -|
|ohos.runningLock|runningLock:createRunningLock(name: string, type: runningLockType): RunningLock|新增| -|
|ohos.runningLock|RunningLock:lock(timeout: number)|新增| -|
|ohos.runningLock|RunningLock:isUsed(): boolean|新增| -|
|ohos.runninglock|RunningLockType:BACKGROUND|新增| -|
|ohos.runninglock|RunningLockType:PROXIMITY_SCREEN_CONTROL|新增| -|
|ohos.power|power:rebootDevice(reason ?: string)|新增| -|
|ohos.power|power:isScreenOn(callback: AsyncCallback<boolean>): void;|新增| -|
|system.fetch|fetch|新增|-|
