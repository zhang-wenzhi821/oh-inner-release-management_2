

# OpenHarmony-V3.1.3-Release

## OpenHarmony_3.1.8.3版本转测试信息

| 转测试版本号：OpenHarmony_3.1.8.3                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.8.3版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/9/22                              |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.3/20220922_001143/version-Release_Version-OpenHarmony-3.1.8.3-20220922_001143-hispark_pegasus.tar.gz |
| L1 liteos：http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.3/20220922_001029/version-Release_Version-OpenHarmony-3.1.8.3-20220922_001029-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.3/20220922_001139/version-Release_Version-OpenHarmony-3.1.8.3-20220922_001139-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/9/22                                |
| **L2****转测试版本获取路径：                                 |
| L2 3516:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.3/20220922_021151/version-Release_Version-OpenHarmony-3.1.8.3-20220922_021151-hispark_taurus_L2.tar.gz |
| L2 RK3568: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.3/20220922_000628/version-Release_Version-OpenHarmony-3.1.8.3-20220922_000628-dayu200.tar.gz |
| sdk full(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.3/20220922_021028/version-Release_Version-OpenHarmony-3.1.8.3-20220922_021028-ohos-sdk.tar.gz |
| sdk full(MAC)API8:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.8.3/20220922041237/L2-SDK-MAC.tar.gz |
| sdk Public(windows+linux):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.8.3/20220922041237/L2-SDK-MAC.tar.gz |
| sdk Public(MAC)API8:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_API8/20220922055948/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.1.8.2版本转测试信息

| 转测试版本号：OpenHarmony_3.1.8.2                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.8.2版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/9/15                              |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.2/20220915_001027/version-Release_Version-OpenHarmony-3.1.8.2-20220915_001027-hispark_pegasus.tar.gz |
| L1 liteos：http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.2/20220915_001139/version-Release_Version-OpenHarmony-3.1.8.2-20220915_001139-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.2/20220915_001140/version-Release_Version-OpenHarmony-3.1.8.2-20220915_001140-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/9/15                                |
| **L2****转测试版本获取路径：                                 |
| L2 3516:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.2/20220915_021028/version-Release_Version-OpenHarmony-3.1.8.2-20220915_021028-hispark_taurus_L2.tar.gz |
| L2 RK3568: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.2/20220915_000625/version-Release_Version-OpenHarmony-3.1.8.2-20220915_000625-dayu200.tar.gz |
| sdk full(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.2/20220915_021037/version-Release_Version-OpenHarmony-3.1.8.2-20220915_021037-ohos-sdk.tar.gz |
| sdk full(MAC)API8:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.8.2/20220915043845/L2-SDK-MAC.tar.gz |
| sdk Public(windows+linux):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony-3.1.8.2/20220915_013024/version-Release_Version-OpenHarmony-3.1.8.2-20220915_013024-ohos-sdk-public.tar.gz |
| sdk Public(MAC)API8:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_API8/20220915070144/L2-SDK-MAC-PUBLIC.tar.gz |



## OpenHarmony_3.1.8.1版本转测试信息

| 转测试版本号：OpenHarmony_3.1.8.1                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.8.1版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/9/7                               |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.8.1/20220907_001146/version-Release_Version-OpenHarmony 3.1.8.1-20220907_001146-hispark_pegasus.tar.gz |
| L1 liteos： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.8.1/20220907_001027/version-Release_Version-OpenHarmony 3.1.8.1-20220907_001027-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.8.1/20220907_001025/version-Release_Version-OpenHarmony 3.1.8.1-20220907_001025-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/9/7                                 |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.8.1/20220907_021027/version-Release_Version-OpenHarmony 3.1.8.1-20220907_021027-hispark_taurus_L2.tar.gz |
| L2 RK3568: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.8.1/20220907_000629/version-Release_Version-OpenHarmony 3.1.8.1-20220907_000629-dayu200.tar.gz |
| sdk full(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.8.1/20220907_021014/version-Release_Version-OpenHarmony 3.1.8.1-20220907_021014-ohos-sdk.tar.gz |
| sdk full(MAC)API8:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.8.1/20220907053322/L2-SDK-MAC.tar.gz |



# OpenHarmony-V3.1.2-Release

## OpenHarmony_3.1.7.7版本转测试信息

| 转测试版本号：OpenHarmony_3.1.7.7                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.7.7版本:             |
| **API****变更：NA                                            |
| **L2****转测试版本获取路径：                                 |
| sdk full(windows+linux)API8:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.7/20220902_111922/version-Release_Version-OpenHarmony 3.1.7.7-20220902_111922-ohos-sdk.tar.gz |
| sdk full(MAC)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.7.7/20220902180621/L2-SDK-MAC.tar.gz |
| sdk public(windows+linux): http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.7/20220902_003043/version-Release_Version-OpenHarmony 3.1.7.7-20220902_003043-ohos-sdk.tar.gz |
| sdk public(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_API8/20220902053147/L2-SDK-MAC-PUBLIC.tar.gz |



## OpenHarmony_3.1.7.5版本转测试信息

| 转测试版本号：OpenHarmony_3.1.7.5                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.7.5版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/8/18                              |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220818_120051/version-Release_Version-OpenHarmony 3.1.7.5-20220818_120051-hispark_pegasus.tar.gz |
| L1 liteos： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220818_120140/version-Release_Version-OpenHarmony 3.1.7.5-20220818_120140-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220818_120031/version-Release_Version-OpenHarmony 3.1.7.5-20220818_120031-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/8/1                                 |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220818_130033/version-Release_Version-OpenHarmony 3.1.7.5-20220818_130033-hispark_taurus_L2.tar.gz |
| L2 RK3568: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220818_120027/version-Release_Version-OpenHarmony 3.1.7.5-20220818_120027-dayu200.tar.gz |
| sdk full(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220818_130029/version-Release_Version-OpenHarmony 3.1.7.5-20220818_130029-ohos-sdk.tar.gz |
| sdk full(MAC)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.7.5/20220818125848/L2-SDK-MAC.tar.gz |
| sdk public(windows+linux): http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.5/20220818_114313/version-Release_Version-OpenHarmony 3.1.7.5-20220818_114313-ohos-sdk-public.tar.gz |
| sdk public(mac):http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_API8/20220818155012/L2-SDK-MAC-PUBLIC.tar.gz |



## OpenHarmony_3.1.7.3(SP1)版本转测试信息

因SDK测试校验版本号不能包含特殊字符，SDK full和SDK public的Mac版本号保持为OpenHarmony_3.1.7.3

| 转测试版本号：OpenHarmony_3.1.7.3(SP1)                       |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.7.3(SP1)版本:        |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/7/28                              |
| **L0L1****转测试版本获取路径：                               |
| L0: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3(SP1)/20220728_000027/version-Release_Version-OpenHarmony 3.1.7.3(SP1)-20220728_000027-hispark_pegasus.tar.gz |
| L1 liteos： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3(SP1)/20220728_000031/version-Release_Version-OpenHarmony 3.1.7.3(SP1)-20220728_000031-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3(SP1)/20220728_000038/version-Release_Version-OpenHarmony 3.1.7.3(SP1)-20220728_000038-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/7/28                                |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3(SP1)/20220728_020028/version-Release_Version-OpenHarmony 3.1.7.3(SP1)-20220728_020028-hispark_taurus_L2.tar.gz |
| L2 RK3568: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3(SP1)/20220728_000026/version-Release_Version-OpenHarmony 3.1.7.3(SP1)-20220728_000026-dayu200.tar.gz |
| sdk full(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220729_104742/version-Release_Version-OpenHarmony 3.1.7.3-20220729_104742-ohos-sdk.tar.gz |
| sdk full(MAC)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.7.3/20220729161134/L2-SDK-MAC.tar.gz |
| sdk public(windows+linux): http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220729_104944/version-Release_Version-OpenHarmony 3.1.7.3-20220729_104944-ohos-sdk-public.tar.gz |
| sdk public(mac): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_API8/20220729132920/L2-SDK-MAC-PUBLIC.tar.gz |





## OpenHarmony_3.1.7.3版本转测试信息

| 转测试版本号：OpenHarmony_3.1.7.3                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.7.3版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/7/21                              |
| **L0L1****转测试版本获取路径：                               |
| L0：http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220721_000150/version-Release_Version-OpenHarmony 3.1.7.3-20220721_000150-hispark_pegasus.tar.gz |
| L1 liteos： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220721_000145/version-Release_Version-OpenHarmony 3.1.7.3-20220721_000145-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220721_000142/version-Release_Version-OpenHarmony 3.1.7.3-20220721_000142-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/7/21                                |
| **L2****转测试版本获取路径：                                 |
| L2 3516: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220721_020035/version-Release_Version-OpenHarmony 3.1.7.3-20220721_020035-hispark_taurus_L2.tar.gz |
| rk3568: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220721_000034/version-Release_Version-OpenHarmony 3.1.7.3-20220721_000034-dayu200.tar.gz |
| sdk full(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220721_020041/version-Release_Version-OpenHarmony 3.1.7.3-20220721_020041-ohos-sdk.tar.gz |
| sdk full(MAC)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.7.3/20220721033749/L2-SDK-MAC.tar.gz |
| sdk public(windows+linux): http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.3/20220720_220029/version-Release_Version-OpenHarmony 3.1.7.3-20220720_220029-ohos-sdk-public.tar.gz |
| sdk public(mac): http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_API8/20220722130249/L2-SDK-MAC-PUBLIC.tar.gz |

## OpenHarmony_3.1.7.2版本转测试信息

| 转测试版本号：OpenHarmony_3.1.7.2                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.7.2版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/7/14                              |
| **L0L1****转测试版本获取路径：                               |
| L0：http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.2/20220714_000203/version-Release_Version-OpenHarmony 3.1.7.2-20220714_000203-hispark_pegasus.tar.gz |
| L1 liteos:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.2/20220714_000148/version-Release_Version-OpenHarmony 3.1.7.2-20220714_000148-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.2/20220714_000035/version-Release_Version-OpenHarmony 3.1.7.2-20220714_000035-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/7/14                                |
| **L2****转测试版本获取路径：                                 |
| L2 3516:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.2/20220714_020032/version-Release_Version-OpenHarmony 3.1.7.2-20220714_020032-hispark_taurus_L2.tar.gz |
| rk3568:  http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.2/20220714_000155/version-Release_Version-OpenHarmony 3.1.7.2-20220714_000155-dayu200.tar.gz |
| sdk(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.2/20220714_020034/version-Release_Version-OpenHarmony 3.1.7.2-20220714_020034-ohos-sdk.tar.gz |
| sdk(MAC)API8:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.7.2/20220714033233/L2-SDK-MAC.tar.gz |



## OpenHarmony_3.1.7.1版本转测试信息

| 转测试版本号：OpenHarmony_3.1.7.1                            |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.7.1版本:             |
| **API****变更：NA                                            |
| **L0L1****转测试时间：2022/7/7                               |
| **L0L1****转测试版本获取路径：                               |
| L0：http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.1/20220707_000034/version-Release_Version-OpenHarmony 3.1.7.1-20220707_000034-hispark_pegasus.tar.gz |
| L1 liteos:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.1/20220707_000150/version-Release_Version-OpenHarmony 3.1.7.1-20220707_000150-hispark_taurus_LiteOS.tar.gz |
| L1 linux: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.1/20220707_000031/version-Release_Version-OpenHarmony 3.1.7.1-20220707_000031-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/7/7                                 |
| **L2****转测试版本获取路径：                                 |
| L2 3516:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.1/20220707_020155/version-Release_Version-OpenHarmony 3.1.7.1-20220707_020155-hispark_taurus_L2.tar.gz |
| rk3568:  http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.1/20220707_000030/version-Release_Version-OpenHarmony 3.1.7.1-20220707_000030-dayu200.tar.gz |
| sdk(windows+linux)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.7.1/20220707_020037/version-Release_Version-OpenHarmony 3.1.7.1-20220707_020037-ohos-sdk.tar.gz |
| sdk(MAC)API8: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.7.1/20220707105147/L2-SDK-MAC.tar.gz |

# OpenHarmony-V3.1.1-Release

## OpenHarmony_3.1.6.1版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.1.6.1 *****          |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.6.1版本:             |
| **API****变更：**：                                          |
| **L0L1****转测试时间：2022/5/12                              |
| **L0L1****转测试版本获取路径：                               |
| hispark_pegasus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.1/20220511_103510/version-Release_Version-OpenHarmony 3.1.6.1-20220511_103510-hispark_pegasus.tar.gz |
| hispark_taurus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.1/20220511_151024/version-Release_Version-OpenHarmony 3.1.6.1-20220511_151024-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.1/20220511_151218/version-Release_Version-OpenHarmony 3.1.6.1-20220511_151218-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022/5/12                                |
| **L2****转测试版本获取路径：                                 |
| L2-hi3516版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.1/20220511_103310/version-Release_Version-OpenHarmony 3.1.6.1-20220511_103310-hispark_taurus_L2.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.1/20220511_103337/version-Release_Version-OpenHarmony 3.1.6.1-20220511_103337-dayu200.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.1/20220511_103449/version-Release_Version-OpenHarmony 3.1.6.1-20220511_103449-ohos-sdk.tar.gz |
| sdk(mac):https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_3.1.6.1/20220511_114438/L2-SDK-MAC.tar.gz |


## OpenHarmony 3.1.6.1版本特性清单：

| no   | issue                                                        | feture description                                     | platform | sig                | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------- | :------- | :----------------- | :-------------------------------------------- |
| 1    | [I53CJX](https://gitee.com/openharmony/communication_netmanager_base/issues/I53CJX) | 【MR】【新增特性】支持兼容system.network | 标准系统 | SIG_Telephony | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 2    | [I53CKH](https://gitee.com/openharmony/communication_netstack/issues/I53CKH) | 【MR】【新增规格】支持兼容system.fetch | 标准系统 | SIG_Telephony | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 3    | [I53CKT](https://gitee.com/openharmony/communication_netstack/issues/I53CKT) | 【MR】【新增特性】支持WebSocket | 标准系统 | SIG_Telephony | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
|      |       |                    |          |      |       |
|      |       |                    |          |      |       |
|      |       |                    |          |      |       |


## OpenHarmony_3.1.6.2版本转测试信息：
| ********转测试版本号：    OpenHarmony_3.1.6.2 *****          |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.6.2版本:             |
| **API****变更：**：                                          |
| **L0L1****转测试时间：5/19                                   |
| **L0L1****转测试版本获取路径：                               |
| hispark_pegasus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2/20220518_120017/version-Release_Version-OpenHarmony 3.1.6.2-20220518_120017-hispark_pegasus.tar.gz |
| hispark_taurus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2/20220518_120027/version-Release_Version-OpenHarmony 3.1.6.2-20220518_120027-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2/20220518_120128/version-Release_Version-OpenHarmony 3.1.6.2-20220518_120128-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：5/19                                     |
| **L2****转测试版本获取路径：                                 |
| L2-hi3516版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2/20220518_130025/version-Release_Version-OpenHarmony 3.1.6.2-20220518_130025-hispark_taurus_L2.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2/20220518_123139/version-Release_Version-OpenHarmony 3.1.6.2-20220518_123139-dayu200.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2/20220520_143947/version-Release_Version-OpenHarmony 3.1.6.2-20220520_143947-ohos-sdk.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.6.2/20220520155900/L2-SDK-MAC.tar.gz |

## OpenHarmony 3.1.6.2版本特性清单：

| no   | issue                                                        | feture description                                     | platform | sig                | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------- | :------- | :----------------- | :-------------------------------------------- |
|      |       |                    |          |      |       |
|      |       |                    |          |      |       |



## OpenHarmony_3.1.6.2(sp1)版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.1.6.2(sp1) *****     |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.6.2(sp1)版本:        |
| **API****变更：**：                                          |
| **L0L1****转测试时间：5/23                                   |
| **L0L1****转测试版本获取路径：                               |
| hispark_pegasus： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2(sp1)/20220523_093635/version-Release_Version-OpenHarmony 3.1.6.2(sp1)-20220523_093635-hispark_pegasus.tar.gz |
| hispark_taurus_LiteOS： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2(sp1)/20220523_093548/version-Release_Version-OpenHarmony 3.1.6.2(sp1)-20220523_093548-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_Linux： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2(sp1)/20220523_093716/version-Release_Version-OpenHarmony 3.1.6.2(sp1)-20220523_093716-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：5/23                                     |
| **L2****转测试版本获取路径：                                 |
| hispark_taurus_L2： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2(sp1)/20220523_101135/version-Release_Version-OpenHarmony 3.1.6.2(sp1)-20220523_101135-hispark_taurus_L2.tar.gz |
| RK3568： http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.2(sp1)/20220523_101747/version-Release_Version-OpenHarmony 3.1.6.2(sp1)-20220523_101747-dayu200.tar.gz |

## OpenHarmony_3.1.6.3版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.1.6.3 *****          |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.6.3版本:             |
| **API****变更：**：                                          |
| **L0L1****转测试时间：5.26                                   |
| **L0L1****转测试版本获取路径：                               |
| hispark_pegasus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.3/20220526_000135/version-Release_Version-OpenHarmony 3.1.6.3-20220526_000135-hispark_pegasus.tar.gz |
| hispark_taurus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.3/20220526_000136/version-Release_Version-OpenHarmony 3.1.6.3-20220526_000136-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.3/20220526_000029/version-Release_Version-OpenHarmony 3.1.6.3-20220526_000029-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：5.26                                     |
| **L2****转测试版本获取路径：                                 |
| L2-hi3516版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.3/20220526_020030/version-Release_Version-OpenHarmony 3.1.6.3-20220526_020030-hispark_taurus_L2.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.3/20220526_000031/version-Release_Version-OpenHarmony 3.1.6.3-20220526_000031-dayu200.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.3/20220526_020029/version-Release_Version-OpenHarmony 3.1.6.3-20220526_020029-ohos-sdk.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.6.3/20220526034954/L2-SDK-MAC.tar.gz |

## OpenHarmony 3.1.6.3版本特性清单：

| no   | issue | feture description | platform | sig  | owner |
| :--- | ----- | :----------------- | :------- | :--- | :---- |
|      |       |                    |          |      |       |
|      |       |                    |          |      |       |

## OpenHarmony_3.1.6.5版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.1.6.5 *****          |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.6.5版本:             |
| **API****变更：**：                                          |
| **L0L1****转测试时间：5/31                                   |
| **L0L1****转测试版本获取路径：                               |
| hispark_pegasus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.5/20220531_152023/version-Release_Version-OpenHarmony 3.1.6.5-20220531_152023-hispark_pegasus.tar.gz |
| hispark_taurus版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.5/20220531_152148/version-Release_Version-OpenHarmony 3.1.6.5-20220531_152148-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.5/20220531_152117/version-Release_Version-OpenHarmony 3.1.6.5-20220531_152117-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：5/31                                     |
| **L2****转测试版本获取路径：                                 |
| L2-hi3516版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.5/20220531_152149/version-Release_Version-OpenHarmony 3.1.6.5-20220531_152149-hispark_taurus_L2.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.5/20220531_152019/version-Release_Version-OpenHarmony 3.1.6.5-20220531_152019-dayu200.tar.gz |
| sdk(windows+linux):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony 3.1.6.5/20220531_152026/version-Release_Version-OpenHarmony 3.1.6.5-20220531_152026-ohos-sdk.tar.gz |
| sdk(mac):http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_3.1.6.5/20220531164251/L2-SDK-MAC.tar.gz |

## OpenHarmony 3.1.6.5版本特性清单：

| no   | issue | feture description | platform | sig  | owner |
| :--- | ----- | :----------------- | :------- | :--- | :---- |
|      |       |                    |          |      |       |

 

## OpenHarmony_3.1.6.6版本转测试信息：

| ********转测试版本号：    OpenHarmony_3.1.6.16*****          |
| ------------------------------------------------------------ |
| **版本用途：**社区发布的OpenHarmony_3.1.6.6 SDK版本:         |
| **API****变更：**：                                          |
| **L0L1****转测试时间：NA                                     |
| **L0L1****转测试版本获取路径：NA                             |
| **L2****转测试时间：2022/6/6                                 |
| **L2****转测试版本获取路径：                                 |
| Mac SDK: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_API8/20220609232143/L2-SDK-MAC-PUBLIC.tar.gz |
| ohos sdk： http://download.ci.openharmony.cn/version/Master_Version/ohos-sdk-public/20220610_095121/version-Master_Version-ohos-sdk-public-20220610_095121-ohos-sdk-public.tar.gz |
