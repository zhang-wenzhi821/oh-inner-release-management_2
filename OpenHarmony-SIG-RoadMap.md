OpenHarmony社区版本发布计划





# OpenHarmony社区版本发布计划：

| **迭代计划**        | **版本**号                   | **版本构建** | **版本转测试** | **版本测试完成** |
| ------------------- | ---------------------------- | ------------ | -------------- | ---------------- |
| IT1（API Level：9） | OpenHarmony 3.2.1.1          | 2022/4/6   | 2022/4/7     | 2022/4/13        |
|                     | OpenHarmony 3.2.1.2          | 2022/4/13  | 2022/4/14     | 2022/4/20        |
|                     | OpenHarmony 3.2 .1.3          | 2022/4/20  | 2022/4/21     | 2022/4/27        |
| IT2（API Level：9） | OpenHarmony 3.2.2.1          | 2022/4/27    | 2022/4/28      | 2022/5/11        |
|                     | OpenHarmony 3.2.2.2          | 2022/5/11   | 2022/5/12     | 2022/5/18   |
|                     | OpenHarmony 3.2.2.3          | 2022/5/18   | 2022/5/19     | 2022/5/25       |
|                     | OpenHarmony 3.2.2.5(Beta1)          | 2022/5/25   | 2022/5/26     | **2022/5/30**       |
| IT3（API Level：9） | OpenHarmony 3.2.3.1          | 2022/6/1    | 2022/6/2      | 2022/6/8        |
|                     | OpenHarmony 3.2.3.2          | 2022/6/8    | 2022/6/9      | 2022/6/15       |
|                     | OpenHarmony 3.2.3.3          | 2022/6/15   | 2022/6/16     | 2022/6/22       |
|                     | OpenHarmony 3.2.3.5    | 2022/6/22   | 2022/6/23     | **2022/6/29**   |
| IT4（API Level：9） | OpenHarmony 3.2.5.1          | 2022/6/29     | 2022/6/30       | 2022/7/6        |
|                     | OpenHarmony 3.2.5.2          | 2022/7/6    | 2022/7/7      | 2022/7/13        |
|                     | OpenHarmony 3.2.5.3          | 2022/7/13    | 2022/7/14      | 2022/7/20        |
|                     | OpenHarmony 3.2.5.5(Beta2)          | 2022/7/20    | 2022/7/21      | **2022/7/30**    |
| IT5（API Level：9） | OpenHarmony 3.2.6.1          | 2022/8/3    | 2022/8/4      | 2022/8/10        |
|                     | OpenHarmony 3.2.6.2          | 2022/8/10    | 2022/8/11      | 2022/8/17         |
|                     | OpenHarmony 3.2.6.3          | 2022/8/17     | 2022/8/18       | 2022/8/24         |
|                     | OpenHarmony 3.2.6.5 | 2022/8/24     | 2022/8/25       | **2022/8/30**    |
| IT6（API Level：9） | OpenHarmony 3.2.7.1          | 2022/8/31    | 2022/9/1      | 2022/9/7        |
|                     | OpenHarmony 3.2.7.2          | 2022/9/7    | 2022/9/8      | 2022/9/14         |
|                     | OpenHarmony 3.2.7.3          | 2022/9/14     | 2022/9/15       | 2022/9/21         |
|                     | OpenHarmony 3.2.7.5(Beta3) | 2022/9/21     | 2022/9/22       | **2022/9/30**    |
| IT7（API Level：9） | OpenHarmony 3.2.8.1          | 2022/10/12    | 2022/10/13      | 2022/10/19        |
|                     | OpenHarmony 3.2.8.2          | 2022/10/19    | 2022/10/20      | 2022/10/26         |
|                     | OpenHarmony 3.2.8.3          | 2022/10/26     | 2022/10/27       | **2022/10/30**         |

# 各版本特性交付清单：

## OpenHarmony 3.2.1.1版本特性清单：

| no   | issue                                                        | feture description                         | platform     | sig                      | owner                                   |
| :--- | ------------------------------------------------------------ | :----------------------------------------- | :----------- | :----------------------- | :-------------------------------------- |
| 1    | [I50DBH](https://gitee.com/openharmony/aafwk_standard/issues/I50DBH) | 【新增特性】卡片数据支持携带图片           | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 2    | [I50DBT](https://gitee.com/openharmony/aafwk_standard/issues/I50DBT) | 【新增特性】FA卡片能力补齐-formManager重构 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |

## OpenHarmony 3.2.1.2版本特性清单：
| no   | issue                                                        | feture description                                     | platform     | sig                      | owner                                         |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------- | :----------- | :----------------------- | :-------------------------------------------- |
| 1    | [I524TT](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I524TT) | 【新增特性】支持对测试框架的配置                       | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 2    | [I524WG](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I524WG) | 【新增特性】支持系统应用不允许清除的能力               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 3    | [I524WP](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I524WP) | 【增强特性】查询指定应用是否安装                       | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 4    | [I52G5Q](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I52G5Q) | 【分布式组件管理】【MR】【DMS】添加群组校验机制        | 标准系统     | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege) |
| 5    | [I526UP](https://gitee.com/openharmony/powermgr_battery_manager/issues/I526UP) | 【MR】【新增特性】支持@system.battery 电池信息查询接口 | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)         |
| 6 | [I50DAQ](https://gitee.com/openharmony/aafwk_standard/issues/I50DAQ) | 【特性增强】卡片使用记录上报 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7    | [I50DOL](https://gitee.com/openharmony/aafwk_standard/issues/I50DOL) | 【增强特性】Stage模型支持Worker机制                    | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |

## OpenHarmony 3.2.1.3版本特性清单：
| no   | issue                                                        | feture description                                       | platform     | sig                      | owner                                   |
| :--- | ------------------------------------------------------------ | :------------------------------------------------------- | :----------- | :----------------------- | :-------------------------------------- |
| 1    | [I524UF](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I524UF) | 【新增特性】支持系统feature的升级（已安装entry的情况下） | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 2    | [I524VP](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I524VP) | 【新增特性】支持隐式查询的优先级                         | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 3    | [I526V9](https://gitee.com/openharmony/powermgr_power_manager/issues/I526V9) | 【增强特性】支持三方等设备的休眠接口                 | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)   |
| 4    | [I526VG](https://gitee.com/openharmony/powermgr_display_manager/issues/I526VG) | 【MR】【新增特性】支持@system.brightness亮度调节接口     | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)   |
| 5   | [I526VC](https://gitee.com/openharmony/powermgr_display_manager/issues/I526VC) | 【MR】【增强特性】支持应用通过窗口可以设置自己系统亮度   | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)   |
| 6   | [I526V1](https://gitee.com/openharmony/powermgr_battery_manager/issues/I526V1) | 【增强特性】提供读取瞬时电压、电流和电量的接口     | 标准系统     | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)   |
| 7    | [I4UD9W](https://gitee.com/openharmony/drivers_adapter/issues/I4UD9W) | 【增强特性】支持内核态动态驱动加载机制，提高设备驱动开发、调试效率 | 标准系统     | SIG_DriverFramework          | [@ji-fengfei](https://gitee.com/ji-fengfei)         |
| 8    | [I531ST](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I531ST) | 支持卡片卸载时，删除对应的卡片使用记录 | 标准系统     | SIG_BasicSoftwareService          | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)         |
| 9    | [I531SU](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I531SU) | 支持查询获取卡片使用记录 | 标准系统     | SIG_BasicSoftwareService          | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)         |
| 10    | [I531SV](https://gitee.com/openharmony/resourceschedule_device_usage_statistics/issues/I531SV) | 支持记录卡片的使用计数以及使用时间 | 标准系统     | SIG_BasicSoftwareService          | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)         |
| 11    | [I53CJX](https://gitee.com/openharmony/communication_netmanager_base/issues/I53CJX) | 【MR】【新增特性】支持兼容system.network | 标准系统 | SIG_Telephony | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 12    | [I53CKH](https://gitee.com/openharmony/communication_netstack/issues/I53CKH) | 【MR】【新增规格】支持兼容system.fetch | 标准系统 | SIG_Telephony | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 13    | [I53CKT](https://gitee.com/openharmony/communication_netstack/issues/I53CKT) | 【MR】【新增特性】支持WebSocket | 标准系统 | SIG_Telephony | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 14    | [I53FCL](https://gitee.com/openharmony/build/issues/I53FCL?from=project-issue) | 【新增特性】【MR】兼容支持stage新模型HAP包随版本编译 | 标准系统 | SIG_CompileRuntime  | [@anguanglin](https://gitee.com/anguanglin) |
| 15   | [I4XLKK](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4XLKK)|【新增特性】【MR】支持API6 System.storage接口|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 16   | [I4XLKL](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4XLKL)|【资料】【MR】提供API6 System.storage的API文档|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 17   | [I53J82](https://gitee.com/openharmony/miscservices_request/issues/I53J82)|【request部件】【upload】支持@system.request接口|标准系统|SIG-Basicsoftwareservice|[@litao33](https://gitee.com/litao33)|
| 18 | [I582RO](https://gitee.com/openharmony/notification_ans_standard/issues/I582RO) |【MR】【增强特性】通知发送和取消API7接口补齐|标准系统|SIG_ApplicationFramework|[@xzz_0810](https://gitee.com/xzz_0810)|
| 19   | [I530U7](https://gitee.com/openharmony/multimodalinput_input/issues/I530U7)|【input部件】三方件json版本升级|标准系统|SIG-basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 20   | [I530UH](https://gitee.com/openharmony/multimodalinput_input/issues/I530UH)|【新增特性】启动优化|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 21   | [I530UU](https://gitee.com/openharmony/multimodalinput_input/issues/I530UU)|【新增特性】支持裁剪鼠标光标显示特性|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 22 | [I530UW](https://gitee.com/openharmony/multimodalinput_input/issues/I530UW) |【MR】【增强特性】通知发送和取消API7接口补齐|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 23 | [I58ZUU](https://e.gitee.com/open_harmony/search?issue=I58ZUU) |【新增规格】测试调度框架支持部件化|标准系统|SIG-Test|[@inter515](https://gitee.com/inter515)|
| 24   | [I530UW](https://gitee.com/openharmony/multimodalinput_input/issues/I530UW)|【新增特性】支持裁剪配置组合按键特性|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|


## OpenHarmony 3.2.2.1版本特性清单：
| no   | issue                                                        | feture description                                       | platform     | sig                      | owner                                   |
| :--- | ------------------------------------------------------------ | :------------------------------------------------------- | :----------- | :----------------------- | :-------------------------------------- |
| 1    | [I54566](https://gitee.com/openharmony/device_board_hihope/issues/I54566) | 【增强特性】相机服务和HDI循环依赖整改 | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 2    | [I56W2U](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W2U) | 【新增特性】【BackupExtension】包管理支持BackupExtension类型          | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 3    | [I56W3Z](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W3Z) | 【新增特性】完善部分命令          | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 4    | [I56W68](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W68) | 【新增特性】将pack.info打到hap包中          | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 5    | [I56W6W](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W6W) | 【新增特性】支持将新旧hap包打包到同一个app包中         | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 6 | [I582UZ](https://gitee.com/openharmony/notification_ces_standard/issues/I582UZ) | 【部件化】事件部件化 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7 | [I58ZUK](https://e.gitee.com/open_harmony/search?issue=I58ZUK) | 【新增规格】UI测试框架能力增强:API接口补充增强 | 标准系统 | SIG_Test | [@inter515](https://gitee.com/inter515) |
| 8 | [I58ZUJ](https://e.gitee.com/open_harmony/search?issue=I58ZUJ) | 【新增规格】用例执行筛选能力 | 标准系统 | SIG_Test | [@inter515](https://gitee.com/inter515) |

## OpenHarmony 3.2.2.2版本特性清单：
| no   | issue                                                        | feture description                                       | platform     | sig                      | owner                                   |
| :--- | ------------------------------------------------------------ | :------------------------------------------------------- | :----------- | :----------------------- | :-------------------------------------- |
| 1    | [I56A90](https://gitee.com/openharmony/drivers_peripheral/issues/I56A90) | 【新增规格】系统支持64位 | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 2    | [I50I6S](https://gitee.com/openharmony/drivers_peripheral/issues/I50I6S) | 【新增特性】Audio新增IPC模式与直调模式接口调用统一 | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 3    | [I54FQG](https://gitee.com/openharmony/drivers_peripheral/issues/I54FQG) | 【新增特性】提供codec设备驱动模型，支持codec类型设备 | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 4    | [I550OL](https://gitee.com/openharmony/drivers_framework/issues/I550OL) | 【新增特性】提供DFX跟踪定位，获取信息能力 | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 5    | [I544XP](https://gitee.com/openharmony/drivers_framework/issues/I544XP) | 【新增特性】支持HDF服务SELinux权限检查| 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 6    | [I4UL98](https://gitee.com/openharmony/drivers_peripheral/issues/I4UL98) | 【新增特性】ADC适配Linux内核IIO框架 | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 7 | [I56H9L](https://gitee.com/openharmony/powermgr_power_manager/issues/I56H9L) | 【增强特性】支持申请亮屏锁的时候，同时点亮屏 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 8 | [I56H9W](https://gitee.com/openharmony/powermgr_power_manager/issues/I56H9W) | 【增强特性】支持窗口单独设置自己的超时灭屏的时长 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 9 | [I56HB8](https://gitee.com/openharmony/powermgr_display_manager/issues/I56HB8) | 【增强特性】支持应用在在一定时间内保持屏幕最大亮度       | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 10 | [I55HGU](https://gitee.com/openharmony/powermgr_display_manager/issues/I55HGU) | 【增强特性】支持setting查询最小、最大、默认亮度值        | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 11 | [I56HBI](https://gitee.com/openharmony/powermgr_power_manager/issues/I56HBI) | 【增强特性】按键检测优化                                 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 12 | [I56HCK](https://gitee.com/openharmony/powermgr_powermgr_lite/issues/I56HCK) | 【增强特性】【powermgr_lite】powermgr_lite支持L1 IPC切换 | 轻量系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 13 | [I55E9Y](https://gitee.com/openharmony/powermgr_battery_lite/issues/I55E9Y) | 【增强特性】【battery_lite】battery_lite支持L1 IPC切换   | 轻量系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 14 | [I577AK](https://gitee.com/openharmony/kernel_linux_5.10/issues/I577AK) | 【新增特性】支持OpenHarmony内存维测增强特性 | 标准系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang) |
| 15 | [I56W4O](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W4O) | 【新增特性】拆包工具将hap包中的rpcid文件提出               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 16 | [I56W59](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W59) | 【新增特性】拆包工具对stage模型包的解析               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 17 | [I56W7H](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W7H) | 【新增特性】bms扫描完成后，发送扫描完成事件               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 18 | [I56WIG](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WIG) | 【新增特性】包管理模块对外提供沙箱应用的安装能力               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 19 | [I56WIS](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WIS) | 【新增特性】包管理模块提供对外沙箱应用的卸载能力               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 20 | [I56WJ7](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WJ7) | 【新增特性】包管理模块提供沙箱应用的包信息的查询能力               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 21 | [I56W07](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W07) | 【新增规格】包管理子系统支持64位               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 22 | [I582UB](https://gitee.com/openharmony/notification_ans_standard/issues/I582UB) | 【新增规格】支持代理通知 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 23 | [I530UL](https://gitee.com/openharmony/multimodalinput_input/issues/I530UL) |【新增特性】支持不可触摸窗口|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 24 | [I530W3](https://gitee.com/openharmony/multimodalinput_input/issues/I530W3) |【新增特性】支持通过JS API监听手写笔输入设备的热插拔|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 25 | [I530W5](https://gitee.com/openharmony/multimodalinput_input/issues/I530W5) |【新增特性】支持通过JS API监听遥控器输入设备的热插拔|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 26 | [I530W9](https://gitee.com/openharmony/multimodalinput_input/issues/I530W9) |【新增特性】支持通过JS API监听鼠标输入设备的热插拔|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 27 | [I530WB](https://gitee.com/openharmony/multimodalinput_input/issues/I530WB) |【新增特性】支持通过JS API监听键盘输入设备的热插拔|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 28 | [I530WF](https://gitee.com/openharmony/multimodalinput_input/issues/I530WF) |【新增特性】支持识别Linux Input手写笔输入设备的热插拔|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 29 | [I530WG](https://gitee.com/openharmony/multimodalinput_input/issues/I530WG) |【新增特性】支持Linux Input手写笔按下、移动、抬起输入事件接收并分发|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 30 | [I530XV](https://gitee.com/openharmony/multimodalinput_input/issues/I530XV) |【新增特性】支持通过JS API监听鼠标移动事件|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 31 | [I530Y7](https://gitee.com/openharmony/multimodalinput_input/issues/I530Y7) |【新增规格】多模输入子系统支持64位|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 32 | [I530WA](https://gitee.com/openharmony/multimodalinput_input/issues/I530WA) |【新增特性】支持通过JS API查看触摸板输入设备扩展信息|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 33 | [I58ZN5](https://e.gitee.com/open_harmony/search?issue=I58ZN5) |测试子系统支持64位|标准系统|SIG_Test|[@inter515](https://gitee.com/inter515)|

## OpenHarmony 3.2.2.3版本特性清单：
| no   | issue                                                        | feture description                                       | platform     | sig                      | owner                                   |
| :--- | ------------------------------------------------------------ | :------------------------------------------------------- | :----------- | :----------------------- | :-------------------------------------- |
| 1 | [I56QF9](https://gitee.com/openharmony/drivers_peripheral/issues/I56QF9) | 【新增规格】UserAuth通用DFX需求（IAM部件合一、南向部件分离） | 标准系统 | SIG_ApplicationFramework | [@wangxu43](https://gitee.com/wangxu43) |
| 2 | [I5875X](https://gitee.com/openharmony/docs/issues/I5875X) | 【新增规格】用户IAM子系统UserAuth南向资料 | 标准系统 | SIG_ApplicationFramework | [@wangxu43](https://gitee.com/wangxu43) |
| 3 | [I58ZUM](https://e.gitee.com/open_harmony/search?issue=I58ZUM) | 【新增规格】UI测试框架能力增强: UI-dump功能开发 | 标准系统 | SIG_Test | [@inter515](https://gitee.com/inter515) |
| 4 | [I58ZUR](https://e.gitee.com/open_harmony/search?issue=I58ZUR) | 【新增规格】UI测试框架能力增强:自定义UI操作参数支持 | 标准系统 | SIG_Test | [@inter515](https://gitee.com/inter515) |
| 5 | [I58ZUS](https://e.gitee.com/open_harmony/search?issue=I58ZUS) | 【新增规格】测试调度框架支持配置 | 标准系统 | SIG_Test | [@inter515](https://gitee.com/inter515) |
| 6 | [I58ZUW](https://e.gitee.com/open_harmony/search?issue=I58ZUW) | 【增强特性】wukong稳定性工具支持控件注入 | 标准系统 | SIG_Test | [@inter515](https://gitee.com/inter515) |
| 7 | [I58ZUV](https://e.gitee.com/open_harmony/search?issue=I58ZUV) | 【增强特性】wukong支持稳定性报告解析、生成与展示 | 标准系统 | SIG_Test | [@inter515](https://gitee.com/inter515) |

## OpenHarmony 3.2.2.5(Beta1)版本特性清单：

## OpenHarmony 3.2.3.1版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------------- | ------------------------ | --------------------------------------- |
| 1    | [I57ZSO](https://gitee.com/openharmony/aafwk_standard/issues/I57ZSO) | 【增强特性】AMS 关键trace补齐                                | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 2    | [I57ZQ2](https://gitee.com/openharmony/aafwk_standard/issues/I57ZQ2?from=project-issue) | 【新增特性】支持动态卡片的信息持久化                         | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 3    | [I57ZPF](https://gitee.com/openharmony/aafwk_standard/issues/I57ZPF) | 支持提供方添加动态卡片                                       | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 4    | [I57ZO2](https://gitee.com/openharmony/aafwk_standard/issues/I57ZO2) | 【特性增强】AMS内部锁优化                                    | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 5    | [I57ZL8](https://gitee.com/openharmony/aafwk_standard/issues/I57ZL8) | 【增强特性】Want支持moduleName                               | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 6    | [I57ZJC](https://gitee.com/openharmony/aafwk_standard/issues/I57ZJC) | 【增强特性】Extension进程管理机制优化                        | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7    | [I58411](https://gitee.com/openharmony/notification_ces_standard/issues/I58411) | 【eventhandler】部件名、仓名和目录整改                       | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 8    | [I582OG](https://gitee.com/openharmony/notification_ans_standard/issues/I582OG) | 【distributed_notification_service部件】部件名、仓名和目录整改 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 9    | [I582OW](https://gitee.com/openharmony/notification_ces_standard/issues/I582OW) | 【common_event_service部件】部件名、仓名和目录整改           | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 10    | [I5A6H6](https://gitee.com/openharmony/device_soc_rockchip/issues/I5A6H6) | 【增强特性】增强显示设备管理,支持多屏显示能力          | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 11    | [I536FN](https://gitee.com/openharmony/drivers_adapter/issues/I536FN) | 【新增特性】支持HDI passthrougt模式           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 12    | [I56V2N](https://e.gitee.com/open_harmony/dashboard?issue=I56V2N) | 【新增特性】HDF WLAN DAL HDI功率模式相关接口的定义与开发           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 13    | [I5936I](https://gitee.com/open_harmony/dashboard?issue_id=I5936I) | 【增强特性】增强Camera设备管理，补齐能力短板           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 14 | [I56W5H](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W5H) | 【新增特性】拆包工具提供解析资源的接口               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 15 | [I56W5T](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W5T) | 【新增特性】配置依赖关系               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 16 | [I56WF6](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WF6) | 【新增特性】支持查询包依赖关系               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 17 | [I56W26](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W26) | 【bundle_framework部件】部件名、仓名和目录整改               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 18 | [I56W1V](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W1V) | 【distributed_bundle_framework部件】部件名、仓名和目录整改               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 19 | [I56W7B](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W7B) | 【新增特性】增加基于ModuleName的查询接口               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 20 | [I56WAB](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WAB) | 【新增特性】预授权机制               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 21 | [I56W1C](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W1C) | 【bundle_tool部件】部件名、仓名和目录整改               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 22 | [I56W0L](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W0L) | 【zlib部件】zlib三方开源软件独立为部件               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 23 | [I56WCC](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WCC) | 【新增特性】包管理DFX               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 24 | [I56WJB](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WJB) | 【DFX打点】【包管理子系统】提供系统事件、SA dump打点               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 25    | [I54CZR](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I54CZR) | 新框架与元能力解耦DataShare代码迁移至数据管理子系统          | 标准系统 | SIG_DataManagement | [@srr101](https://gitee.com/srr101) |
| 26    | [I54CWP](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I54CWP) | 新框架与元能力解耦DataShare 独立部件交付          | 标准系统 | SIG_DataManagement | [@srr101](https://gitee.com/srr101) |
| 27    | [I54D25](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I54D25) | 【新增特性】DataShare支持多种数据储存类型           | 标准系统 | SIG_DataManagement | [@srr101](https://gitee.com/srr101) |
| 28 | [I530XH](https://gitee.com/open_harmony/dashboard?issue_id=I530XN) | 【新增特性】支持系统按键通过配置文件配置按键映射规则	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 29 | [I530XN](https://gitee.com/open_harmony/dashboard?issue_id=I530XX) | 【新增特性】支持键盘按键自动重复	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 30 | [I530XX](https://gitee.com/open_harmony/dashboard?issue_id=I530Y1) | 【新增特性】支持触摸点压力属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 31 | [I530Y1](https://gitee.com/open_harmony/dashboard?issue_id=I530Y2) | 【新增特性】支持触摸点工具区域属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 32 | [I530Y2](https://gitee.com/open_harmony/dashboard?issue_id=I530XZ) | 【新增特性】支持触摸点工具类型属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 33 | [I530XZ](https://gitee.com/open_harmony/dashboard?issue_id=I530WD) | 【新增特性】支持触摸点触摸区域属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|

## OpenHarmony 3.2.3.2版本特性清单：

| no   | issue                                                        | feture description                                   | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ---------------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1    | [I57ZUI](https://gitee.com/openharmony/aafwk_standard/issues/I57ZUI) | 【新增特性】支持无UI应用开发的设备-graphics模块解耦 | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 2    | [I57ZVC](https://gitee.com/openharmony/aafwk_standard/issues/I57ZVC) | 【新增特性】支持无UI应用开发的设备-power模块解耦    | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 3    | [I57ZW3](https://gitee.com/openharmony/aafwk_standard/issues/I57ZW3) | 【新增特性】支持提供方添加静态卡片(配置文件配置)     | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 4    | [I57ZWM](https://gitee.com/openharmony/aafwk_standard/issues/I57ZWM) | 【新增特性】支持查询动态卡片信息                     | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 5    | [I57ZXK](https://gitee.com/openharmony/aafwk_standard/issues/I57ZXK) | 【新增特性】提供方查询静态卡片                       | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 6    | [I57ZXS](https://gitee.com/openharmony/aafwk_standard/issues/I57ZXS) | 【新增特性】Extention接口整改                        | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7    | [I57ZYQ](https://gitee.com/openharmony/aafwk_standard/issues/I57ZYQ) | 【新增特性】组件提供销毁状态查询接口                 | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 8    | [I57ZZH](https://gitee.com/openharmony/aafwk_standard/issues/I57ZZH) | 【新增特性】提供创建不同Hap包上下文能力              | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 9    | [I57ZZZ](https://gitee.com/openharmony/aafwk_standard/issues/I57ZZZ) | 【新增特性】【MR】组件本地启动支持免安装             | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 10   | [I5800S](https://gitee.com/openharmony/aafwk_standard/issues/I5800S) | 【增强特性】 支持ability销毁后移除任务               | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 11   | [I5801E](https://gitee.com/openharmony/aafwk_standard/issues/I5801E) | 【新增特性】测试框架支持应用abilityStage层级测试     | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 12   | [I5801V](https://gitee.com/openharmony/aafwk_standard/issues/I5801V) | 【新增特性】【MR】支持获取当前top FA信息             | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 13   | [I58025](https://gitee.com/openharmony/aafwk_standard/issues/I58025) | 【新增特性】支持系统SA启动和访问组件                 | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 14   | [I5802M](https://gitee.com/openharmony/aafwk_standard/issues/I5802M) | 【新增特性】支持系统SA查询和监听系统环境变化         | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 15   | [I58034](https://gitee.com/openharmony/aafwk_standard/issues/I58034) | 【增强特性】使用libuv统一JS Looper机制               | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 16   | [I5803R](https://gitee.com/openharmony/aafwk_standard/issues/I5803R) | 应用程序框架需要具备接受heapdump信号的能力           | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 17   | [I58047](https://gitee.com/openharmony/aafwk_standard/issues/I58047) | 【元能力】支持单实例Ability迁移                      | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 18   | [I5841H](https://gitee.com/openharmony/notification_ces_standard/issues/I5841H) | 【内存基线】事件通知内存基线                         | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 19   | [I582QQ](https://gitee.com/openharmony/notification_ans_standard/issues/I582QQ) | 【新增规格】通知发送支持设置未读角标                 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 20 | [I56W39](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W39) | 【新增特性】支持原子化服务老化卸载               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 21 | [I524R7](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I524R7) | 【新增特性】支持原子化服务免安装基本能力               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 22 | [I56W50](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W50) | 【新增特性】打包同一应用的app包检查其中的hap包的moduleName要唯一               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 23 | [I56W6P](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W6P) | 【新增特性】增加启动页的配置字段               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 24 | [I5AWKX](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5AWKX) | 【新增特性】包管理支持获取包的指纹信息               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 25 | [I56OQI](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56OQI) | 【新增特性】包管理支持存取hap包的sha256哈希值               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 26 | [I5AWNC](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5AWNC) | 【新增特性】打包工具支持多工程打包               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 27 | [I53JX4](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I53JX4) | 【新增特性】跨设备查询指定语言的Label               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 28 | [I5B0AB](https://gitee.com/openharmony/powermgr_power_manager/issues/I5B0AB) | 【新增规格】【电源管理服务子系统】电源管理子系统的SELinux策略配置 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 29 | [I5B084](https://gitee.com/openharmony/powermgr_power_manager/issues/I5B084) | 【增强特性】Power HDI 接口补齐 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 30 | [I5B09M](https://gitee.com/openharmony/powermgr_battery_manager/issues/I5B09M) | 【增强特性】充放电和电池HDI接口补齐 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 31 | [I5B9DJ](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I5B9DJ) | 【DFX打点】【电源服务子系统】提供系统事件、SA dump、trace打点 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 32 | [I5AOM8](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I5AOM8) | 【新增特性】增加Action Hub功能 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 33 | [I5AJW1](https://gitee.com/openharmony/drivers_peripheral/issues/I5AJW1) | 【新增特性】支持Linux libALSA音频接口兼容           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 34 | [I41H53](https://e.gitee.com/open_harmony/dashboard?issue=I41H53) | 【新增特性】基于HDF框架及平台驱模型实现CANBUS总线框架           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 35 | [I5A2BJ](https://e.gitee.com/open_harmony/dashboard?issue=I5A2BJ) | 【新增特性】用户态中断支持           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 36 | [I5B0C5](https://gitee.com/openharmony/drivers_peripheral/issues/I5B0C5) | 【新增特性】camera支持实现Mate类型的流           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 37 | [I5B0BR](https://gitee.com/openharmony/drivers_peripheral/issues/I5B0BR) | 【新增特性】录像模式自拍镜像功能           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 38 | [I59MYK](https://gitee.com/openharmony/usb_manager/issues/I59MYK) | 【新增特性】USB服务广播消息           | 标准系统 | SIG_UsbManager | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 39 | [I5AR8N](https://gitee.com/openharmony/usb_manager/issues/I5AR8N) | 【新增规格】【USB服务子系统】USB服务子系统的SELinux策略配置           | 标准系统 | SIG_UsbManager | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 40 | [I530WD](https://gitee.com/open_harmony/dashboard?issue_id=I530W0) | 【新增特性】支持通过JS API查看触摸屏输入设备扩展信息	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 41 | [I530W0](https://gitee.com/open_harmony/dashboard?issue_id=I530XT) | 【新增特性】支持通过JS API 查看鼠标输入设备扩展信息	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 42 | [I530XT](https://gitee.com/open_harmony/dashboard?issue_id=I530XM) | 【新增特性】支持通过C++ API按照相对坐标调整鼠标位置	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 43 | [I530XM](https://gitee.com/open_harmony/dashboard?issue_id=I530XK) | 【新增特性】支持键盘通过配置文件配置按键映射规则	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 44 | [I530XK](https://gitee.com/open_harmony/dashboard?issue_id=I530WP) | 【新增特性】支持键盘类型识别	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 45 | [I530WP](https://gitee.com/open_harmony/dashboard?issue_id=I530WM) | 【新增特性】支持触摸点工具类型属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 46 | [I530WM](https://gitee.com/open_harmony/dashboard?issue_id=I530XL) | 【新增特性】支持触摸点触摸区域属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 47 | [I530XL](https://gitee.com/open_harmony/dashboard?issue_id=I530WI) | 【新增特性】支持按键能力识别	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 48 | [I530WI](https://gitee.com/open_harmony/dashboard?issue_id=I530WJ) | 【新增特性】支持Linux Input手写笔压感合成	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 49 | [I530WJ](https://gitee.com/open_harmony/dashboard?issue_id=I530WH) | 【新增特性】支持Linux Input手写笔倾角	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 50 | [I530WH](https://gitee.com/open_harmony/dashboard?issue_id=I530XJ) | 【新增特性】支持Linux Input手写笔功能按键输入事件接收并分发	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|

## OpenHarmony 3.2.3.3版本特性清单：

| no   | issue                                                        | feture description                                   | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ---------------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1 | [I50MMB](https://gitee.com/openharmony/drivers_adapter_khdf_linux/issues/I50MMB) | 【增强特性】MMC现有驱动框架及适配驱动优化           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |

## OpenHarmony 3.2.3.5 版本特性清单：

## OpenHarmony 3.2.5.1版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------------- | ------------------------ | --------------------------------------- |
| 1    | [I5805B](https://gitee.com/openharmony/aafwk_standard/issues/I5805B) | 【增强特性】AMS ANR功能优化                                  | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 2    | [I580B2](https://gitee.com/openharmony/aafwk_standard/issues/I580B2) | 【新增特性】查询系统是否支持卡片使用方添加卡片到桌           | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 3    | [I580GE](https://gitee.com/openharmony/aafwk_standard/issues/I580GE) | 【新增特性】FA卡片提供方升级卸载管理                         | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 4    | [I580IF](https://gitee.com/openharmony/aafwk_standard/issues/I580IF) | 【新增特性】应用启动支持跳转应用市场下载页面                 | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 5    | [I580IQ](https://gitee.com/openharmony/aafwk_standard/issues/I580IQ) | 【ability_runtime部件】部件名仓名和目录整改                  | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 6    | [I580JB](https://gitee.com/openharmony/aafwk_standard/issues/I580JB) | 【新增特性】上报adj模块系统应用被杀后是否能够正常恢复        | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7    | [I580JM](https://gitee.com/openharmony/aafwk_standard/issues/I580JM) | 【新增特性】支持系统应用ability不在最新任务列表显示          | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 8    | [I580JW](https://gitee.com/openharmony/aafwk_standard/issues/I580JW) | 【增强特性】部件依赖整改                                     | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 9    | [I580KA](https://gitee.com/openharmony/aafwk_standard/issues/I580KA) | 【增强特性】支持获任务取低分辨率snapshot                     | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 10   | [I580KL](I580KL)                                             | 【新增规格】【元能力子系统】元能力子系统的SELinux策略配置    | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 11   | [I580LF](https://gitee.com/openharmony/aafwk_standard/issues/I580LF) | 【新增规格】运行时按需加载ABC模块                            | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 12   | [I58418](https://gitee.com/openharmony/notification_ces_standard/issues/I58418) | 【新增规格】【事件通知子系统】事件通知子系统的SELinux策略配置 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 13   | [I582ST](https://gitee.com/openharmony/notification_ans_standard/issues/I582ST) | 【新增规格】支持配置通知清理事件                             | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 14   | [I582TF](https://gitee.com/openharmony/notification_ans_standard/issues/I582TF) | 【增强特性】通知渠道信息设置和查询增强                       | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 15   | [I582UL](https://gitee.com/openharmony/notification_ans_standard/issues/I582UL) | 【增强特性】分布式通知同步增强                               | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 16   | [I582Y4](https://gitee.com/openharmony/notification_ces_standard/issues/I582Y4) | 【DFX打点】【事件通知子系统】提供系统事件、SA dump、trace打点 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 22 | [I5DC5E](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I5DC5E) | 【增强特性】Camera耗电统计 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 22 | [I5DC7M](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I5DC7M) | 【增强特性】Audio耗电统计 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 22 | [I5DC8L](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I5DC8L) | 【增强特性】Timer功耗DFX | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 22 | [I5DOI3](https://gitee.com/openharmony/powermgr_power_manager/issues/I5DOI3) | 【增强特性】power_manager及display_power_manager的重启恢复机制 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 22 | [I5DOIE](https://gitee.com/openharmony/powermgr_battery_manager/issues/I5DOIE) | 【增强特性】battery_manager的重启恢复机制 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 22 | [I5B070](https://gitee.com/openharmony/powermgr_power_manager/issues/I5B070) | 【增强特性】电源管理服务内存基线 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 23 | [I56WD7](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WD7) | 【新增特性】支持查询默认应用               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 24 | [I56WDJ](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WDJ) | 【新增特性】支持更改默认应用               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 25 | [I56WDR](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WDR) | 【新增特性】支持恢复系统默认应用               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 26 | [I56W2E](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W2E) | 【新增规格】【包管理子系统】包管理子系统的SELinux策略配置               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 27 | [I56W2N](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W2N) | 【packing_tool部件】支持应用窗口的属性字段               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 28 | [I56WFD](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WFD) | 【新增特性】提供查询metadata resource的js接口               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 29 | [I56WEK](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WEK) | 【新增特性】支持按文件类型设置默认应用               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 30 | [I56WDY](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WDY) | 【新增特性】支持三方应用把自己设置为默认应用               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 31 | [I5ERJT](https://gitee.com/openharmony/useriam_user_auth_framework/issues/I5ERJT) | 【新增规格】用户IAM子系统SELinux策略配置 | 标准系统 | SIG_ApplicationFramework | [@wangxu43](https://gitee.com/wangxu43) |
| 32 | [I5EPCD](https://gitee.com/openharmony/useriam_user_auth_framework/issues/I5EPCD?from=project-issue) | 支持完成指定用户的指纹录入【框架】 | 用户IAM子系统 | SIG_Security |  [@wangxu](https://gitee.com/wangxu43) |
| 33 | [I5EPCM](https://gitee.com/openharmony/useriam_user_auth_framework/issues/I5EPCM?from=project-issue) | 支持完成指定用户的指纹认证和指纹识别【框架】| 用户IAM子系统 | SIG_Security |  [@wangxu](https://gitee.com/wangxu43)  |
| 34 | [I5EPCU](https://gitee.com/openharmony/useriam_user_auth_framework/issues/I5EPCU?from=project-issue) | 支持完成指定用户的指纹认证删除【框架】| 用户IAM子系统 | SIG_Security |  [@wangxu](https://gitee.com/wangxu43)  |
| 35 | [I5D6CQ](https://gitee.com/openharmony/security_selinux/issues/I5D6CQ) | 【新增规格】【驱动子系统】驱动子系统SELinux策略配置            | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 36 | [I5DVBW](https://e.gitee.com/open_harmony/dashboard?issue=I5DVBW) | 【新增特性】新增音频音效控制           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 37 | [I5CXAB](https://gitee.com/openharmony/drivers_hdf_core/issues/I5CXAB) | 【增强特性】支持红外设备输入           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 38 | [I571EI](https://gitee.com/openharmony/drivers_hdf_core/issues/I571EI) | 【新增特性】基于HDF驱动框架提供心率计Sensor驱动能力           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 39 | [I5E3IQ](https://gitee.com/openharmony/usb_manager/issues/I5E3IQ) | 【增强特性】增强USB设备模式，兼容Linux原生接口           | 标准系统 | SIG_UsbManager | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 40 | [I5ESLP](https://gitee.com/openharmony/drivers_peripheral/issues/I5ESLP) | 【增强特性】增强显示设备管理及硬件合成接口能力，并扩展色彩配置能力           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 41 | [I5DUOW](https://gitee.com/openharmony/drivers_peripheral/issues/I5DUOW) | 【新增特性】增强HDF WLAN DAL的能力           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 42 | [I530XJ](https://gitee.com/open_harmony/dashboard?issue_id=I530X8) | 【新增特性】支持系统按键自动重复	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 43 | [I530X8](https://gitee.com/open_harmony/dashboard?issue_id=I530WO) | 【新增特性】支持通过配置文件配置按键映射规则	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 44 | [I530WO](https://gitee.com/open_harmony/dashboard?issue_id=I530UN) | 【新增特性】支持触摸点工具区域属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 45 | [I530UN](https://gitee.com/open_harmony/dashboard?issue_id=I530YF) | 【新增特性】窗口多热区分发机制	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 46 | [I530YF](https://gitee.com/open_harmony/dashboard?issue_id=I530WK) | 【新增规格】【多模输入】【input】SELinux策略配置	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
## OpenHarmony 3.2.5.2版本特性清单：

| no   | issue                                                        | feture description                           | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | -------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1    | [I580MY](https://gitee.com/openharmony/aafwk_standard/issues/I580MY) | 【新增特性】fms支持dump                      | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 2    | [I580NR](https://gitee.com/openharmony/aafwk_standard/issues/I580NR) | 【新增特性】数据库文件切换路径               | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 3    | [I580OA](https://gitee.com/openharmony/aafwk_standard/issues/I580OA) | 【新增特性】stage模型卡片能力补齐            | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 4    | [I581GX](https://gitee.com/openharmony/aafwk_standard/issues/I581GX) | 【新增特性】AMS支持打开沙箱应用              | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 5    | [I581HE](https://gitee.com/openharmony/aafwk_standard/issues/I581HE) | 【新增特性】FA模型支持windows平台轻量模拟    | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 6    | [I581JD](https://gitee.com/openharmony/aafwk_standard/issues/I581JD) | 【新增特性】Stage模型支持windows平台轻量模拟 | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7    | [I581JY](https://gitee.com/openharmony/aafwk_standard/issues/I581JY) | 【增强特性】常驻进程白名单                   | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 8    | [I581KD](https://gitee.com/openharmony/aafwk_standard/issues/I581KD) | 【增强特性】dataAbility兼容                  | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 9    | [I581KO](https://gitee.com/openharmony/aafwk_standard/issues/I581KO) | 【增强特性】运行管理服务新增线程             | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 10   | [I581L0](https://gitee.com/openharmony/aafwk_standard/issues/I581L0) | 【新增特性】应用上下文支持监听系统状态       | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 11   | [I581LC](https://gitee.com/openharmony/aafwk_standard/issues/I581LC) | 【新增特性】清理整个应用后，发送公共事件     | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 12   | [I581LR](https://gitee.com/openharmony/aafwk_standard/issues/I581LR) | 【增强特性】清理任务时，清理整个应用         | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 13   | [I581M3](https://gitee.com/openharmony/aafwk_standard/issues/I581M3) | 【新增特性】应用选择框                       | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 14   | [I581MR](https://gitee.com/openharmony/aafwk_standard/issues/I581MR) | 【新增特性】ACE 声明式框架适配新的JS runtime | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 15   | [I581ND](https://gitee.com/openharmony/aafwk_standard/issues/I581ND) | 【新增特性】Stage模型适配新的JS runtime      | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 16   | [I581PY](https://gitee.com/openharmony/aafwk_standard/issues/I581PY) | 【新增特性】JS runtime支持日志打印           | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 17   | [I581QJ](https://gitee.com/openharmony/aafwk_standard/issues/I581QJ) | 【新增特性】抽取JS runtime对接vm             | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 18   | [I582SD](https://gitee.com/openharmony/notification_ans_standard/issues/I582SD) | 【DFX】【增强特性】通知DFX能力增强           | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 19   | [I582V2](https://gitee.com/openharmony/notification_ces_standard/issues/I582V2) | 【新增规格】支持点对点发送事件               | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 20   | [I582XB](https://gitee.com/openharmony/notification_ces_standard/issues/I582XB) | 【新增规格】工具适配多用户                   | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 21 | [I5DOLA](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I5DOLA) | 【新增特性】适配SOC资源管控框架 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 22 | [I5DC9H](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I5DC9H) | 【增强特性】分布式任务调度功耗DFX | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 23 | [I5DONL](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I5DONL) | 【新增特性】充电资源管控框架 | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 24 | [I5DOP4](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I5DOP4) | 【新增特性】热维测DFX | 标准系统 | SIG_HardwareMgr | [@aqxyjay](https://gitee.com/aqxyjay) |
| 25 | [I56W7P](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W7P) | 【新增特性】包管理服务内存基线               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 26 | [I56WCV](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WCV) | 【新增特性】支持查询指定应用的PackInfo信息               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 27 | [I56W83](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W83) | 【新增特性】支持组件的迁移前检查               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 28 | [I59ZMR](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I59ZMR) | 【新增特性】包管理支持众测应用               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 29 | [I56W26](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W26) | 【bundle_framework部件】部件名、仓名和目录整改               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 30 | [I5A7OV](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5A7OV) | 【新增特性】包管理提供NDK接口，获取当前包的包名和证书指纹信息               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 31 | [I5ACYA](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5ACYA) | 【新增特性】app图标支持模块化得方式获取               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 32 | [I5214K](https://e.gitee.com/open_harmony/dashboard?issue=I5214K) | 【新增特性】Audio usb插拔识别及事件上报            | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 33 | [I5F411](https://gitee.com/openharmony/drivers_hdf_core/issues/I5F411) | 【增强特性】马达效果能力增强           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 34 | [I528DG](https://gitee.com/openharmony/drivers_peripheral/issues/I528DG) | 【新增特性】支持Codec 2.0参考实现，简化适配难度           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 34 | [I5ETTJ](https://gitee.com/openharmony/drivers_hdf_core/issues/I5ETTJ) | 【新增特性】MMC适配Liteos_m内核           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 34 | [I5DJE5](https://gitee.com/openharmony/drivers_framework/issues/I5DJE5) | 【增强特性】兼容Linux uevent事件上报机制，增强设备即插即用功能           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 34 | [I5FK6J](https://gitee.com/openharmony/drivers_peripheral/issues/I5FK6J) | 【增强特性】提供软件Vsync机制，保障图形合成渲染同步           | 标准系统 | SIG_DriverFramework | [@ji-fengfei](https://gitee.com/ji-fengfei) |
| 38 | [I5EHGF](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I5EHGF) | 【资料】DataShare跨应用数据管理           | 标准系统 |SIG_DataManagement | [@srr101](https://gitee.com/srr101) |
| 39 | [I5FWVI](I5FWVI) | 【新增规格】用户IAM子系统DFX | 标准系统 | SIG_ApplicationFramework | [@wangxu43](https://gitee.com/wangxu43) |
| 40 | [I530WK](https://gitee.com/open_harmony/dashboard?issue_id=I530UT) | 【新增特性】支持触摸点压力属性	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 41 | [I530UT](https://gitee.com/open_harmony/dashboard?issue_id=I530YB) | 【新增特性】JS API Mock	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 42 | [I530YB](https://gitee.com/openharmony/multimodalinput_input/issues/I5HMBS) | 【DFX打点】【多模输入子系统】提供系统事件、SA dump、trace打点	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 43 | [I5HMBS](https://gitee.com/openharmony/multimodalinput_input/issues/I5HMDY) | 【input部件】键鼠自适应布局适配	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 44    | [I5NU4L](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I5NU4L?from=project-issue) | 【新增特性】【hilog部件】系统domain白名单管理    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 45    | [I5NU71](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I5NU71?from=project-issue) | 【新增特性】【hilog部件】日志统计功能    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 46    | [I5NU7F](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I5NU7F?from=project-issue) | 【新增特性】【hilog部件】日志服务权限管理   | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 47    | [I5KDIG](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I5KDIG?from=project-issue) | 【新增规格】【DFX子系统】【hisysevent部件】订阅功能提供应对系统事件风暴能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 48    | [I5L2RV](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I5L2RV?from=project-issue) | 【新增规格】【DFX子系统】【hisysevent部件】查询功能提供应对系统事件风暴能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 49    | [I5LFCZ](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I5LFCZ?from=project-issue) | 【新增规格】【DFX子系统】【hisysevent部件】系统事件存储处理提供应用系统事件风暴能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 50    | [I5FNPQ](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I5FNPQ?from=project-issue) | 【新增规格】【DFX子系统】【hisysevent部件】系统事件打点接口提供应对事件风暴能力   | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 51    | [I5NTOS](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I5NTOS?from=project-issue) | 【新增规格】【DFX子系统】【hiappevent部件】hiappevent的事件清理能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 52    | [I5NTOD](https://gitee.com/openharmony/hiviewdfx_hhiappevent/issues/I5NTOD?from=project-issue) | 【新增规格】【DFX子系统】【hiappevent部件】hiappevent的事件查询能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 53    | [I5LB4N](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I5LB4N?from=project-issue) | 【新增规格】【DFX子系统】【hiappevent部件】hiappevent的事件订阅能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 54    | [I5KYYI](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I5KYYI?from=project-issue) | 【新增规格】【DFX子系统】【hiappevent部件】hiappevent的事件分发能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 55    | [I5K0X6](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I5K0X6?from=project-issue) | 【新增规格】【DFX子系统】【hiappevent部件】hiappevent的事件管理能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 56    | [I5NULM](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I5NULM?from=project-issue) | 【新增规格】【DFX子系统】【hiview部件】提供读取内核事件处理能力    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 57    | [I5NXHX](https://gitee.com/openharmony/developtools_profiler/issues/I5NXHX?from=project-issue) | 【新增特性】支持CPU调优垂直整合JS napi和native栈关联    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 58    | [I5NX04](https://gitee.com/openharmony/hiviewdfx_hidumper/issues/I5NX04?from=project-issue) | 【新增特性】调用MEMTRACK hidl接口，支持获取GPU等设备占用的内存    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 59    | [I5GXTG](https://gitee.com/openharmony/hiviewdfx_hidumper/issues/I5GXTG?from=project-issue) | 【新增特性】hidumper权限隐私优化    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 60    | [5NWZQ](https://gitee.com/openharmony/hiviewdfx_hidumper/issues/I5NWZQ?from=project-issue) | 【新增特性】对进程SMAPS进一步进行分类，在hidumper信息导出时候展示更加细致的分类信息    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |

## OpenHarmony 3.2.5.3版本特性清单：

| no   | issue                                                        | feture description              | platform     | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ------------------------------- | ------------ | ------------------------ | --------------------------------------- |
| 1    | [I581R1](https://gitee.com/openharmony/aafwk_standard/issues/I581R1) | 【新增特性】JS runtime支持timer | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |

## OpenHarmony 3.2.5.5(Beta2)版本特性清单：

## OpenHarmony 3.2.6.1版本特性清单：

| no   | issue                                                        | feture description                                           | platform     | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------ | ------------------------ | --------------------------------------- |
| 1    | [I581RO](https://gitee.com/openharmony/aafwk_standard/issues/I581RO) | 【新增特性】最近任务列表适配加密文件沙箱进程                 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 2    | [I581SE](https://gitee.com/openharmony/aafwk_standard/issues/I581SE) | 【新增特性】AMS对沙箱进程增加权限校验                        | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 3    | [I581T3](https://gitee.com/openharmony/aafwk_standard/issues/I581T3) | 【增强特性】同一进程Extension进行引擎隔离                    | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 4    | [I581UL](https://gitee.com/openharmony/aafwk_standard/issues/I581UL) | 【增强特性】系统环境变化通知支持指针类型设备插拔事件         | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 5    | [I581UZ](https://gitee.com/openharmony/aafwk_standard/issues/I581UZ) | 【新增特性】内存基线级别通知应用                             | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 6    | [I581VW](https://gitee.com/openharmony/aafwk_standard/issues/I581VW) | 【ability_runtime部件】支持hot reload特性，通知虚拟机和ArkUI更新 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7 | [I5HMDY](https://gitee.com/open_harmony/dashboard?issue_id=I530W3) | 【新增特性】支持在指定线程上上报输入事件	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 8 | [I5JX7I](https://gitee.com/openharmony/usb_usb_manager/issues/I5JX7I) | 【DFX打点】【USB服务子系统】提供系统事件、SA dump、trace打点            | 标准系统 | SIG_DriverFramework | [@sunxuejiao1](https://gitee.com/sunxuejiao1) |
| 9 | [I5KFEP](https://gitee.com/openharmony/drivers_peripheral/issues/I5KFEP) | 【新增特性】兼容FB显示框架，支持基于FB显示驱动的快速适配            | 标准系统 | SIG_DriverFramework | [@sunxuejiao1](https://gitee.com/sunxuejiao1) |
| 10 | [I5HAII](https://gitee.com/openharmony/drivers_peripheral/issues/I5HAII) | 【新增特性】支持Codec Adapter,实现2.0到2.1接口转换            | 标准系统 | SIG_DriverFramework | [@sunxuejiao1](https://gitee.com/sunxuejiao1) |
| 11 | [I5HKOK](https://gitee.com/openharmony/drivers_hdf_core/issues/I5HKOK) | 【新增特性】增强wlan驱动的能力，支持三方开发板wlan的快速适配            | 标准系统 | SIG_DriverFramework | [@sunxuejiao1](https://gitee.com/sunxuejiao1) |
| 12 | [I5G0O5](https://gitee.com/openharmony/drivers_hdf_core/issues/I5G0O5) | 【新增特性】提供GPIO命名索引支持           | 标准系统 | SIG_DriverFramework | [@sunxuejiao1](https://gitee.com/sunxuejiao1) |
| 13 | [I5C8C1](https://gitee.com/openharmony/usb_usb_manager/issues/I5C8C1) | 【芯片组件独立构建】USB模块HDI接口IDL改造           | 标准系统 |||
| 14 | [I5K61J](https://gitee.com/openharmony/drivers_peripheral/issues/I5K61J) | 【新增特性】pipeline模块能力增强           | 标准系统 | SIG_DriverFramework | [@sunxuejiao1](https://gitee.com/sunxuejiao1) |
| 15 | [I5J7PW](https://gitee.com/openharmony/deviceprofile_device_info_manager/issues/I5J7PW) | 【新增特性】deviceprofile补齐基础信息采集           | 标准系统 | SIG_Basicsoftwareservice | [@cangegegege](https://gitee.com/cangegegege) |
| 16 | [I5JQ1E](https://gitee.com/openharmony/communication_dsoftbus/issues/I5JQ1E) | 【DFX打点】【分布式软总线子系统】提供系统事件、SA dump           | 标准系统 | SIG_Softbus | [@wang-yougang](https://gitee.com/wang-yougang) |
| 17 | [I5FBFG](https://gitee.com/openharmony/communication_dsoftbus/issues/I5FBFG) | 【增强特性】【组网】支持lane资源管理           | 标准系统 | SIG_Softbus | [@zhangjie1999](https://gitee.com/zhangjie1999) |
| 18 | [I5I7B9](https://gitee.com/openharmony/communication_dsoftbus/issues/I5I7B9) | 【增强特性】【组网】支持软总线真实错误码传递，对外错误码梳理定义到接口           | 标准系统 | SIG_Softbus | [@ma-haoyun](https://gitee.com/ma-haoyun) |
| 19 | [I5GAGX](https://gitee.com/openharmony/interface_sdk-js/issues/I5GAGX) | 【SDK】元能力相关d.ts 支持二级模块导出 | 标准系统 | SIG_ApplicationFramework | [@detail-hua](https://gitee.com/detail-hua) |
| 20 | [I5ICCK](https://gitee.com/openharmony/ability_ability_runtime/issues/I5ICCK) | 【新增特性】ServiceExtension支持call调用 | 标准系统 | SIG_ApplicationFramework | [@H-xinwei](https://gitee.com/H-xinwei) |
| 21 | [I5HQEM](https://gitee.com/openharmony/ability_ability_runtime/issues/I5HQEM) | 【新增特性】上下文支持监听WindowStage生命周期 | 标准系统 | SIG_ApplicaitonFramework | [@dy_study](https://gitee.com/dy_study) |
| 22 | [I5I0DY](https://gitee.com/openharmony/ability_ability_runtime/issues/I5I0DY) | 【新增特性】运行管理支持众测应用 | 标准系统 | SIG_ApplicaitonFramework | [@donglin9](https://gitee.com/donglin9) |
| 23 | [I5N90O](https://gitee.com/openharmony/account_os_account/issues/I5N90O) | 【新增规格】帐号子系统新增用户身份认证服务| 标准系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 24 | [I5N1K3](https://gitee.com/openharmony/distributedhardware_device_manager/issues/I5N1K3) | 【新增特性】设备靠近发现与同账号设备触发组网       | 标准系统 | SIG_Distributedhardwaremgr | [@hhh2](https://gitee.com/hhh2) |
| 25 | [I5N1JI](https://gitee.com/openharmony/distributedhardware_distributed_camera/issues/I5N1JI) | 【新增特性】分布式硬件子系统-分布式相机基础录像能力       | 标准系统 | SIG_Distributedhardwaremgr | [@hhh2](https://gitee.com/hhh2) |
| 26 | [I5HQGA](https://gitee.com/openharmony/communication_dsoftbus/issues/I5HQGA) | 【增强特性】传输对外接口权限校验增强       | 标准系统 | SIG_Softbus | [@breakfei1](https://gitee.com/breakfei1) |
| 27 | [I5FG70](https://gitee.com/openharmony/communication_dsoftbus/issues/I5FG70) | 【新增特性】基于IP的文件传输支持CHACHA加解密算法       | 标准系统 | SIG_Softbus | [@wang-baoguang](https://gitee.com/wang-baoguang) |
| 28 | [I5HSOL](https://gitee.com/openharmony/communication_dsoftbus/issues/I5HSOL) | 【增强特性】【连接】软总线支持NewIp协议       | 标准系统 | SIG_Softbus | [@duxbbo](https://gitee.com/duxbbo) |
| 29 | [I5HZ6N](https://gitee.com/openharmony/communication_dsoftbus/issues/I5HZ6N) | 【增强特性】【传输】支持软总线真实错误码传递，对外错误码梳理定义到接口       | 标准系统 | SIG_Softbus | [@stonesimilar](https://gitee.com/stonesimilar) |
| 30 | [I5IUWJ](https://gitee.com/openharmony/startup_init_lite/issues/I5IUWJ) | 【新增特性】系统进程沙盒环境支持Seccomp-BPF       | 标准系统 | SIG_BscSoftSrv | [@xia-bubai](https://gitee.com/xia-bubai) |
| 31 | [I5NYCT](https://gitee.com/openharmony/useriam_pin_auth/issues/I5NYCT) | 【新增规格】支持口令防离线暴力破解 | 标准系统 | SIG_ApplicationFramework | [@wangxu43](https://gitee.com/wangxu43) |
| 32 | [I5NYDJ](https://gitee.com/openharmony/useriam_user_auth_framework/issues/I5NYDJ) | 【新增规格】支持口令防离线暴力破解 | 标准系统 | SIG_ApplicationFramework | [@wangxu43](https://gitee.com/wangxu43) |
| 33 | [I5NY0M](https://gitee.com/openharmony/security_huks/issues/I5NY0M?from=project-issue) | 【新增规格】HUKS支持密钥锁屏，人脸，指纹访问控制能力  | 标准系统 | SIG_Security | [@wkr321_ent](https://gitee.com/wkr321_ent) |
| 34 | [I5NY2Y](https://gitee.com/openharmony/security_huks/issues/I5NY2Y?from=project-issue) | 【新增规格】HUKS支持应用分身的密钥隔离，并且在删除应用分身的情况下，可以删除相关的密钥数据  | 标准系统 | SIG_Security | [@haixiangw](https://gitee.com/haixiangw) |
| 35 | [I5MZ2R](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ2R) | 【bundle_framework部件】适配iccarm编译               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 36 | [I56W4A](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W4A) | 【新增特性】打包工具检查依赖关系               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 37 | [I56WB8](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WB8) | 【新增特性】支持64位的应用               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 38 | [I5MZ33](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ33) | 【新增特性】支持extension的类型为缩略图、预览               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 39 | [I5MZ3F](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ3F) | 【新增特性】包管理支持更多卡片尺寸以及动态调整大小              | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 40 | [I5MZ3N](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ3N) | 【SDK】包管理相关d.ts 支持二级模块导出               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 41 | [I5JIGT](https://gitee.com/openharmony/arkcompiler_ets_runtime/issues/I5JIGT) | 【新增规格】AsyncGenerator2021规范 | 标准系统 | SIG-CompilerRuntime | [@du-wenboboo](https://gitee.com/du-wenboboo) |
| 42 | [I5J5Z3](https://gitee.com/openharmony/commonlibrary_ets_utils/issues/I5J5Z3) | 【新增规格】buffer 特性 | 标准系统 | SIG-CompilerRuntime | [@leng-changjing](https://gitee.com/leng-changjing) |
| 43 | [I5INW1](https://gitee.com/openharmony/arkcompiler_ets_frontend/issues/I5INW1) | 【新增规格】支持带注解的function tostring方法 | 标准系统 | SIG-CompilerRuntime | [@hufeng](https://gitee.com/hufeng) |
| 44 | [I5INW1](https://gitee.com/openharmony/arkcompiler_ets_frontend/issues/I5INW1) | 【新增规格】支持带注解的function tostring方法 | 标准系统 | SIG-CompilerRuntime | [@su-chongwei](https://gitee.com/su-chongwei) |
| 45 | [I5NO8G](https://gitee.com/openharmony/arkcompiler_ets_runtime/issues/I5NO8G) | 【新增规格】支持方舟运行时汇编解释器增强 | 标准系统 | SIG-CompilerRuntime | [@zhangyukun8](https://gitee.com/zhangyukun8) |
| 46 | [I5J7EC](https://gitee.com/openharmony/arkcompiler_ets_runtime/issues/I5J7EC) | 【新增规格】JS和C++精准交叉混合栈 | 标准系统 | SIG-CompilerRuntime | [@Rtangyu](https://gitee.com/Rtangyu) |
| 47 | [I5K6KF](https://gitee.com/openharmony/arkui_napi/issues/I5K6KF) | 【新增规格】JSI接口支持线程安全接口实现 | 标准系统 | SIG-CompilerRuntime | [@wang_zhaoyong](https://gitee.com/wang_zhaoyong) |
| 48 | [I5NXZ8](https://gitee.com/openharmony/global_resource_management/issues/I5NXZ8?from=project-issue) | 【新增规格】支持输入设备限定词  | 标准系统 | SIG_ApplicationFramework | [@zhuxiang6](https://gitee.com/zhuxiang6) |
| 49 | [I5IY7K](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IY7K) | 【ace_engine_standard部件】主题能力支持 | 标准系统 | SIG_ArkUI | [@huye](https://gitee.com/huye) |
| 50 | [I5IYA3](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IYA3) | 【新增特性】【外部依赖】内容持续时间（内置） | 标准系统 | SIG_ArkUI | [@hzzhouzebin](https://gitee.com/hzzhouzebin) |
| 51 | [I5IZU9](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IZU9) | 【ace_engine_standard部件】ui_service常驻内存优化 | 标准系统 | SIG_ArkUI | [@lushi1202](https://gitee.com/lushi1202) |
| 52 | [I5IZVY](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IZVY) | 【ace_engine_standard部件】键鼠接入时支持组件刷新 | 标准系统 | SIG_ArkUI | [@huye](https://gitee.com/huye) |
| 53 | [I5IZX0](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IZX0) | 【toolchain部件】编译支持解析$r新增bundleName和moduleName参数 | 标准系统 | SIG_ArkUI | [@lihong](https://gitee.com/lihong) |
| 54 | [I5IZXS](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IZXS) | 【toolchain部件】DFX打印错误堆栈时支持显示开发者变量名称原文 | 标准系统 | SIG_ArkUI | [@zhangbingce](https://gitee.com/zhangbingce) |
| 55 | [I5IZYG](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IZYG) | 【toolchain部件】极速预览场景支持静态语法检查 | 标准系统 | SIG_ArkUI | [@houhaoyu](https://gitee.com/houhaoyu) |
| 56 | [I5IZZ7](https://gitee.com/openharmony/arkui_ace_engine/issues/I5IZZ7) | 【ace_engine_standard部件】panel组件支持单独设置每个角的borderRadius | 标准系统 | SIG_ArkUI | [@xiexiyun](https://gitee.com/xiexiyun) |
| 57 | [I5J09I](https://gitee.com/openharmony/arkui_ace_engine/issues/I5J09I) | 【toolchain部件】@Builder支持export导出 | 标准系统 | SIG_ArkUI | [@houhaoyu](https://gitee.com/houhaoyu) |
| 58 | [I5MWS0](https://gitee.com/openharmony/arkui_ace_engine/issues/I5MWS0) | 【ace_engine_standard部件】panel组件弹出高度通知给开发者 | 标准系统 | SIG_ArkUI | [@xiexiyun](https://gitee.com/xiexiyun) |
| 59 | [I5JQ67](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ67) | 【ace_engine_standard部件】Refresh组件refreshing状态支持双向绑定 | 标准系统 | SIG_ArkUI | [@wzztoone](https://gitee.com/wzztoone) |
| 60 | [I5MWTB](https://gitee.com/openharmony/arkui_ace_engine/issues/I5MWTB) | 【ace_engine_standard部件】媒体查询支持vp查询 | 标准系统 | SIG_ArkUI | [@yangfan229](https://gitee.com/yangfan229) |
| 61   | [I5GZGX](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I5GZGX)|【增强特性】【relational_store部件】本地RDB 能力增强|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 62   | [I5O336](https://gitee.com/openharmony/multimedia_audio_framework/issues/I5O336)|【新增特性】支持单应用音频投播和系统所有音频投播|标准系统|SIG_GraphicsandMedia|[@huang-jianfei200](https://gitee.com/huang-jianfei200)|
| 63 | [I5NYBJ](https://gitee.com/openharmony/multimedia_player_framework/issues/I5NYBJ) | 【性能】媒体播放启动时间 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|

## OpenHarmony 3.2.6.2版本特性清单：

| no   | issue                                                        | feture description                                         | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ---------------------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1    | [I581XD](https://gitee.com/openharmony/aafwk_standard/issues/I581XD) | 【新增特性】应用捕获JS Crash异常                           | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 2    | [I581YL](https://gitee.com/openharmony/aafwk_standard/issues/I581YL) | 【新增特性】支持卡片分享                                   | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 3    | [I581Z1](https://gitee.com/openharmony/aafwk_standard/issues/I581Z1) | 【新增特性】FA模型支持apple平台轻量模拟                    | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 4    | [I581ZY](https://gitee.com/openharmony/aafwk_standard/issues/I581ZY) | 【新增特性】Stage模型支持apple平台轻量模拟                 | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 5    | [I58213](https://gitee.com/openharmony/aafwk_standard/issues/I58213) | 【增强特性】提供监听Service/Data/Extension关联关系状态能力 | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 6    | [I58224](https://gitee.com/openharmony/aafwk_standard/issues/I58224) | 【新增特性】应用环境创建和管理优化                         | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 7    | [I5822I](https://gitee.com/openharmony/aafwk_standard/issues/I5822I) | 【新增特性】提供模拟发送点击/触摸事件相关的功能            | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 8    | [I5822Q](https://gitee.com/openharmony/aafwk_standard/issues/I5822Q) | 【新增特性】支持指定应用查询应用生命周期                   | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 9   | [I5823X](https://gitee.com/openharmony/aafwk_standard/issues/I5823X) | 【新增特性】支持后台Ability释放窗口buffer和UI资源          | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 10   | [I5824D](https://gitee.com/openharmony/aafwk_standard/issues/I5824D) | 【新增特性】JS runtime支持worker                           | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 11   | [I5824R](https://gitee.com/openharmony/aafwk_standard/issues/I5824R) | 【新增特性】ACE 类web框架适配新的JS runtime                | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 12   | [I5825N](https://gitee.com/openharmony/aafwk_standard/issues/I5825N) | 【新增特性】不解压HAP加载应用代码                          | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 13   | [I5826I](https://gitee.com/openharmony/aafwk_standard/issues/I5826I) | 【新增特性】不解压HAP加载应用资源                          | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 14   | [I5826Y](https://gitee.com/openharmony/aafwk_standard/issues/I5826Y) | 【新增特性】加载依赖HAP包中的代码                          | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 15   | [I583ZA](https://gitee.com/openharmony/aafwk_standard/issues/I583ZA) | 【新增特性】动态加载多Hap包                                | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 16   | [I5JZEI](https://gitee.com/openharmony/ability_ability_runtime/issues/I5JZEI) | 【启动事件】用户框架启动事件定义                           | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 17   | [I582B9](https://gitee.com/openharmony/aafwk_standard/issues/I582B9) | 【x86镜像】元能力支持x86形态                               | 标准系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 18   | [I582R9](https://gitee.com/openharmony/notification_ans_standard/issues/I582R9) | 【新增规格】工具适配多用户                                 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 19   | [I582TY](https://gitee.com/openharmony/notification_ans_standard/issues/I582TY) | 【新增规格】通知适配DLP                                    | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 20   | [I582VA](https://gitee.com/openharmony/notification_ces_standard/issues/I582VA) | 【新增规格】事件适配DLP                                    | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 21   | [I582WG](https://gitee.com/openharmony/notification_ces_standard/issues/I582WG) | 【DFX】【增强特性】事件DFX能力增强                         | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 22 | [I530W3](https://gitee.com/open_harmony/dashboard?issue_id=I530XB) | 【新增特性】支持通过JS API监听手写笔输入设备的热插拔	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 23 | [I530XB](https://gitee.com/open_harmony/dashboard?issue_id=I530VZ) | 【新增特性】支持遥控器按键自动重复	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 24 | [I530VZ](https://gitee.com/open_harmony/dashboard?issue_id=I530VY) | 【新增特性】支持通过JS API查看遥控器输入设备基础信息	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 25 | [I530VY](https://gitee.com/open_harmony/dashboard?issue_id=I530VV) | 【新增特性】支持通过JS API查看手写笔输入设备基础信息	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 26 | [I530VV](https://gitee.com/open_harmony/dashboard?issue_id=I530XC) | 【新增特性】支持手写笔触摸和手指触摸分开上报	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 27 | [I530XC](https://gitee.com/open_harmony/dashboard?issue_id=I530WU) | 【新增特性】支持识别Linux Input遥控器输入设备	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 28 | [I530WU](https://gitee.com/open_harmony/dashboard?issue_id=I530X6) | 【新增特性】支持逻辑层注入触摸屏单指滑动输入事件	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 29 | [I530X6](https://gitee.com/open_harmony/dashboard?issue_id=I530WQ) | 【新增特性】支持获取Linux Input遥控器设备上报的按键事件并分发	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 30 | [I530WQ](https://gitee.com/open_harmony/dashboard?issue_id=I530X2) | 【新增特性】支持裁剪输入事件拦截	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 31 | [I530X2](https://gitee.com/open_harmony/dashboard?issue_id=I530WR) | 【新增特性】支持裁剪输入事件监听	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 32 | [I530WR](https://gitee.com/open_harmony/dashboard?issue_id=I530X3) | 【新增特性】支持裁剪输入设备管理API	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 33 | [I530X3](https://gitee.com/open_harmony/dashboard?issue_id=I530UX) | 【新增特性】支持按照设备类型裁剪系统能力	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 34 | [I5LGWK](https://gitee.com/openharmony/drivers_interface/issues/I5LGWK) | 【芯片组件独立构建】驱动子系统支持与系统组件解耦           | 标准系统 | SIG_DriverFramework | [@zhangyxgit](https://gitee.com/zhangyxgit) |
| 35 | [I5I0BL](https://gitee.com/openharmony/drivers_peripheral/issues/I5I0BL) | 【增强特性】兼容Linux 原生接口实现PNP功能          | 标准系统 | SIG_DriverFramework | [@zhangyxgit](https://gitee.com/zhangyxgit) |
| 36 | [I5KMF7](https://gitee.com/openharmony/systemabilitymgr_samgr/issues/I5KMF7) | 【增强特性】远程加载系统服务          | 标准系统 | SIG_Basicsoftwareservice | [@cangegegege](https://gitee.com/cangegegege) |
| 37 | [I5GORX](https://gitee.com/openharmony/communication_ipc/issues/I5GORX) | 【samgr】RPC支持远程加载系统服务         | 标准系统 | SIG_SoftBus | [@shufewhx](https://gitee.com/shufewhx) |
| 38 | [I5HMXC](https://gitee.com/openharmony/communication_dsoftbus/issues/I5HMXC) | 【增强特性】【组网】保活能力增强        | 标准系统 | SIG_SoftBus | [@jeosif](https://gitee.com/jeosif) |
| 39 | [I5L0Y7](https://gitee.com/openharmony/interface_sdk-js/issues/I5L0Y7) | 【SDK】事件通知相关d.ts 支持二级模块导出 | 标准系统 | SIG_ApplicationFramework | [@fangJinliang1](https://gitee.com/fangJinliang1) |
| 40 | [I5N90B](https://gitee.com/openharmony/account_os_account/issues/I5N90B) | 【新增规格】应用帐号适配沙箱应用| 标准系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 41 | [I5KYNK](https://gitee.com/openharmony/web_webview/issues/I5KYNK) | 【新增规格】web组件的message管理| 标准系统 | SIG_ApplicationFramework | [@echoorchid](https://gitee.com/echoorchid) |
| 42 | [I5J1GW](https://gitee.com/openharmony/web_webview/issues/I5J1GW) | 【新增规格】web支持页面保存| 标准系统 | SIG_ApplicationFramework | [@cr_wanghui](https://gitee.com/cr_wanghui) |
| 43 | [I5K4NJ](https://gitee.com/openharmony/web_webview/issues/I5K4NJ) | 【新增特性】web支持页面搜索| 标准系统 | SIG_ApplicationFramework | [@fredranking](https://gitee.com/fredranking) |
| 44 | [I5LBE8](https://gitee.com/openharmony/kernel_liteos_m/issues/I5LBE8) | 【新增特性】支持queue地址外部部署共功能及queue名字设置       | 轻量系统 | SIG_Kernel | [@xuiny](https://gitee.com/xuiny) |
| 45 | [I5NU8U](https://gitee.com/openharmony/security_access_token/issues/I5NU8U) | 【新增特性】权限弹框UX界面增强 | 标准系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 46 | [I5NXMW](https://gitee.com/openharmony/useriam_user_auth_framework/issues/I5NXMW) | 【新增特性】用户IAM子系统DFX类需求 | 标准系统 | SIG_ApplicationFramework | [@wangxu43](https://gitee.com/wangxu43) |
| 47 | [I5NY0L](https://gitee.com/openharmony/security_huks/issues/I5NY0L?from=project-issue) | 【新增规格】支持key attestation和id attestation. | 标准系统 | SIG_Security | [@zhang-wenzhi821](https://gitee.com/zhang-wenzhi821) |
| 48 | [I56WA0](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WA0) | 【新增特性】防呆机制               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 49 | [I56WAY](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WAY) | 【新增特性】包管理特性可裁剪配置               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 50 | [I5MZ4C](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ4C) | 【新增特性】包管理支持获取指定应用的图标和label               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 51 | [I56WCO](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WCO) | 【新增特性】支持原子化服务免安装更新基本能力               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 52 | [I5IRJK](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I5IRJK) | 【新增特性】长时任务检测| 标准系统 | SIG_BasicSoftwareService | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf) |
| 53 | [I5NYDH](https://gitee.com/openharmony/customization_config_policy/issues/I5NYDH) | 【新增规格】提供带随卡能力的配置策略接口 | 标准系统 | SIG_ApplicationFramework | [@huangke11](https://gitee.com/huangke11) |
| 54 | [I5HVQ0](https://gitee.com/openharmony/third_party_musl/issues/I5HVQ0) | 【新增规格】fortify功能支持 | 标准系统 | SIG-CompilerRuntime | [@dhy308](https://gitee.com/dhy308) |
| 55 | [I5KT7X](https://gitee.com/openharmony/third_party_musl/issues/I5KT7X) | 【新增特性】NDK API检测工具 | 标准系统 | SIG-CompilerRuntime | [@dhy308](https://gitee.com/dhy308) |
| 56 | [I5HVQE](https://gitee.com/openharmony/third_party_musl/issues/I5HVQE) | 【新增特性】线程stacksize根据产品配置，增加stack pageguard保护 | 标准系统 | SIG-CompilerRuntime | [@dhy308](https://gitee.com/dhy308) |
| 57 | [I5LRIN](https://gitee.com/openharmony-sig/third_party_llvm-project/issues/I5LRIN) | 【增强特性】支持Clang/llvm工具链开源到蓝区 | 标准系统 | SIG-CompilerRuntime | [@liwentao_uiw](https://gitee.com/liwentao_uiw) |
| 58 | [I5EKUV](https://gitee.com/openharmony/arkcompiler_ets_frontend/issues/I5EKUV) | 【新增规格】ES2ABC支持js转换方舟字节码 | 标准系统 | SIG-CompilerRuntime | [@zhuoli72](https://gitee.com/zhuoli72) |
| 59 | [I5LDNO](https://gitee.com/openharmony/arkcompiler_ets_frontend/issues/I5LDNO) | 【资料】语言编译器运行时资料更新 | 标准系统 | SIG-CompilerRuntime | [@zhuoli72](https://gitee.com/zhuoli72) |
| 60 | [I5HYBL](https://gitee.com/openharmony/arkcompiler_runtime_core/issues/I5HYBL) | 【新增规格】bytecode optimizer类型信息处理 | 标准系统 | SIG-CompilerRuntime | [@zhuoli72](https://gitee.com/zhuoli72) |
| 61 | [I5IZTL](https://gitee.com/openharmony/arkcompiler_runtime_core/issues/I5IZTL) | 【新增特性】【外部依赖】动画参数（内置） | 标准系统 | SIG_ArKUI | [@hzzhouzebin](https://gitee.com/hzzhouzebin) |
| 62 | [I5H6CS](https://gitee.com/openharmony/arkui_ace_engine/issues/I5H6CS) | 【window_manager部件】【新增规格】提供窗口属性动画接口 | 标准系统 | SIG_ArKUI | [@hehongyang9](https://gitee.com/hehongyang9) |
| 63 | [I5JQ5Y](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ5Y) | 【ace_engine_standard部件】走焦能力增强 | 标准系统 | SIG_ArKUI | [@catpoison](https://gitee.com/catpoison) |
| 64 | [I5JQ1R](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ1R) | 【ace_engine_standard部件】支持图片复制粘贴 | 标准系统 | SIG_ArKUI | [@luoying_ace](https://gitee.com/luoying_ace_admin) |
| 65 | [I5JQ26](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ26) | 【ace_engine_standard部件】Vsync请求机制流程优化 | 标准系统 | SIG_ArKUI | [@lightningHo](https://gitee.com/lightningHo) |
| 66 | [I5JQ2D](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ2D) | 【ace_engine_standard部件】Move事件重采样优化 | 标准系统 | SIG_ArKUI | [@wzztoone](https://gitee.com/wzztoone) |
| 67 | [I5JQ2O](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ2O) | 【ace_engine_standard部件】公共资源预加载 | 标准系统 | SIG_ArKUI | [@lightningHo](https://gitee.com/lightningHo) |
| 68 | [I5JQ36](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ36) | 【ace_engine_standard部件】页面切换事件上报 | 标准系统 | SIG_ArKUI | [@hzzhouzebin](https://gitee.com/hzzhouzebin) |
| 69 | [I5JQ39](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ39) | 【ace_engine_standard部件】TextInput/TextArea/Search支持点击其他区域失焦 | 标准系统 | SIG_ArKUI | [@catpoison](https://gitee.com/catpoison) |
| 70 | [I5JQ3F](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ3F) | 【ace_engine_standard部件】输入框能力增强 | 标准系统 | SIG_ArKUI | [@xiexiyun](https://gitee.com/xiexiyun) |
| 71 | [I5JQ3J](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ3J) | 【ace_engine_standard部件】stack组件新增事件拓传机制 | 标准系统 | SIG_ArKUI | [@yangfan229](https://gitee.com/yangfan229) |
| 72 | [I5JQ4S](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ4S) | 【ace_engine_standard部件】支持侧边栏左右位置配置 | 标准系统 | SIG_ArKUI | [@lijuan124](https://gitee.com/lijuan124) |
| 73 | [I5JQ4X](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ4X) | 【ace_engine_standard部件】Panel内输入法弹出时，主页面不受影响，且背景有可选蒙层 | 标准系统 | SIG_ArKUI | [@xiexiyun](https://gitee.com/xiexiyun) |
| 74 | [I5JQ54](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ54) | 【ace_engine_standard部件】指定控件获取焦点 | 标准系统 | SIG_ArKUI | [@catpoison](https://gitee.com/catpoison) |
| 75 | [I5JQ5P](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ5P) | 【ace_engine_standard部件】popup支持尖角位置自定义 | 标准系统 | SIG_ArKUI | [@shanshurong](https://gitee.com/shanshurong) |
| 76   | [I5JV75](https://gitee.com/openharmony/distributeddatamgr_datamgr_service/issues/I5JV75)|【新增特性】【relational_store部件】支持远程查询特性|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 77   | [I5OEWG](https://gitee.com/openharmony/multimedia_audio_framework/issues/I5OEWG)|【新增特性】​支持系统应用设置和获取单个音频流的音量|标准系统|SIG_GraphicsandMedia|[@liuyuehua1](https://gitee.com/liuyuehua1)|
| 78 | [I5NYCP](https://gitee.com/openharmony/multimedia_player_framework/issues/I5NYCP) | 【新增特性】支持基于HDI codec接口的视频硬解码能力 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|
| 79 | [I5NYCF](https://gitee.com/openharmony/multimedia_player_framework/issues/I5NYCF) | 【新增特性】支持基于HDI codec接口的视频硬编码能力 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|

## OpenHarmony 3.2.6.3 版本特性清单：

| no   | issue                                                        | feture description                                         | platform | sig                 | owner                                                   |
| ---- | :----------------------------------------------------------- | ---------------------------------------------------------- | -------- | ------------------- | ------------------------------------------------------- |
| 1    | [I5NLY9](https://gitee.com/openharmony/arkcompiler_ets_runtime/issues/I5NLY9) | 【新增规格】方舟支持轻量级模拟器                           | 标准系统 | SIG-CompilerRuntime | [@weng-changcheng](https://gitee.com/weng-changcheng)   |
| 2    | [I5MX66](https://gitee.com/openharmony/arkui_ace_engine/issues/I5MX66) | 【ace_engine_standard部件】支持ColorFilter能力             | 标准系统 | SIG_ArKUI           | [@niulihua](https://gitee.com/niulihua)                 |
| 3    | [I5JQ2T](https://gitee.com/openharmony/arkui_ace_engine/issues/I5JQ2T) | 【ace_engine_standard部件】图片复制/粘贴支持ctrl+c/v快捷键 | 标准系统 | SIG_ArKUI           | [@catpoison](https://gitee.com/catpoison)               |
| 4    | [I5MX7J](https://gitee.com/openharmony/arkui_ace_engine/issues/I5MX7J) | 【ace_engine_standard部件】list列表支持左滑/右滑及回弹效果 | 标准系统 | SIG_ArKUI           | [@yeyinglong_admin](https://gitee.com/yeyinglong_admin) |
| 5    | [I4WLV5](https://gitee.com/openharmony/global_i18n/issues/I4WLV5) | 【资料】全球化新增API资料更新                           | 标准系统   | SIG_Docs | [@neeen](https://gitee.com/neeen) |
| 6    | [I4WLSJ](https://gitee.com/openharmony/global_i18n/issues/I4WLSJ) | 【新增特性】翻译伪本地化能力                                   | 标准系统   | SIG_ApplicationFramework | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 7    | [I4WKP8](https://gitee.com/openharmony/global_i18n_lite/issues/I4WKP8) | 【新增特性】度量衡体系与格式化                    | 轻量系统   | SIG_ApplicationFramework | [@zhengbin5](https://gitee.com/zhengbin5) |
| 8    | [I5NXHK](https://gitee.com/openharmony/inputmethod_imf/issues/I5NXHK)|【input_method_fwk部件】输入法框架支持仅绑定输入法innerkits接口和独立控制软键盘显隐的js接口|标准系统|SIG_BasicSoftwareService|[@liuwenhui_ddmp](https://gitee.com/liuwenhui_ddmp)|


## OpenHarmony 3.2.6.5 版本特性清单：

## OpenHarmony 3.2.7.1 版本特性清单：

| no   | issue                                                        | feture description                                         | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ---------------------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1 | [I530UX](https://gitee.com/open_harmony/dashboard?issue_id=I530VT) | 【新增特性】支持修改鼠标移动速度	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 2 | [I530VT](https://gitee.com/open_harmony/dashboard?issue_id=I530XP) | 【新增特性】支持显示/隐藏鼠标图标	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 3 | [I530XP](https://gitee.com/open_harmony/dashboard?issue_id=I530XS) | 【新增特性】支持鼠标加速算法	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 4 | [I530XS](https://gitee.com/open_harmony/dashboard?issue_id=I530UQ) | 【新增特性】支持多种鼠标样式	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 5 | [I5IZQ7](https://gitee.com/openharmony/communication_dsoftbus/issues/I5IZQ7) | 【soft_bus部件】适配iccarm编译	|标准系统| SIG_SoftBus |[@stonesimilar](https://gitee.com/stonesimilar)|
| 6 | [I5KRE8](https://gitee.com/openharmony/communication_dsoftbus/issues/I5KRE8) | 【增强特性】流传输支持COMMON流	|标准系统| SIG_SoftBus |[@breakfei1](https://gitee.com/breakfei1)|
| 7 | [I5NG8L](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I5NG8L) | 【新增特性】WorkScheduler需要新增参数携带接口	|标准系统| SIG_BasicSoftwareService |[@wangwenli_wolf](https://gitee.com/wangwenli_wolf)|
| 8 | [I5NOQI](https://gitee.com/openharmony/security_access_token/issues/I5NOQI) | 【新增特性】权限服务支持精准定位或模糊定位 | 标准系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 9 | [I56WGH](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WGH) | 【新增特性】支持同一个应用的fa模型和stage模型包的同时安装               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 10 | [I56W8B](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W8B) | 【新增特性】数据库整改               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 11 | [I56W8O](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56W8O) | 【新增特性】预置应用特定能力管理               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 12 | [I56WFH](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WFH) | 【新增特性】数据库文件切换路径               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 13 | [I5MZ7Y](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ7Y) | 【新增特性】 支持打包工具对module，ability，entry的唯一性校验               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 14 | [I5O4BN](https://gitee.com/openharmony/web_webview/issues/I5O4BN) | 【新增特性】webview剪切板对接系统剪切板               | 标准系统 | SIG_ApplicationFramework | [@zourongchun](https://gitee.com/zourongchun)       |
| 15 | [I5O4BB](https://gitee.com/openharmony/web_webview/issues/I5O4BB) | 【新增特性】webview支持页面图片的选中和复制               | 标准系统 | SIG_ApplicationFramework | [@zourongchun](https://gitee.com/zourongchun)       |
| 16 | [I5O4B5](https://gitee.com/openharmony/web_webview/issues/I5O4B5) | 【新增特性】webview支持页面input标签粘贴纯文本               | 标准系统 | SIG_ApplicationFramework | [@zourongchun](https://gitee.com/zourongchun)       |
| 17 | [I5O4AZ](https://gitee.com/openharmony/web_webview/issues/I5O4AZ) | 【新增特性】webview支持页面纯文本的选中和复制               | 标准系统 | SIG_ApplicationFramework | [@zourongchun](https://gitee.com/zourongchun)       |
| 18 | [I5OHDE](https://gitee.com/openharmony/communication_dsoftbus/issues/I5OHDE) | 【增强特性】软总线加解密适配Openssl       | 标准系统 | SIG_softbus | [@breakfei1](https://gitee.com/breakfei1)    |
| 19 | [I5N9IY](https://gitee.com/openharmony/systemabilitymgr_safwk/issues/I5N9IY) | 【samgr】foundation进程重启恢复机制               | 标准系统 | SIG_Basicsoftwareservice | [@cangegegege](https://gitee.com/cangegegege)       |
| 20 | [I5NOA1](https://gitee.com/openharmony/ability_dmsfwk/issues/I5NOA1) | 【分布式组件管理】【DMS】DMS上报分布式应用信息给管控模块               | 标准系统 | SIG_Basicsoftwareservice | [@cangegegege](https://gitee.com/cangegegege)       |
| 21 | [I5OJDU](https://gitee.com/openharmony/ability_dmsfwk/issues/I5OJDU) | 【增强特性】迁移体验跟踪               | 标准系统 | SIG_Basicsoftwareservice | [@cangegegege](https://gitee.com/cangegegege)       |
| 22 | [I5MUXD](https://gitee.com/openharmony/startup_init_lite/issues/I5MUXD) | 【新增特性】应用进程沙盒支持seccomp-bpf       | 标准系统 | SIG_BscSoftSrv | [@xia-bubai](https://gitee.com/xia-bubai) |
| 23 | [I5OSHX](https://gitee.com/openharmony/base_location/issues/I5OSHX) | 【新增特性】【位置服务子系统】支持模糊位置能力       | 标准系统 | SIG_softbus | [@liu-binjun](https://gitee.com/liu-binjun) |
| 24 | [I5OE8Q](https://gitee.com/openharmony/startup_appspawn/issues/I5OE8Q) | 【新增特性】新增PID Namespace | 标准系统 | SIG_BscSoftSrv | [@zhu-binjun](https://gitee.com/zhushengle) |
| 25 | [I5P97S](https://gitee.com/openharmony/web_webview/issues/I5P97S?from=project-issue) | 【增强特性】web支持w3c input标签自动获焦下绑定输入法 | 标准系统 | SIG_ApplicationFramework | [@yu-haoge](https://gitee.com/yu-haoge) |
| 26 | [I5P1GU](https://gitee.com/openharmony/notification_distributed_notification_service/issues/I5P1GU) | 【distributed_notification_service部件】支持子系统发送通知 | 标准系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810) |
| 27 | [I5P4IU](https://gitee.com/openharmony/security_access_token/issues/I5P4IU) | 【新增特性】新增隐私管理服务 | 标准系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 28 | [I5P4J7](https://gitee.com/openharmony/security_access_token/issues/I5P4J7) | 【新增特性】UX支持支持精准定位或模糊定位 | 标准系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 29 | [I5OGBZ](https://gitee.com/openharmony/ability_ability_runtime/issues/I5OGBZ) | 【特性增强】Stage模型Ability和Extension支持异步生命周期回调 | 标准系统 | SIG_ApplicationFramework | [@yzkp](https://gitee.com/yzkp) |
| 30 | [I5L811](https://gitee.com/openharmony/ability_ability_runtime/issues/I5L811) | 【特性增强】FA模型DataAbility接口支持权限校验 | 标准系统 | SIG_ApplicationFramework | [@detail-hua](https://gitee.com/detail-hua) |
| 31 | [I5OOKW](https://gitee.com/openharmony/multimedia_player_framework/issues/I5OOKW) | 【新增特性】支持h.265格式视频硬编码 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|
| 32 | [I5OOKN](https://gitee.com/openharmony/multimedia_player_framework/issues/I5OOKN) | 【新增特性】支持h.265格式视频硬解码 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|

## OpenHarmony 3.2.7.2 版本特性清单：

| no   | issue                                                        | feture description                                         | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ---------------------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1 | [I530UQ](https://gitee.com/open_harmony/dashboard?issue_id=I530Y9) | 【新增特性】分布式输入	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 2 | [I530Y9](https://gitee.com/openharmony/multimodalinput_input/issues/I5HMEF) | 【x86镜像】多模输入子系统支持x86形态	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 3 | [I5HMF3](https://gitee.com/openharmony/multimodalinput_input/issues/I5HMD9) | 【增强特性】支持鼠标离开窗口时上报事件	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 4 | [I5HMEF](https://gitee.com/openharmony/multimodalinput_input/issues/I5HMCX) | 【增强特性】鼠标上报事件增强	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 5 | [I5HMD9](https://gitee.com/openharmony/multimodalinput_input/issues/I5HMCB) | 【input部件】配置按键序列拉起Ability	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 6 | [I56WJL](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WJL) | 【x86镜像】x86镜像端侧模拟器上不需要对hap包进行验签               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 7 | [I56WJP](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WJP) | 【x86镜像】包管理子系统支持x86形态               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 8 | [I56WGB](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I56WGB) | 【新增特性】安装流程适配hap包不解压              | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 9 | [I5MZ5D](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ5D) | 【新增特性】支持补丁包的拆包解包工具               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 10 | [I5MZ5L](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ5L) | 【新增特性】支持生成补丁包的打包工具               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 11 | [I5MZ5Y](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ5Y) | 【新增特性】支持补丁包的信息查询               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 12 | [I5MZ69](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ69) | 【新增特性】支持旧补丁包的删除               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 13 | [I5N7AD](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5N7AD) | 【新增特性】支持补丁包的部署               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 14 | [I5MZ6P](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ6P) | 【新增特性】支持补丁包的切换               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 15 | [I5MZ6Z](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ6Z) | 【新增特性】支持补丁包的开机扫描               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 16 | [I5MZ7R](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ7R) | 【bundle_framework部件】支持HotReload补丁包的部署               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 17 | [I53SG1](https://gitee.com/openharmony/sensors_sensor/issues/I53SG1) | 泛Sensor服务子系统支持x86形态	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 18 | [I53SGE](https://gitee.com/openharmony/sensors_miscdevice/issues/I53SGE) | 支持马达优先级管理	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 19 | [I5NT1X](https://gitee.com/openharmony/security_access_token/issues/I5NT1X) | 【新增特性】权限使用记录管理增强 | 标准系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 20 | [I5OB2Y](https://gitee.com/openharmony/ability_ability_runtime/issues/I5OB2Y) | 【新增特性】Ability关闭时，支持任务回调 | 标准系统 | SIG_ApplicationFramework | [@ccllee](https://gitee.com/ccllee) |
| 21 | [I5LW79](https://gitee.com/openharmony/web_webview/issues/I5LW79) | 【新增规格】web支持地理位置权限管理 | 标准系统 | SIG_ApplicationFramework | [@xiongjun_gitee](https://gitee.com/xiongjun_gitee) |
| 22 | [I5P91V](https://gitee.com/openharmony/web_webview/issues/I5P91V) | 【增强特性】web组件cookie权限配置、权限状态获取和清除 | 标准系统 | SIG_ApplicationFramework | [@zhufenghao](https://gitee.com/zhufenghao) |
| 23 | [I5OH5C](https://gitee.com/openharmony/communication_bluetooth/issues/I5OH5C) | 【新增特性】蓝牙scan filter的C类接口实现 | 标准系统 | SIG_Basicsoftwareservice | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf) |
| 24 | [I5HRX9](https://gitee.com/openharmony/web_webview/issues/I5HRX9?from=project-issue) | 【新增特性】webview支持多媒体播放基础能力 | 标准系统 | SIG_ApplicationFramework | [@pilipala195](https://gitee.com/pilipala195) |
| 25 | [I5P001](https://gitee.com/openharmony/web_webview/issues/I5P001?from=project-issue) | 【增强特性】webview支持shift连选多个文字 | 标准系统 | SIG_ApplicationFramework | [@pilipala195](https://gitee.com/pilipala195) |
| 26 | [I5OZZ8](https://gitee.com/openharmony/web_webview/issues/I5OZZ8?from=project-issue) | 【新增特性】webview支持鼠标左键滑动选词 | 标准系统 | SIG_ApplicationFramework | [@pilipala195](https://gitee.com/pilipala195) |
| 27 | [I5PPVO](https://gitee.com/openharmony/useriam_user_auth_framework/issues/I5PPVO?from=project-issue) | 【新增特性】构建结果BEP一致性治理 | 用户IAM子系统 | SIG_Security | [@youliang_1314](https://gitee.com/youliang_1314) |
| 28 | [I5PBT1](https://gitee.com/openharmony/customization_enterprise_device_management/issues/I5PBT1) | 【新增特性】EDM API权限校验增强 | 标准系统 | SIG_BasicSoftwareService | [@caiminggang](https://gitee.com/caiminggang) |
| 29 | [I5PTUS](https://gitee.com/openharmony/communication_dsoftbus/issues/I5PTUS) | 【增强特性】【组网】Wifi单向认证 |标准系统| SIG_SoftBus |[@yuanmmy](https://gitee.com/yuanmmy)|
| 30 | [I5OAEP](https://gitee.com/openharmony/communication_dsoftbus/issues/I5OAEP) | 【增强特性】【组网】认证框架优化 |标准系统| SIG_SoftBus |[@liangliang_ma](https://gitee.com/liangliang_ma)|
| 31 | [I5PIFW](https://gitee.com/openharmony/communication_dsoftbus/issues/I5PIFW) | 【增强特性】【组网】补全决策中心感知模块能力，支持动态决策 |标准系统| SIG_SoftBus |[@jeosif](https://gitee.com/jeosif)|
| 32 | [I5P530](https://gitee.com/openharmony/security_access_token/issues/I5P530) | 【新增特性】位置服务使用状态管理 | 标准系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng) |
| 33 | [I5NXG9](https://gitee.com/openharmony/web_webview/issues/I5NXG9?from=project-issue) | 【新增规格】web组件touch、scroll、gesture事件支持 | 标准系统 | SIG_ApplicationFramework| [@laosan_ted](https://gitee.com/laosan_ted) |
| 34 | [I5OURV](https://gitee.com/openharmony/web_webview/issues/I5OURV?from=project-issue) | 【增强特性】webview支持软键盘弹出效果优化 | 标准系统 | SIG_ApplicationFramework| [@laosan_ted](https://gitee.com/laosan_ted) |
| 35 | [I5OESN](https://gitee.com/openharmony/web_webview/issues/I5OESN?from=project-issue) | 【增强特性】webview支持w3c draggable属性 | 标准系统 | SIG_ApplicationFramework| [@LongLie](https://gitee.com/LongLie) |
| 36 | [I5OSHX](https://gitee.com/openharmony/base_location/issues/I5OSHX) | 【新增特性】【位置服务子系统】支持模糊位置能力       | 标准系统 | SIG_softbus | [@liu-binjun](https://gitee.com/liu-binjun) |
| 37 | [I5PX7W](https://gitee.com/openharmony/base_location/issues/I5PX7W) | 【新增特性】【位置服务子系统】支持后台定位的显性化       | 标准系统 | SIG_softbus | [@liu-binjun](https://gitee.com/liu-binjun) |
| 38 | [I5OD7X](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I5OD7X) | 提供给系统应用的运行状态注册API | 标准系统     | SIG_BasicSoftwareService | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf) |
| 39 | [I5MSBN](https://gitee.com/openharmony/drivers_peripheral/issues/I5MSBN) | 【新增特性】USB服务在Device模式下支持端口功能切换接口           | 标准系统 | SIG_DriverFramework | [@zhangyxgit](https://gitee.com/zhangyxgit) |
| 40 | [I5OUD1](https://gitee.com/openharmony/usb_usb_manager/issues/I5OUD1) | 【新增特性】USB服务支持动态按需启停能力（非常驻进程）          | 标准系统 | SIG_DriverFramework | [@zhangyxgit](https://gitee.com/zhangyxgit) |
| 41 | [I5JE68](https://gitee.com/openharmony/third_party_flutter/issues/I5JE68) | 【新增特性】【基础资源调度子系统】【graphic部件】字体纹理按需缓存       | 标准系统 | SIG_BasicSoftwareService | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf) |
| 42 | [I5PJ9O](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I5PJ9O?from=project-issue) | 【新增规格】【faultloggerd部件】混合JS/Native栈打印    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 43 | [I5OA3F](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I5OA3F?from=project-issue) | 【新增规格】【DFX子系统】【hisysevent部件】提升HiSysEvent的订阅查询接口易用性    | 标准系统 | SIG_BasicSoftwareService  | [@stone2050](https://gitee.com/stone2050) |
| 44 | [I5Q0QD](https://gitee.com/openharmony/customization_enterprise_device_management/issues/I5Q0QD?from=project-issue) | 【SDK】EDM相关d.ts 支持二级模块导出 | 标准系统 | SIG_BasicSoftwareService	 | [@caiminggang](https://gitee.com/caiminggang) |
| 45 | [I5OCZV](https://gitee.com/openharmony/ability_ability_runtime/issues/I5OCZV) | 【新增特性】快速修复服务 - 命令行工具 | 标准系统 | SIG_ApplicationFramework	| [@zhangyafei-echo](https://gitee.com/zhangyafei-echo) |
| 46 | [I5QBZT](https://gitee.com/openharmony/ability_ability_runtime/issues/I5QBZT) | 【特性集成】【E2E完整性】元能力子系统二进制可信治理 | 标准系统 | SIG_ApplicationFramework	| [@xzz_0810](https://gitee.com/xzz_0810) |
| 47 | [I5G2SH](https://gitee.com/openharmony/ability_form_fwk/issues/I5G2SH) | 【增强特性】卡片框架支持2*1规格卡片 | 标准系统 | SIG_ApplicationFramework	| [@li-weifeng2](https://gitee.com/li-weifeng2) |
| 48 | [I5ODCD](https://gitee.com/openharmony/ability_ability_runtime/issues/I5ODCD) | 【新增特性】快速修复服务 - 查询补丁 | 标准系统 | SIG_ApplicationFramework	| [@zhangyafei-echo](https://gitee.com/zhangyafei-echo) |
| 49 | [I5OD2E](https://gitee.com/openharmony/ability_ability_runtime/issues/I5OD2E) | 【新增特性】快速修复服务 - 安装补丁 | 标准系统 | SIG_ApplicationFramework	| [@zhangyafei-echo](https://gitee.com/zhangyafei-echo) |
| 50 | [I5J1VS](https://gitee.com/openharmony/third_party_mimalloc/issues/I5J1VS) | 【新增特性】支持高性能Musl malloc分配器适配特性 | 标准系统 | SIG_Kernel	| [@liuyoufang](https://gitee.com/liuyoufang) |
| 51 | [I5P8N0](https://gitee.com/openharmony/multimedia_player_framework/issues/I5P8N0) | 【新增特性】支持视频编码native接口 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|
| 52 | [I5OX06](https://gitee.com/openharmony/multimedia_player_framework/issues/I5OX06) | 【新增特性】支持视频解码native接口 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|
| 53 | [I5OXCD](https://gitee.com/openharmony/multimedia_player_framework/issues/I5OXCD) | 【新增特性】支持音频编码native接口 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|
| 54 | [I5OWXY](https://gitee.com/openharmony/multimedia_player_framework/issues/I5OWXY) | 【新增特性】支持音频解码native接口 | 标准系统 | SIG_GraphicsandMedia | [@liuyuehua1](https://gitee.com/liuyuehua1)|

## OpenHarmony 3.2.7.3 版本特性清单：

| no   | issue                                                        | feture description                                         | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ---------------------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1 | [I5QA3D](https://gitee.com/openharmony/web_webview/issues/I5QA3D) | 【新增特性】webview支持页面图文混排内容的选中和复制               | 标准系统 | SIG_ApplicationFramework | [@jiangkuaixue](https://gitee.com/jiangkuaixue)       |
| 2 | [I5PLYI](https://gitee.com/openharmony/communication_dsoftbus/issues/I5PLYI) | 【特性增强】软总线支持识别远端服务未向软总线注册并返回信息               | 标准系统 | SIG_SoftBus | [@breakfei1](https://gitee.com/breakfei1)       |
| 3 | [I5QDNN](https://gitee.com/openharmony/security_crypto_framework/issues/I5QDNN) | 【crypto_framework部件】支持OpenSSL证书生成，解析，验证 | 标准系统 | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin) |
| 4 | [I5QWEG](https://gitee.com/openharmony/security_crypto_framework/issues/I5QWEG) | 【crypto_framework部件】支持OpenSSL对称加密算法 | 标准系统 | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin) |
| 5 | [I5QWEI](https://gitee.com/openharmony/security_crypto_framework/issues/I5QWEI) | 【crypto_framework部件】支持OpenSSL对称非对称加密算法 | 标准系统 | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin) |
| 3 | [I5QWEM](https://gitee.com/openharmony/security_crypto_framework/issues/I5QWEM) | 【crypto_framework部件】支持OpenSSL消息摘要算法 | 标准系统 | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin) |
| 3 | [I5QWEN](https://gitee.com/openharmony/security_crypto_framework/issues/I5QWEN) | 【crypto_framework部件】支持OpenSSL安全随机数生成 | 标准系统 | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin) |
| 3 | [I5QWEO](https://gitee.com/openharmony/security_crypto_framework/issues/I5QWEO) | 【crypto_framework部件】支持加解密算法库框架 | 标准系统 | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin) |
| 9 | [I5QPGN](https://gitee.com/openharmony/deviceprofile_device_info_manager/issues/I5QPGN) | 【device_profile部件】DP适配数据库自动同步策略 | 标准系统 | SIG_Basicsoftwareservice | [@cangegegege](https://gitee.com/cangegegege) |
| 10 | [I5KIZC](https://gitee.com/openharmony/arkui_ace_engine/issues/I5KIZC) | 【新增特性】卡片联动 | 标准系统 | SIG_ApplicationFramework | [@li-weifeng2](https://gitee.com/li-weifeng2) |
| 11 | [I5PFT9](https://gitee.com/openharmony/ability_form_fwk/issues/I5PFT9) | 【新增特性】应用内卡片主动刷新 | 标准系统 | SIG_ApplicationFramework | [@li-weifeng2](https://gitee.com/li-weifeng2) |
| 12 | [I5Q8IU](https://gitee.com/openharmony/ability_form_fwk/issues/I5Q8IU) | 【新增特性】应用内卡片强制刷新 | 标准系统 | SIG_ApplicationFramework | [@li-weifeng2](https://gitee.com/li-weifeng2) |
| 13 | [I5QGMS](https://gitee.com/openharmony/ability_form_fwk/issues/I5QGMS) | 【新增特性】应用内卡片事件管理 | 标准系统 | SIG_ApplicationFramework | [@li-weifeng2](https://gitee.com/li-weifeng2) |
| 14 | [I5OORU](https://gitee.com/openharmony/ability_form_fwk/issues/I5OORU) | 【新增特性】应用内删除卡片 | 标准系统 | SIG_ApplicationFramework | [@li-weifeng2](https://gitee.com/li-weifeng2) |
| 15 | [I5MVKZ](https://gitee.com/openharmony/ability_form_fwk/issues/I5MVKZ) | 【新增特性】应用内加载卡片 | 标准系统 | SIG_ApplicationFramework | [@li-weifeng2](https://gitee.com/li-weifeng2) |
| 16 | [I5ORNO](https://gitee.com/openharmony/web_webview/issues/I5ORNO) | 【新增特性】支持webview的https的双向校验 | 标准系统 | SIG_ApplicationFramework | [@GITEE_xuBin](https://gitee.com/GITEE_xuBin) |
| 17 | [I5PRJ7](https://gitee.com/openharmony/ability_ability_runtime/issues/I5PRJ7) | 【特性增强】ServiceAbility、DataAbility以及Extension启动规则 | 标准系统 | SIG_ApplicationFramework | [@H-xinwei](https://gitee.com/H-xinwei) |
| 18 | [I5QXCQ](https://gitee.com/openharmony/ability_ability_runtime/issues/I5QXCQ) | 【特性增强】Ability启动规则 | 标准系统 | SIG_ApplicationFramework | [@H-xinwei](https://gitee.com/H-xinwei) |
| 19 | [I5PXW4](https://gitee.com/openharmony/ability_ability_runtime/issues/I5PXW4) | 【特性增强】支持查询监听进程/组件可见性等状态能力 | 标准系统 | SIG_ApplicationFramework | [@zhou-shicheng2](https://gitee.com/zhou-shicheng2) |

## OpenHarmony 3.2.7.5(Beta3) 版本特性清单：

## OpenHarmony 3.2.8.1 版本特性清单：

| no   | issue                                                        | feture description                                         | platform       | sig                      | owner                                   |
| ---- | ------------------------------------------------------------ | ---------------------------------------------------------- | -------------- | ------------------------ | --------------------------------------- |
| 1 | [I5HMCX](https://gitee.com/open_harmony/dashboard?issue_id=I5G3RY) | 【新增特性】支持键盘功能按键使能去使能	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 2 | [I5HMCB](https://gitee.com/open_harmony/dashboard?issue_id=I530Y5) | 【input部件】输入设备相关的多热区	|标准系统|SIG-Basicsoftwareservice|[@hhh2](https://gitee.com/hhh2)|
| 3 | [I5MZ8C](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ8C) | 【新增特性】设置处置状态调整               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 4 | [I5MZ8K](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ8K) | 【新增特性】支持增、删、查应用禁止运行的名单               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 5 | [I5MZ8Q](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ8Q) | 【新增特性】支持增、删、查应用安装和卸载的应用名单               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 6 | [I5MZ8V](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ8V) | 【包管理】API 支持异常处理               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 7 | [I5MZ90](https://gitee.com/openharmony/bundlemanager_bundle_framework/issues/I5MZ90) | 【三方开源软件】API 支持异常处理               | 标准系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)       |
| 8 | [I5R6E0](https://gitee.com/openharmony/web_webview/issues/I5R6E0) | 【新增规格】web组件全屏事件支持 | 标准系统 | SIG_ApplicationFramework | [@echoorchid](https://gitee.com/echoorchid) |
| 9 | [I5T6GJ](https://gitee.com/openharmony/ability_dmsfwk/issues/I5T6GJ) | 【分布式组件管理】【DMS】ability组件启动管控 | 标准系统 | SIG_ApplicationFramework | [@myzfan](https://gitee.com/myzfan) |
| 10 | [I5T6HF](https://gitee.com/openharmony/ability_dmsfwk/issues/I5T6HF) | 【分布式组件管理】【DMS】ServiceAbility/ServiceExtension组件启动管控 | 标准系统 | SIG_ApplicationFramework | [@myzfan](https://gitee.com/myzfan) |
| 11 | [I5UONG](https://gitee.com/openharmony/security_huks/issues/I5UONG) | 【huks部件】HUKS提供NDK对外接口 | 标准系统 | SIG_Security | [@wkr321_ent](https://gitee.com/wkr321_ent) |
| 12 | [I5UOK7](https://gitee.com/openharmony/security_huks/issues/I5UOK7) | 【安全基础能力】API 支持异常处理 | 标准系统 | SIG_Security | [@zhang-wenzhi821](https://gitee.com/zhang-wenzhi821) |

## OpenHarmony 3.2.8.2 版本特性清单：

## OpenHarmony 3.2.8.3 版本特性清单：

###### ()以上计划由OpenHarmony社区版本发布SIG组织发布

